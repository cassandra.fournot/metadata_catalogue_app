export default async function List_inputs(id_document) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const inputsStep1_required = [];
    const inputsStep1 = [];
    const inputsStep2_required = [];
    const inputsStep2 = [];
    const inputsStep3_required = [];
    const inputsStep3 = [];
    const state = {};

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/new")

        .then(function (response) {
            // Récupération de la réponse de la requête
            var structureStep1_required = response.data[0];
            var structureStep1 = response.data[1];
            var structureStep2_required = response.data[3];
            var structureStep2 = response.data[4];
            var structureStep3_required = response.data[6];
            var structureStep3 = response.data[7];

            // Extraction des clés du dictionnaire
            var keysStep1_required = Object.keys(structureStep1_required);
            var keysStep1 = Object.keys(structureStep1);
            var keysStep2_required = Object.keys(structureStep2_required);
            var keysStep2 = Object.keys(structureStep2);
            var keysStep3_required = Object.keys(structureStep3_required);
            var keysStep3 = Object.keys(structureStep3);

            // Pour tous les champs base required
            for (var s = 0; s < keysStep1_required.length; s++) {

                // Récupération des informations
                var id = keysStep1_required[s];
                var placeholder = structureStep1_required[id]["placeholder"];
                var label = structureStep1_required[id]["label"];

                // Ajout du champ à la liste
                inputsStep1_required.push({ "id": id, "placeholder": placeholder, "label": label });
            }

            // Pour tous les champs base
            for (var s = 0; s < keysStep1.length; s++) {

                // Récupération des informations
                var id = keysStep1[s];
                var placeholder = structureStep1[id]["placeholder"];
                var label = structureStep1[id]["label"];

                // Ajout du champ à la liste
                inputsStep1.push({ "id": id, "placeholder": placeholder, "label": label });
            }

            // Pour tous les champs table required
            for (var s = 0; s < keysStep2_required.length; s++) {

                // Récupération des informations
                var id = keysStep2_required[s];
                var placeholder = structureStep2_required[id]["placeholder"];
                var label = structureStep2_required[id]["label"];

                // Ajout du champ à la liste
                inputsStep2_required.push({ "id": id, "placeholder": placeholder, "label": label });
            }

            // Pour tous les champs table
            for (var s = 0; s < keysStep2.length; s++) {

                // Récupération des informations
                var id = keysStep2[s];
                var placeholder = structureStep2[id]["placeholder"];
                var label = structureStep2[id]["label"];

                // Ajout du champ à la liste
                inputsStep2.push({ "id": id, "placeholder": placeholder, "label": label });
            }

            // Pour tous les champs variable required
            for (var s = 0; s < keysStep3_required.length; s++) {

                // Récupération des informations
                var id = keysStep3_required[s];
                var placeholder = structureStep3_required[id]["placeholder"];
                var label = structureStep3_required[id]["label"];

                // Ajout du champ à la liste
                inputsStep3_required.push({ "id": id, "placeholder": placeholder, "label": label });
            }

            // Pour tous les champs variable
            for (var s = 0; s < keysStep3.length; s++) {

                // Récupération des informations
                var id = keysStep3[s];
                var placeholder = structureStep3[id]["placeholder"];
                var label = structureStep3[id]["label"];

                // Ajout du champ à la liste
                inputsStep3.push({ "id": id, "placeholder": placeholder, "label": label, "value": "" });
            }

        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });


    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/edit_base_in_progress/" + id_document + "_" + JSON.parse(window.sessionStorage.getItem("token"))['token'];

    // Envoi de la requête
    await axios.post(url)
        // Récupération de la réponse
        .then(function (response) {
            state["state"] = response.data;
        })
        .catch(function (error) {
            console.log(error);
        });

    var State = state["state"];

    var dico = {};
    const ListStep2 = {};

    // Parcour des clés 
    for (var s = 0; s < Object.keys(State).length; s++) {
        if (Object.keys(State)[s].includes(inputsStep2_required[0]["id"])) {
            dico[Object.keys(State)[s]] = State[Object.keys(State)[s]];
        }
    }

    for (var d = 0; d < Object.keys(dico).length; d++) {
        ListStep2[d] = Object.keys(dico)[d];
    }

    var dico = {};
    const ListStep3 = {};

    // Parcour des clés 
    for (var s = 0; s < Object.keys(State).length; s++) {
        if (Object.keys(State)[s].includes(inputsStep3_required[0]["id"])) {
            dico[Object.keys(State)[s]] = State[Object.keys(State)[s]];
        }
    }

    for (var d = 0; d < Object.keys(dico).length; d++) {
        ListStep3[Object.keys(dico)[d].split('#')[1] + '#' + Object.keys(dico)[d].split('#')[2]] = Object.keys(dico)[d];
    }

    return ({ "Step2": Object.keys(ListStep2).length - 1, "Step3": -1, "State": State, "ListStep2": ListStep2, "ListStep3": ListStep3 });

}
