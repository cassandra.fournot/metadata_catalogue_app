export default async function List_Step3(base){
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const list_step3 = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/essentiels_variables" )
            
    .then(function (response) {        
        // Affichage de la réponse dans la console
        list_step3.push(response)
    })
        // En cas d'erreur
    .catch(function (error) {
        // Affichage de l'erreur dans la console
        console.log(error);
    });
  
    return list_step3

}
