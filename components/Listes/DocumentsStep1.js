export default async function List_Step1(base) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const list_step1 = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/bases/" + base)

        .then(function (response) {
            // Affichage de la réponse dans la console
            list_step1.push(response.data);
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return list_step1[0];

}
