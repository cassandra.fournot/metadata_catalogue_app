export default async function Historique() {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const historique = {};

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/insertion")

        .then(function (response) {
            historique["insertion"] = response.data;
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/modification")

        .then(function (response) {
            historique["modification"] = response.data;
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/suppression")

        .then(function (response) {
            historique["suppression"] = response.data;
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return historique;
}
