export default async function Users() {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const users = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/list_users")

        // Récupération de la réponse
        .then(function (response) {
            users.push(response.data);
        });

    return users;
}

