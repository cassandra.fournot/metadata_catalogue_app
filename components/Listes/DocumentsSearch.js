export default async function DocumentsSearch(input, selectID) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const listDocumentSearch = [];
    const listDocumentSearchSelect = [];

    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/search/" + input;

    // Définition des données à envoyer (pour la requête)
    var data = { q: input };

    // Envoi de la requête
    await axios.post(url, data)

        .then(function (response) {
            // Extraction des clés du dictionnaire
            var keys = Object.keys(response.data.hits);
            for (var k = 0; k < keys.length; k++) {
                var id = response.data.hits[k]["id"];
                var keys_k = Object.keys(response.data.hits[k]);
                var document = String([keys_k[1]]);
                if (response.data.hits[k][keys_k[1]]["dct:description"] == undefined) {
                    var description = "";
                }
                else {
                    var description = String(response.data.hits[k][keys_k[1]]["dct:description"]);
                }
                listDocumentSearch.push({ "id": id, "document": document, "description": description });
            }
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    if (selectID.length != 0) {
        for (var d = 0; d < listDocumentSearch.length; d++) {
            for (var s = 0; s < selectID.length; s++) {
                if (selectID[s].id == listDocumentSearch[d].id) {
                    listDocumentSearchSelect.push({ "id": listDocumentSearch[d].id, "document": listDocumentSearch[d].document, "description": listDocumentSearch[d].description });
                }
            }
        }
    }

    if (listDocumentSearchSelect.length != 0) {
        return listDocumentSearchSelect;
    }
    else {
        return listDocumentSearch;
    }
}
