export default async function List_Step2(data) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const list_step2 = [];

    // Envoi de la requête
    await axios.post("http://workmetadata.francecentral.cloudapp.azure.com:5004/tables", data)

        .then(function (response) {
            list_step2.push(response.data);
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return list_step2[0];

}
