export default async function DocumentsSearch(input, selectID, step1) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const listDocumentSearch = [];
    const listDocumentSearchSelect = [];

    var data = { "base_id": step1, "input": input };
    // Envoi de la requête
    await axios.post("http://workmetadata.francecentral.cloudapp.azure.com:5004/search_tables", data)

        .then(function (response) {
            for (var document = 0; document < response.data.length; document++) {
                var keys = Object.keys(response.data[document]);
                var id = response.data[document][keys[0]];
                var doc = [keys[1]][0];
                listDocumentSearch.push({ "id": id, "document": doc });
            }

            if (selectID.length != 0) {
                for (var d = 0; d < listDocumentSearch.length; d++) {
                    for (var s = 0; s < selectID.length; s++) {
                        if (selectID[s].id == listDocumentSearch[d].id) {
                            listDocumentSearchSelect.push({ "id": listDocumentSearch[d].id, "document": listDocumentSearch[d].document });
                        }
                    }
                }
            }
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    if (listDocumentSearchSelect.length != 0) {
        return listDocumentSearchSelect;
    }
    else {
        console.log(listDocumentSearch);
        return listDocumentSearch;
    }
}
