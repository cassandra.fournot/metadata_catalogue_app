export default async function AllDocuments() {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const listDocuments = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/list_bases")

        .then(function (response) {
            // Extraction des clés du dictionnaire
            var keys = Object.keys(response.data);
            for (var k = 0; k < keys.length; k++) {
                var id = response.data[k]["id"];
                var keys_k = Object.keys(response.data[k]);
                var document = String([keys_k[1]]);
                if (response.data[k][keys_k[1]]["dct:description"] == undefined) {
                    var description = "";
                }
                else {
                    var description = String(response.data[k][keys_k[1]]["dct:description"]);
                }
                listDocuments.push({ "id": id, "document": document, "description": description });
            }
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return listDocuments;
}
