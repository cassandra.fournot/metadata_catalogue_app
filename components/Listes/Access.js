export default async function ListAccess(token) {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const listAccess = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/list_access")

        .then(function (response) {
            // Extraction des clés du dictionnaire
            var keys = Object.keys(response.data);
            for (var k = 0; k < keys.length; k++) {
                var document = response.data[k]["document_id"];
                var username = response.data[k]["username"];
                listAccess.push({ "document": document, "username": username });
            }
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return listAccess;
}
