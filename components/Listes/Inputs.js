export default async function List_inputs() {
    const axios = require('axios');

    // Déclaration des variables nécessaires
    const inputsStep1_required = [];
    const inputsStep1 = [];
    const categoriesStep1 = [];
    const inputsStep2_required = [];
    const inputsStep2 = [];
    const categoriesStep2 = [];
    const inputsStep3_required = [];
    const inputsStep3 = [];
    const categoriesStep3 = [];

    // Envoi de la requête
    await axios.get("http://workmetadata.francecentral.cloudapp.azure.com:5004/new")

        .then(function (response) {
            // Récupération de la réponse de la requête
            var structureStep1_required = response.data[0];
            var structureStep1 = response.data[1];
            var structureCategoriesStep1 = response.data[2];
            var structureStep2_required = response.data[3];
            var structureStep2 = response.data[4];
            var structureCategoriesStep2 = response.data[5];
            var structureStep3_required = response.data[6];
            var structureStep3 = response.data[7];
            var structureCategoriesStep3 = response.data[8];

            // Extraction des clés du dictionnaire
            var keysStep1_required = Object.keys(structureStep1_required);
            var keysStep1 = Object.keys(structureStep1);
            var keysCategoriesStep1 = Object.keys(structureCategoriesStep1);
            var keysStep2_required = Object.keys(structureStep2_required);
            var keysStep2 = Object.keys(structureStep2);
            var keysCategoriesStep2 = Object.keys(structureCategoriesStep2);
            var keysStep3_required = Object.keys(structureStep3_required);
            var keysStep3 = Object.keys(structureStep3);
            var keysCategoriesStep3 = Object.keys(structureCategoriesStep3);

            // Pour tous les champs base required
            for (var s = 0; s < keysStep1_required.length; s++) {

                // Récupération des informations
                var id = keysStep1_required[s];
                var placeholder = structureStep1_required[id]["placeholder"];
                var label = structureStep1_required[id]["label"];
                var type = structureStep1_required[id]["type"];

                // Ajout du champ à la liste
                inputsStep1_required.push({ "id": id, "placeholder": placeholder, "label": label, "type": type });
            }

            // Pour tous les champs base
            for (var s = 0; s < keysStep1.length; s++) {

                // Récupération des informations
                var id = keysStep1[s];
                var placeholder = structureStep1[id]["placeholder"];
                var label = structureStep1[id]["label"];
                var type = structureStep1[id]["type"];

                // Ajout du champ à la liste
                inputsStep1.push({ "id": id, "placeholder": placeholder, "label": label, "type": type });
            }

            // Pour tous les catégories des champs base
            for (var s = 0; s < keysCategoriesStep1.length; s++) {

                // Récupération des informations
                var categorie = keysCategoriesStep1[s];
                var listChamps = structureCategoriesStep1[categorie];

                // Ajout du champ à la liste
                categoriesStep1.push({ "categorie": categorie, "listChamps": listChamps });
            }

            // Pour tous les champs table required
            for (var s = 0; s < keysStep2_required.length; s++) {

                // Récupération des informations
                var id = keysStep2_required[s];
                var placeholder = structureStep2_required[id]["placeholder"];
                var label = structureStep2_required[id]["label"];
                var type = structureStep2_required[id]["type"];

                // Ajout du champ à la liste
                inputsStep2_required.push({ "id": id, "placeholder": placeholder, "label": label, "type": type });
            }

            // Pour tous les champs table
            for (var s = 0; s < keysStep2.length; s++) {

                // Récupération des informations
                var id = keysStep2[s];
                var placeholder = structureStep2[id]["placeholder"];
                var label = structureStep2[id]["label"];
                var type = structureStep2[id]["type"];

                // Ajout du champ à la liste
                inputsStep2.push({ "id": id, "placeholder": placeholder, "label": label, "type": type });
            }

            // Pour tous les catégories des champs table
            for (var s = 0; s < keysCategoriesStep2.length; s++) {

                // Récupération des informations
                var categorie = keysCategoriesStep2[s];
                var listChamps = structureCategoriesStep2[categorie];

                // Ajout du champ à la liste
                categoriesStep2.push({ "categorie": categorie, "listChamps": listChamps });
            }

            // Pour tous les champs variable required
            for (var s = 0; s < keysStep3_required.length; s++) {

                // Récupération des informations
                var id = keysStep3_required[s];
                var placeholder = structureStep3_required[id]["placeholder"];
                var label = structureStep3_required[id]["label"];
                var type = structureStep3_required[id]["type"];

                // Ajout du champ à la liste
                inputsStep3_required.push({ "id": id, "placeholder": placeholder, "label": label, "type": type });
            }

            // Pour tous les champs variable
            for (var s = 0; s < keysStep3.length; s++) {

                // Récupération des informations
                var id = keysStep3[s];
                var placeholder = structureStep3[id]["placeholder"];
                var label = structureStep3[id]["label"];
                var type = structureStep3[id]["type"];

                // Ajout du champ à la liste
                inputsStep3.push({ "id": id, "placeholder": placeholder, "label": label, "value": "", "type": type });
            }

            // Pour tous les catégories des champs variable
            for (var s = 0; s < keysCategoriesStep3.length; s++) {

                // Récupération des informations
                var categorie = keysCategoriesStep3[s];
                var listChamps = structureCategoriesStep3[categorie];

                // Ajout du champ à la liste
                categoriesStep3.push({ "categorie": categorie, "listChamps": listChamps });
            }
        })
        // En cas d'erreur
        .catch(function (error) {
            // Affichage de l'erreur dans la console
            console.log(error);
        });

    return ({ "Step1_required": inputsStep1_required, "Step1": inputsStep1, "Categories_Step1": categoriesStep1, "Step2_required": inputsStep2_required, "Step2": inputsStep2, "Categories_Step2": categoriesStep2, "Step3_required": inputsStep3_required, "Step3": inputsStep3, "Categories_Step3": categoriesStep3 });

}
