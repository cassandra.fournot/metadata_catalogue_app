import React, { useState, useEffect } from "react";
import { Box, MenuItem, Select, Grid } from "@mui/material";
import { BoxContent, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import Users from '../Listes/Users';

const axios = require('axios');

export default function ModifUser() {

  const [error, setError] = useState("");
  const [username, setUsername] = useState(0);
  const [status, setStatus] = useState(0);
  const [listUsers, setListUsers] = useState([]);
  const [isInit, setIsInit] = useState(false);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      ListUsers();
    }
  }, [isInit]);


  async function ListUsers() {
    const response = await Users();
    setListUsers(response[0]);
  }

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Modification(state) {
    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/modif_user";

    // Envoi de la requête
    var axios_resp = axios.post(url, state)
      // Récupération de la réponse
      .then(function (response) {
        if (response.status == 200) {
          setValidationPost(true);
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    if (validationPost) {
      document.location.href = "success_user_modif";
    }
    else {
      setError("Un problème est apparu lors de la modification.");
    }
  };

  return (
    <BoxContent>
      <Box sx={{ marginTop: 5 }}>
        <Titre_section variant='h5'>Informations sur l'utilisateur</Titre_section>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Nom d'utilisateur :</Texte></Grid>
          <Grid item xs={9}>
            <Select
              value={username}
              onChange={(event) => { setUsername(event.target.value); }}
              sx={{ width: '200px' }}
              size="small"
            >
              <MenuItem value={0}>Utilisateur</MenuItem>
              {listUsers.map((e) => {
                return (
                  <MenuItem value={e.username}>{e.username}</MenuItem>
                );
              })}
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box sx={{ marginTop: 1 }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Status :</Texte></Grid>
          <Grid item xs={9}>
            <Select
              value={status}
              onChange={(event) => { setStatus(event.target.value); }}
              size="small"
            >
              <MenuItem value={1}>Administateur</MenuItem>
              <MenuItem value={0}>Utilisateur</MenuItem>
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box>
        <Button_form id="button" onClick={() => { Modification({ "username": username, "status": status }); }}>Valider</Button_form>
        <Texte>{error}</Texte>
      </Box>
    </BoxContent>
  );
}