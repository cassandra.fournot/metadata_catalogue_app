import React, { useState } from "react";
import { Box, TextField, Grid } from "@mui/material";
import { BoxContent, Texte_justify, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import hasLowerCase from "../Functions/hasLowerCase.js";
import hasUpperCase from "../Functions/hasUpperCase.js";
import hasInt from "../Functions/hasInt.js";
import hasSpe from "../Functions/hasSpe.js";
import extractParamsUrl from "../Functions/extractParamsUrl.js";

const axios = require('axios');

function User() {

    const [error, setError] = useState("");
    const [password, setPassword] = useState("");

    const [color_nb, setColorNb] = useState("red");
    const [color_min, setColorMin] = useState("red");
    const [color_maj, setColorMaj] = useState("red");
    const [color_int, setColorInt] = useState("red");
    const [color_spe, setColorSpe] = useState("red");

    // Variables de vérification
    const [validationPost, setValidationPost] = useState([]);

    // Fonction qui permet de sauvegarder les modifications d'une base
    function Add(state) {
        if (!state["password"].match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%&_])([-+!*$@%&_\w]{8,50})$/)) {
            setError("Le mot de passe doit contenir respecter les conditions ci-dessus.");
        }
        else {

            // Définition de l'URL (pour la requête)
            var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/new_login";

            // Envoi de la requête
            var axios_resp = axios.post(url, state)
                // Récupération de la réponse
                .then(function (response) {
                    if (response.status == 200) {
                        setValidationPost(true);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });

            if (validationPost) {
                document.location.href = "success_first";
            }
            else {
                setError("Un problème est apparu lors de l'insertion.");
            }
        }
    };

    return (
        <BoxContent>
            <Texte_justify>Merci de bien vouloir saisir vos informations de connexion qui vous serviront à utiliser votre compte sur le catalogue de métadonnées du Health Data Hub.</Texte_justify>
            <Box>
                <Titre_section variant='h5' sx={{ marginTop: "20px" }}>Informations de connexion</Titre_section>
                <Texte_justify>Le mot de passe des utilisateurs doit contenir respecter les conditions suivantes :</Texte_justify>
                <Texte_justify sx={{ color: color_nb }}>- entre 8 et 50 catactères,</Texte_justify>
                <Texte_justify sx={{ color: color_min }}>- au moins une lettre minuscule,</Texte_justify>
                <Texte_justify sx={{ color: color_maj }}>- au moins une lettre majuscule,</Texte_justify>
                <Texte_justify sx={{ color: color_int }}>- au moins un chiffre</Texte_justify>
                <Texte_justify sx={{ color: color_spe }}>- au moins un de ces caractères spéciaux : & $ @ % * + - _ !</Texte_justify>
                <Grid container sx={{ alignItems: "center" }}>
                    <Grid item xs={2}><Texte>Mot de passe :</Texte></Grid>
                    <Grid item xs={9}>
                        <TextField required type="password" label="Mot de passe" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => {
                            setPassword(event.target.value);
                            if (event.target.value.length >= 8 && event.target.value.length <= 50) {
                                setColorNb("green");
                            }
                            else {
                                setColorNb("red");
                            }
                            if (hasLowerCase(event.target.value)) {
                                setColorMin("green");
                            }
                            else {
                                setColorMin("red");
                            }
                            if (hasUpperCase(event.target.value)) {
                                setColorMaj("green");
                            }
                            else {
                                setColorMaj("red");
                            }
                            if (hasInt(event.target.value)) {
                                setColorInt("green");
                            }
                            else {
                                setColorInt("red");
                            }
                            if (hasSpe(event.target.value)) {
                                setColorSpe("green");
                            }
                            else {
                                setColorSpe("red");
                            }
                        }} />
                    </Grid>
                </Grid>
            </Box>
            <Box>
                <Button_form id="button" onClick={() => { Add({ "username": Object.keys(extractParamsUrl(window.location.href))[0], "password": password }); }}>Valider</Button_form>
                {error.split("\n").map((e) => {
                    return (<Texte>{e}</Texte>);
                })}
            </Box>
        </BoxContent>
    );
}

export default User;
