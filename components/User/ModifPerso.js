import React, { useState, useEffect } from "react";
import { Box, TextField, Grid } from "@mui/material";
import { BoxContent, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import Users from '../Listes/Users';
import verificationEmail from "../Functions/verificationEmail.js";

const axios = require('axios');

export default function ModifPerso() {

  const { token, setToken } = useToken();
  const [error, setError] = useState("");
  const [listUsers, setListUsers] = useState([]);
  const [lastMail, setLastMail] = useState("");
  const [mail, setMail] = useState("");
  const [isInit, setIsInit] = useState(false);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      InfoUser();
    }
  }, [isInit]);

  // Fonction qui récupère les droits de l'utilisateur
  async function InfoUser() {
    const response = await Users();
    setListUsers(response[0]);
    for (var user = 0; user < response[0].length; user++) {
      if (response[0][user]["username"] == token) {
        setLastMail(response[0][user]["mail"]);
      }
    }
  }

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Modification(state) {
    if (!verificationEmail(state["mail"])) {
      setError("Merci de saisir une adresse mail valide.");
    }
    else {
      var mail = false;

      for (var user = 0; user < listUsers.length; user++) {
        if (listUsers[user]["mail"] == state["mail"]) {
          mail = true;
        }
      }
      if (mail) {
        setError("Cette adresse mail est déjà utilisée pour un autre compte.");
      }
      else {
        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/modif";

        // Envoi de la requête
        var axios_resp = axios.post(url, state)
          // Récupération de la réponse
          .then(function (response) {
            if (response.status == 200) {
              setValidationPost(true);
            }
          })
          .catch(function (error) {
            console.log(error);
          });

        if (validationPost) {
          document.location.href = "success_modif_perso";
        }
        else {
          setError("Un problème est apparu lors de la modification.");
        }
      }
    }
  };

  return (
    <BoxContent>
      <Box>
        <Titre_section variant='h5'>Informations personnelles</Titre_section>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Adresse mail :</Texte></Grid>
          <Grid item xs={9}>
            <TextField label={lastMail} sx={{ marginBottom: "10px" }} size="small" onChange={(event) => { setMail(event.target.value); }} />
          </Grid>
        </Grid>
      </Box>
      <Box>
        <Button_form id="button" onClick={() => { Modification({ "username": token, "mail": mail }); }}>Modifier</Button_form>
        <Texte>{error}</Texte>
      </Box>
    </BoxContent>
  );

}