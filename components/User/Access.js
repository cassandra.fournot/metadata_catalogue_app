import React, { useState, useEffect } from "react";
import { Box, Tab, TableContainer, MenuItem, Select, Grid } from "@mui/material";
import { BoxContent, Texte, Button_form } from "../Styles/Styles.js";
import { TabPanel, TabContext, TabList } from '@mui/lab';
import useToken from '../Token/Token';
import Users from '../Listes/Users';
import Documents from '../Listes/AllDocuments';

const axios = require('axios');

export default function Access() {

  const [error, setError] = useState('');
  const [statusUsersAdd, setStatusUsersAdd] = useState(0);
  const [statusDocumentsAdd, setStatusDocumentsAdd] = useState(0);
  const [statusUsersSupp, setStatusUsersSupp] = useState(0);
  const [statusDocumentsSupp, setStatusDocumentsSupp] = useState(0);
  const [listUsers, setListUsers] = useState([]);
  const [listDocuments, setListDocuments] = useState([]);
  const [isInit, setIsInit] = useState(false);
  const [value, setValue] = React.useState('Ajouter');

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      ListUsers();
      ListDocuments();
      setIsInit(true);
    }
  }, [isInit]);

  async function ListUsers() {
    const response = await Users();
    setListUsers(response[0]);
  }

  async function ListDocuments() {
    const response = await Documents();
    setListDocuments(response);
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };



  // Fonction qui permet d'ajouter des accès
  function Ajouter() {
    setError("");
    if (statusUsersAdd == "0") {
      setError("Veuillez saisir un nom d'utilisateur.");
    }
    else if (statusDocumentsAdd == "0") {
      setError("Veuillez saisir l'identifiant d'une base de données.");
    }
    else {
      // Définition de l'URL (pour la requête)
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/list_access";

      // Envoi de la requête
      var axios_resp = axios.get(url)
        // Récupération de la réponse
        .then(function (response) {
          var validation = true;
          for (var doc = 0; doc < Object.keys(response.data).length; doc++) {
            if (response.data[doc]["document_id"] == statusDocumentsAdd && response.data[doc]["username"] == statusUsersAdd) {
              validation = false;
            }
          }
          if (!validation) {
            setError("L'utilisateur a déjà accès à cette base.");
          }
          else {
            // Définition de l'URL (pour la requête)
            var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/ajout_access";

            var modfications = { "document_id": statusDocumentsAdd, "username": statusUsersAdd };
            // Envoi de la requête
            var axios_resp = axios.post(url, modfications)
              // Récupération de la réponse
              .then(function (response) {
                if (response.status == 200) {
                  setValidationPost(true);
                }
              })
              .catch(function (error) {
                console.log(error);
              });

            if (validationPost) {
              document.location.href = "success_access";
            }
            else {
              setError("Un problème est apparu lors de la modification.");
            }
          }

        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  // Fonction qui permet de supprimer des accès
  function Supprimer() {
    setError("");
    if (statusUsersSupp == "0") {
      setError("Veuillez saisir un nom d'utilisateur.");
    }
    else if (statusDocumentsSupp == "0") {
      setError("Veuillez saisir l'identifiant d'une base de données.");
    }
    else {
      // Définition de l'URL (pour la requête)
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/list_access";

      // Envoi de la requête
      var axios_resp = axios.get(url)
        // Récupération de la réponse
        .then(function (response) {
          var validation = true;
          for (var doc = 0; doc < Object.keys(response.data).length; doc++) {
            if (response.data[doc]["document_id"] == statusDocumentsSupp && response.data[doc]["username"] == statusUsersSupp) {
              validation = false;
            }
          }
          if (validation) {
            setError("L'utilisateur n'a pas accès à cette base.");
          }
          else {
            // Définition de l'URL (pour la requête)
            var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/supp_access";

            var modfications = { "document_id": statusDocumentsSupp, "username": statusUsersSupp };
            // Envoi de la requête
            var axios_resp = axios.post(url, modfications)
              // Récupération de la réponse
              .then(function (response) {
                if (response.status == 200) {
                  setValidationPost(true);
                }
              })
              .catch(function (error) {
                console.log(error);
              });

            if (validationPost) {
              document.location.href = "success_access";
            }
            else {
              setError("Un problème est apparu lors de la modification.");
            }
          }

        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };


  return (
    <BoxContent>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} textColor="#036287">
            <Tab label="Ajouter accès" value="Ajouter" />
            <Tab label="Supprimer accès" value="Supprimer" />
          </TabList>
        </Box>
        <TabPanel value="Ajouter">
          <Box>
            <TableContainer>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={2}><Texte>Nom d'utilisateur :</Texte></Grid>
                <Grid item xs={9}>
                  <Select
                    value={statusUsersAdd}
                    onChange={(event) => { setStatusUsersAdd(event.target.value); setStatusDocumentsAdd(0); }}
                    sx={{ width: '200px' }}
                    size="small"
                  >
                    <MenuItem value={0}>Utilisateur</MenuItem>
                    {listUsers.map((e) => {
                      return (
                        <MenuItem value={e.username}>{e.username}</MenuItem>
                      );
                    })}
                  </Select>
                </Grid>
              </Grid>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={2} sx={{ marginTop: "20px" }}><Texte>Identifiant de la base :</Texte></Grid>
                <Grid item xs={9}>
                  <Select
                    value={statusDocumentsAdd}
                    onChange={(event) => { setStatusDocumentsAdd(event.target.value); }}
                    sx={{ width: '200px', marginTop: '10px' }}
                    size="small"
                  >
                    <MenuItem value={0}>ID Base</MenuItem>
                    {listDocuments.map((e) => {
                      return (
                        <MenuItem value={e.id}>{e.id}</MenuItem>
                      );
                    })}
                  </Select>
                </Grid>
              </Grid>
              <Box>
                <Button_form id="button" onClick={() => Ajouter({ "document_id": statusDocumentsAdd, "username": statusUsersAdd })}>Ajouter l'accès</Button_form>
                <Texte>{error}</Texte>
              </Box>
            </TableContainer>
          </Box>
        </TabPanel>
        <TabPanel value="Supprimer">
          <Box>
            <TableContainer>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={2}><Texte>Nom d'utilisateur :</Texte></Grid>
                <Grid item xs={9}>
                  <Select
                    value={statusUsersSupp}
                    onChange={(event) => { setStatusUsersSupp(event.target.value); setStatusDocumentsSupp(0); }}
                    sx={{ width: '200px' }}
                    size="small"
                  >
                    <MenuItem value={0}>Utilisateur</MenuItem>
                    {listUsers.map((e) => {
                      return (
                        <MenuItem value={e.username}>{e.username}</MenuItem>
                      );
                    })}
                  </Select>
                </Grid>
              </Grid>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={2} sx={{ marginTop: "20px" }}><Texte>Identifiant de la base :</Texte></Grid>
                <Grid item xs={9}>
                  <Select
                    value={statusDocumentsSupp}
                    onChange={(event) => { setStatusDocumentsSupp(event.target.value); }}
                    sx={{ width: '200px', marginTop: '10px' }}
                    size="small"
                  >
                    <MenuItem value={0}>ID Base</MenuItem>
                    {listDocuments.map((e) => {
                      return (
                        <MenuItem value={e.id}>{e.id}</MenuItem>
                      );
                    })}
                  </Select>
                </Grid>
              </Grid>
              <Box>
                <Button_form id="button" onClick={() => Supprimer({ "document_id": statusDocumentsSupp, "username": statusUsersSupp })}>Supprimer l'accès</Button_form>
                <Texte>{error}</Texte>
              </Box>
            </TableContainer>
          </Box>
        </TabPanel>
      </TabContext>
    </BoxContent>
  );

}