import React, { useState, useEffect } from "react";
import { Box, TextField, Grid } from "@mui/material";
import { BoxContent, Texte_justify, Texte, Button_form } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import hasLowerCase from "../Functions/hasLowerCase.js";
import hasUpperCase from "../Functions/hasUpperCase.js";
import hasInt from "../Functions/hasInt.js";
import hasSpe from "../Functions/hasSpe.js";

const axios = require('axios');

export default function ModifPassword() {

  const { token, setToken } = useToken();
  const [error, setError] = useState("");
  const [lastPassword, setLastPassword] = useState("");
  const [lastPasswordSaisi, setLastPasswordSaisi] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [isInit, setIsInit] = useState(false);
  const [validation, setValidation] = useState(false);
  const [validationPost, setValidationPost] = useState([]);

  const [color_nb, setColorNb] = useState("red");
  const [color_min, setColorMin] = useState("red");
  const [color_maj, setColorMaj] = useState("red");
  const [color_int, setColorInt] = useState("red");
  const [color_spe, setColorSpe] = useState("red");

  useEffect(() => {
    if (isInit === false) {
      InfoUser();
    }
  }, [isInit]);

  // Fonction qui vérifie que l'ancien mot de passe saisi est correct
  function Validation(state) {
    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/verif_password";

    // Envoi de la requête
    var axios_resp = axios.post(url, state)
      // Récupération de la réponse
      .then(function (response) {
        if (response.data == true) {
          setValidation(true);
          setError('');
        }
        else {
          setError("Mot de passe incorrect.");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Modification(state) {
    if (state["password"] === state["passwordConfirm"]) {
      if (!state["password"].match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%&_])([-+!*$@%&_\w]{8,50})$/)) {
        setError("Le mot de passe doit contenir respecter les conditions ci-dessus.");
      }
      else {
        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/modif_password";

        // Envoi de la requête
        var axios_resp = axios.post(url, state)
          // Récupération de la réponse
          .then(function (response) {
            if (response.status == 200) {
              setValidationPost(true);
            }
          })
          .catch(function (error) {
            console.log(error);
          });

        if (validationPost) {
          document.location.href = "success_modif_password";
        }
        else {
          setError("Un problème est apparu lors de la modification.");
        }
      }
    }
    else {
      setError("Les deux mots de passe saisis ne sont pas identiques.");
    }
  };

  // Fonction qui récupère les droits de l'utilisateur
  function InfoUser() {
    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/password/" + token;

    // Envoi de la requête
    var axios_resp = axios.post(url)
      // Récupération de la réponse
      .then(function (response) {
        setLastPassword(response.data[0]["password"]);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  // Fonction qui affiche la saisie de l'ancien mot de passe
  function LastPassword() {
    if (validation == false) {
      return (
        <Box>
          <Box>
            <Texte>Pour modifier votre mot de passe, vous devez dans un premier temps saisir votre ancien mot de passe.</Texte>
            <Grid container sx={{ alignItems: "center" }}>
              <Grid item xs={2}><Texte>Ancien mot de passe :</Texte></Grid>
              <Grid item xs={4}>
                <TextField type="password" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => { setLastPasswordSaisi(event.target.value); }} />
              </Grid>
            </Grid>
          </Box>
          <Box>
            <Button_form id="button" onClick={() => { Validation({ "lastPassword": lastPassword, "lastPasswordSaisi": lastPasswordSaisi }); }}>Valider</Button_form>
            <Texte>{error}</Texte>
          </Box>
        </Box>
      );
    }
  }

  // Fonction qui affiche la saisie du nouveau mot de passe
  function NewPassword() {
    if (validation == true) {
      return (
        <Box>
          <Texte_justify>Vous pouvez maintenant saisir votre nouveau mot de passe.</Texte_justify>
          <Texte_justify>Le mot de passe doit contenir respecter les conditions suivantes :</Texte_justify>
          <Texte_justify sx={{ color: color_nb }}>- entre 8 et 50 catactères,</Texte_justify>
          <Texte_justify sx={{ color: color_min }}>- au moins une lettre minuscule,</Texte_justify>
          <Texte_justify sx={{ color: color_maj }}>- au moins une lettre majuscule,</Texte_justify>
          <Texte_justify sx={{ color: color_int }}>- au moins un chiffre</Texte_justify>
          <Texte_justify sx={{ color: color_spe }}>- au moins un de ces caractères spéciaux : & $ @ % * + - _ !</Texte_justify>
          <br></br>
          <Grid container sx={{ alignItems: "center" }}>
            <Grid item xs={3.2}><Texte>Saisir votre nouveau mot de passe :</Texte></Grid>
            <Grid item xs={4}>
              <TextField type="password" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => {
                setPassword(event.target.value);
                if (event.target.value.length >= 8 && event.target.value.length <= 50) {
                  setColorNb("green");
                }
                else {
                  setColorNb("red");
                }
                if (hasLowerCase(event.target.value)) {
                  setColorMin("green");
                }
                else {
                  setColorMin("red");
                }
                if (hasUpperCase(event.target.value)) {
                  setColorMaj("green");
                }
                else {
                  setColorMaj("red");
                }
                if (hasInt(event.target.value)) {
                  setColorInt("green");
                }
                else {
                  setColorInt("red");
                }
                if (hasSpe(event.target.value)) {
                  setColorSpe("green");
                }
                else {
                  setColorSpe("red");
                }
              }} />
            </Grid>
          </Grid>
          <Grid container sx={{ alignItems: "center" }}>
            <Grid item xs={4.05}><Texte>Confirmation de votre nouveau mot de passe :</Texte></Grid>
            <Grid item xs={4}>
              <TextField type="password" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => { setPasswordConfirm(event.target.value); }} />
            </Grid>
          </Grid>
          <Box>
            <Button_form id="button" onClick={() => { Modification({ "username": token, "password": password, "passwordConfirm": passwordConfirm }); }}>Modifier</Button_form>
            {error.split("\n").map((e) => {
              return (<Texte>{e}</Texte>);
            })}
          </Box>
        </Box>
      );
    }
  }

  return (
    <BoxContent>
      {LastPassword()}
      {NewPassword()}
    </BoxContent>
  );

}