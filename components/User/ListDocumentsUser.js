import React, { useEffect, useState } from "react";
import { Box, Tab, TableContainer, Paper, Table, TableRow, TableCell, TableBody, TableFooter, TablePagination } from "@mui/material";
import { Texte, BoxContent, Titre_section } from "../Styles/Styles.js";
import DocumentsUser from '../Listes/DocumentsUser';
import DocumentInProgressUser from '../Listes/DocumentsInProgressUser';
import useToken from '../Token/Token';
import { TabPanel, TabContext, TabList } from '@mui/lab';
import TablePaginationActions from '../Functions/TablePaginationActions';

export default function ListDocumentsUser() {
  const { token, setToken } = useToken();
  const [listDocument, setListDocument] = useState([]);
  const [listDocumentInProgress, setListDocumentInProgress] = useState([]);
  const [isInit, setIsInit] = useState(false);
  const [value, setValue] = React.useState('Bases');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [pageInProgress, setPageInProgress] = useState(0);
  const [rowsPerPageInProgress, setRowsPerPageInProgress] = useState(5);

  useEffect(() => {
    if (isInit === false) {
      ListDocument();
      ListDocumentInProgress();
      setIsInit(true);
    }
  }, [isInit]);

  async function ListDocument() {
    const response = await DocumentsUser(token);
    setListDocument(response);
  }

  async function ListDocumentInProgress() {
    const response = await DocumentInProgressUser(token);
    setListDocumentInProgress(response);
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - listDocument.length) : 0;

  const emptyRowsInProgress =
    pageInProgress > 0 ? Math.max(0, (1 + pageInProgress) * rowsPerPageInProgress - listDocumentInProgress.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangePageInProgress = (event, newPage) => {
    setPageInProgress(newPage);
  };

  const handleChangeRowsPerPageInProgress = (event) => {
    setRowsPerPageInProgress(parseInt(event.target.value, 10));
    setPageInProgress(0);
  };

  return (
    <BoxContent>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} textColor="#036287">
            <Tab label="Bases" value="Bases" />
            <Tab label="Bases à saisir" value="Bases à saisir" />
          </TabList>
        </Box>
        <TabPanel value="Bases">
          <Box>
            <Titre_section variant='h5' sx={{ marginTop: "10px" }}>Liste des bases que vous pouvez modifier : </Titre_section>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableBody>
                  {(rowsPerPage > 0
                    ? listDocument.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    : listDocument
                  ).map((row) => {
                    return (
                      <TableRow
                        key={row.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        onClick={() => window.location.href = "/base?base=" + row.id}

                      >
                        <TableCell component="th" scope="row">
                          <Texte sx={{ marginBottom: '0px' }}><strong>{row.id}</strong></Texte>
                          <br></br>
                          {row.description}
                        </TableCell>
                        <TableCell align="right">{row.document}</TableCell>
                      </TableRow>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={listDocument.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      SelectProps={{
                        inputProps: {
                          'aria-label': 'rows per page',
                        },
                        native: true,
                      }}
                      onPageChange={handleChangePage}
                      onRowsPerPageChange={handleChangeRowsPerPage}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Box>
        </TabPanel>
        <TabPanel value="Bases à saisir">
          <Box>
            <Titre_section variant='h5' sx={{ marginTop: "10px" }}>Liste des formulaire en cours : </Titre_section>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableBody>
                  {(rowsPerPageInProgress > 0
                    ? listDocumentInProgress.slice(pageInProgress * rowsPerPageInProgress, pageInProgress * rowsPerPageInProgress + rowsPerPageInProgress)
                    : listDocumentInProgress
                  ).map((row) => {
                    return (
                      <TableRow
                        key={row.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        onClick={() => window.location.href = "/edit_base_in_progress?" + row.id}

                      >
                        <TableCell component="th" scope="row">
                          <Texte sx={{ marginBottom: '0px' }}><strong>{row.id}</strong></Texte>
                          <br></br>
                          {row.description}
                        </TableCell>
                        <TableCell align="right">{row.document}</TableCell>
                      </TableRow>
                    );
                  })}
                  {emptyRowsInProgress > 0 && (
                    <TableRow style={{ height: 53 * emptyRowsInProgress }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={listDocumentInProgress.length}
                      rowsPerPage={rowsPerPageInProgress}
                      page={pageInProgress}
                      SelectProps={{
                        inputProps: {
                          'aria-label': 'rows per page',
                        },
                        native: true,
                      }}
                      onPageChange={handleChangePageInProgress}
                      onRowsPerPageChange={handleChangeRowsPerPageInProgress}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Box>
        </TabPanel>
      </TabContext>
    </BoxContent>
  );
}