import React, { useState, useEffect } from "react";
import { Box, TextField, MenuItem, Select, Grid } from "@mui/material";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { BoxContent, Texte_justify, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import verificationEmail from "../Functions/verificationEmail.js";

const axios = require('axios');

export default function NewUser() {

  const [error, setError] = useState("");
  const [mail, setMail] = useState("");
  const [status, setStatus] = useState(0);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Add(state) {
    if (!verificationEmail(state["mail"])) {
      setError("Merci de saisir une adresse mail valide.");
    }
    else {
      // Définition de l'URL (pour la requête)
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/list_users";

      // Envoi de la requête
      var axios_resp = axios.get(url)
        // Récupération de la réponse
        .then(function (response) {
          var list_users = response.data;
          var username = false;
          var mail = false;
          for (var user = 0; user < list_users.length; user++) {
            if (list_users[user]["mail"] == state["mail"]) {
              mail = true;
            }
          }

          if (mail) {
            setError("Cette adresse mail est déjà utilisée pour un autre compte.");
          }
          else {
            // Définition de l'URL (pour la requête)
            var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/new_user";

            // Envoi de la requête
            var axios_resp = axios.post(url, state)
              // Récupération de la réponse
              .then(function (response) {
                if (response.status == 200) {
                  setValidationPost(true);
                }
              })
              .catch(function (error) {
                console.log(error);
              });

            if (validationPost) {
              document.location.href = "success_user";
            }
            else {
              setError("Un problème est apparu lors de l'insertion.");
            }
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  return (
    <BoxContent>
      <Texte_justify>Merci de remplir ce formulaire pour ajouter un nouvel utilisateur. Les utilisateurs sont des utilisateurs simples par défaut mais vous pouvez leur attribuer un status d'administrateur pour qu'ils aient accès à la gestion des utilisateurs et à la suppression des métadonnées.</Texte_justify>
      <Box>
        <Titre_section variant='h5' sx={{ marginTop: "20px" }}>Informations sur l'utilisateur</Titre_section>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Adresse mail :</Texte></Grid>
          <Grid item xs={9}>
            <TextField required label="Adresse mail" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => { setMail(event.target.value); }} />
          </Grid>
        </Grid>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Status :</Texte></Grid>
          <Grid item xs={9}>
            <Select
              value={status}
              onChange={(event) => { setStatus(event.target.value); }}
              size="small"
            >
              <MenuItem value={1}>Administateur</MenuItem>
              <MenuItem value={0}>Utilisateur</MenuItem>
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box>
        <Button_form id="button" onClick={() => { Add({ "username": mail, "mail": mail, "status": status }); }}>Valider</Button_form>
        {error.split("\n").map((e) => {
          return (<Texte>{e}</Texte>);
        })}
      </Box>
    </BoxContent>
  );
}