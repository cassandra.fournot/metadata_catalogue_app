import React, { useState, useEffect } from "react";
import { Box, FormControl, InputLabel, MenuItem, Select, Grid } from "@mui/material";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { BoxContent, Titre_section_form, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import Users from '../Listes/Users';
import Documents from '../Listes/AllDocuments';

const axios = require('axios');

export default function SuppUser() {

  const { token, setToken } = useToken();
  const [error, setError] = useState('');
  const [statusUsers, setStatusUsers] = useState(0);
  const [listUsers, setListUsers] = useState([]);
  const [listDocuments, setListDocuments] = useState([]);
  const [statusToken, setStatusToken] = useState(0);
  const [isInit, setIsInit] = useState(false);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      ListUsers();
      ListDocuments();
      setIsInit(true);
    }
  }, [isInit]);

  async function ListUsers() {
    const response = await Users();
    setListUsers(response[0]);
  }

  async function ListDocuments() {
    const response = await Documents();
    setListDocuments(response);
  }

  // Fonction qui permet de sauvegarder les modifications des accèss
  function Suppression(modifications) {
    setError("");
    if (statusUsers == "0") {
      setError("Veuillez saisir un nom d'utilisateur.");
    }
    else {
      // Définition de l'URL (pour la requête)
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/supp_user";

      // Envoi de la requête
      var axios_resp = axios.post(url, modifications)
        // Récupération de la réponse
        .then(function (response) {
          if (response.status == 200) {
            setValidationPost(true);
          }

        })
        .catch(function (error) {
          console.log(error);
        });

      if (validationPost) {
        document.location.href = "success_supp";
      }
      else {
        setError("Un problème est apparu lors de la suppression.");
      }

    }
  };

  return (
    <BoxContent>
      <Box>
        <Titre_section variant='h5'>Informations sur l'utilisateur</Titre_section>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Nom d'utilisateur :</Texte></Grid>
          <Grid item xs={9}>
            <Select
              value={statusUsers}
              onChange={(event) => { setStatusUsers(event.target.value); }}
              sx={{ width: '200px' }}
              size="small"
            >
              <MenuItem value={0}>Utilisateur</MenuItem>
              {listUsers.map((e) => {
                return (
                  <MenuItem value={e.username}>{e.username}</MenuItem>
                );
              })}
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box>
        <Button_form id="button" onClick={() => Suppression({ "username": statusUsers })}>Valider</Button_form>
        <Texte>{error}</Texte>
      </Box>
    </BoxContent>
  );
}