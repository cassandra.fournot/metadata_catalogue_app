import React, { useState, useEffect } from "react";
import { Box, TableContainer, Paper, Table, TableRow, TableCell, TableBody, TableHead, Stack, Tab, TableFooter, TablePagination } from "@mui/material";
import { BoxContent, LinkNav, Titre_section, Texte } from "../Styles/Styles.js";
import { TabPanel, TabContext, TabList } from '@mui/lab';
import useToken from '../Token/Token';
import Users from "../Listes/Users";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import Access from '../Listes/Access';
import { grey } from '@mui/material/colors';
import TablePaginationActions from '../Functions/TablePaginationActions';

export default function User() {

  const { token, setToken } = useToken();
  const [isInit, setIsInit] = useState(0);
  const [user, setUser] = useState([]);
  const [listUsers, setListUsers] = useState(0);
  const [listAccess, setListAccess] = useState([]);
  const [status, setStatus] = useState("");
  const [value, setValue] = React.useState('Gestion');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
    if (isInit == false) {
      ListUsers();
      ListAccess();
      setIsInit(true);
    }
  }, [isInit]);

  async function ListUsers() {
    const response = await Users();
    setListUsers(response[0]);
    for (var user = 0; user < response[0].length; user++) {
      if (response[0][user]["username"] == token) {
        setUser(response[0][user]);
        setStatus(response[0][user]["status"]);
      }
    }
  }

  async function ListAccess() {
    const response = await Access(token);
    setListAccess(response);
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // Fonction qui retourne le status des utilisateurs
  function returnStatus(status) {
    if (status == 1) {
      return "Administrateur";
    }
    else {
      return "Utilisateur";
    }
  }

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - listAccess.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // Fonction qui affiche les informations accessibles aux administrateurs seulement
  function verifStatusModif(status) {
    if (status == 1) {
      return (
        <Box sx={{ marginTop: 5 }}>
          <Titre_section variant='h5' sx={{ marginTop: "20px" }}>Gestion des utilisateurs</Titre_section>
          <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabList onChange={handleChange} textColor="#036287">
                <Tab label="Gestion utilisateurs" value="Gestion" />
                <Tab label="Accès utilisateurs" value="Accès" />
              </TabList>
            </Box>
            <TabPanel value="Gestion">
              <Box>
                <Stack direction='row'>
                  <LinkNav to="/new_user" sx={{ marginLeft: 0 }}>Ajouter un nouvel utilisateur</LinkNav>
                  <LinkNav to="/modif_user" sx={{ marginLeft: 5 }}>Modifier le status d'un utilisateur</LinkNav>
                  <LinkNav to="/supp_user" sx={{ marginLeft: 5 }}>Supprimer un utilisateur</LinkNav>
                </Stack>
              </Box>
            </TabPanel>
            <TabPanel value="Accès">
              <Box>
                <LinkNav to="/access" sx={{ marginLeft: 0 }}>Modifier les accès des utilisateurs</LinkNav>
                <TableContainer component={Paper} sx={{ marginTop: 2 }}>
                  <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                      <TableRow sx={{ backgroundColor: grey[400] }}>
                        <TableCell>Identifiant base</TableCell>
                        <TableCell align="center">Nom d'utilisateur</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {(rowsPerPage > 0
                        ? listAccess.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : listAccess
                      ).map((row) => {
                        return (
                          <TableRow
                            key={row.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                          >
                            <TableCell sx={{ marginBottom: '0px' }}><strong>{row.document}</strong></TableCell>
                            <TableCell align="center">{row.username}</TableCell>
                          </TableRow>
                        );
                      })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    <TableFooter>
                      <TableRow>
                        <TablePagination
                          rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                          colSpan={3}
                          count={listAccess.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          SelectProps={{
                            inputProps: {
                              'aria-label': 'rows per page',
                            },
                            native: true,
                          }}
                          onPageChange={handleChangePage}
                          onRowsPerPageChange={handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActions}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </TableContainer>
              </Box>
            </TabPanel>
          </TabContext>
        </Box>
      );
    }
  }

  return (
    <BoxContent>
      <MuiThemeProvider>
        <Titre_section variant='h5'>Informations personnelles</Titre_section>
        <Texte>Nom d'utilisateur : {user['username']}</Texte>
        <Texte>Adresse mail : {user['mail']}</Texte>
        <Texte>Status : {returnStatus(user["status"])}</Texte>
        <br></br>
        <Box><LinkNav to="/modif_perso" sx={{ marginLeft: 0 }}>Modifier mes informations personnelles</LinkNav></Box>
        <Box sx={{ marginTop: 1 }}><LinkNav to="/modif_password" sx={{ marginLeft: 0 }}>Modifier mon mot de passe</LinkNav></Box>
      </MuiThemeProvider>
      {verifStatusModif(status)}
    </BoxContent>
  );
}