import React, { useEffect, useState } from "react";
import { Box, Tab, TableContainer, Paper, Table, TableRow, TableCell, TableBody, TableHead, TableFooter, TablePagination } from "@mui/material";
import { BoxContent, Titre_section } from "../Styles/Styles.js";
import Historique from '../Listes/Historique';
import { TabPanel, TabContext, TabList } from '@mui/lab';
import { grey } from '@mui/material/colors';
import TablePaginationActions from '../Functions/TablePaginationActions';

export default function ListDocumentsUser() {
  const [insertions, setInsertions] = useState([]);
  const [modifications, setModifications] = useState([]);
  const [suppressions, setSuppressions] = useState([]);
  const [isInit, setIsInit] = useState(false);
  const [value, setValue] = React.useState('Insertions');
  const [pageInsert, setPageInsert] = useState(0);
  const [rowsPerPageInsert, setRowsPerPageInsert] = useState(5);
  const [pageModif, setPageModif] = useState(0);
  const [rowsPerPageModif, setRowsPerPageModif] = useState(5);
  const [pageSupp, setPageSupp] = useState(0);
  const [rowsPerPageSupp, setRowsPerPageSupp] = useState(5);

  useEffect(() => {
    if (isInit === false) {
      Insertions();
      Modifications();
      Suppressions();
      setIsInit(true);
    }
  }, [isInit]);

  async function Insertions() {
    const response = await Historique();
    setInsertions(response.insertion);
  }

  async function Modifications() {
    const response = await Historique();
    setModifications(response.modification);
  }

  async function Suppressions() {
    const response = await Historique();
    setSuppressions(response.suppression);
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const emptyRowsInsert =
    pageInsert > 0 ? Math.max(0, (1 + pageInsert) * rowsPerPageInsert - insertions.length) : 0;

  const handleChangePageInsert = (event, newPage) => {
    setPageInsert(newPage);
  };

  const handleChangeRowsPerPageInsert = (event) => {
    setRowsPerPageInsert(parseInt(event.target.value, 10));
    setPageInsert(0);
  };

  const emptyRowsModif =
    pageModif > 0 ? Math.max(0, (1 + pageModif) * rowsPerPageModif - modifications.length) : 0;

  const handleChangePageModif = (event, newPage) => {
    setPageModif(newPage);
  };

  const handleChangeRowsPerPageModif = (event) => {
    setRowsPerPageModif(parseInt(event.target.value, 10));
    setPageModif(0);
  };

  const emptyRowsSupp =
    pageSupp > 0 ? Math.max(0, (1 + pageSupp) * rowsPerPageSupp - suppressions.length) : 0;

  const handleChangePageSupp = (event, newPage) => {
    setPageSupp(newPage);
  };

  const handleChangeRowsPerPageSupp = (event) => {
    setRowsPerPageSupp(parseInt(event.target.value, 10));
    setPageSupp(0);
  };
  return (
    <BoxContent>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} textColor="#036287">
            <Tab label="Insertions" value="Insertions" />
            <Tab label="Modifications" value="Modifications" />
            <Tab label="Suppressions" value="Suppressions" />
          </TabList>
        </Box>
        <TabPanel value="Insertions">
          <Box>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow sx={{ backgroundColor: grey[400] }}>
                    <TableCell>Identifiant base</TableCell>
                    <TableCell align="center">Nom d'utilisateur</TableCell>
                    <TableCell align="center">Date</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowsPerPageInsert > 0
                    ? insertions.slice(pageInsert * rowsPerPageInsert, pageInsert * rowsPerPageInsert + rowsPerPageInsert)
                    : insertions
                  ).map((row) => (
                    <TableRow
                      key={row.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell sx={{ marginBottom: '0px' }}><strong>{row.base}</strong></TableCell>
                      <TableCell align="center">{row.username}</TableCell>
                      <TableCell align="center">{row.date.replace("T", " ")}</TableCell>
                    </TableRow>
                  ))}
                  {emptyRowsInsert > 0 && (
                    <TableRow style={{ height: 53 * emptyRowsInsert }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={insertions.length}
                      rowsPerPage={rowsPerPageInsert}
                      page={pageInsert}
                      SelectProps={{
                        inputProps: {
                          'aria-label': 'rows per page',
                        },
                        native: true,
                      }}
                      onPageChange={handleChangePageInsert}
                      onRowsPerPageChange={handleChangeRowsPerPageInsert}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Box>
        </TabPanel>
        <TabPanel value="Modifications">
          <Box>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow sx={{ backgroundColor: grey[400] }}>
                    <TableCell>Identifiant base</TableCell>
                    <TableCell align="center">Nom d'utilisateur</TableCell>
                    <TableCell align="center">Date</TableCell>
                    <TableCell align="center">Champ modifié</TableCell>
                    <TableCell align="center">Modification</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowsPerPageModif > 0
                    ? modifications.slice(pageModif * rowsPerPageModif, pageModif * rowsPerPageModif + rowsPerPageModif)
                    : modifications
                  ).map((row) => (
                    <TableRow
                      key={row.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell sx={{ marginBottom: '0px' }}><strong>{row.base}</strong></TableCell>
                      <TableCell align="center">{row.username}</TableCell>
                      <TableCell align="center">{row.date.replace("T", " ")}</TableCell>
                      <TableCell align="center">{row.input}</TableCell>
                      <TableCell align="center">{row.modification}</TableCell>
                    </TableRow>
                  ))}
                  {emptyRowsModif > 0 && (
                    <TableRow style={{ height: 53 * emptyRowsModif }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={modifications.length}
                      rowsPerPage={rowsPerPageModif}
                      page={pageModif}
                      SelectProps={{
                        inputProps: {
                          'aria-label': 'rows per page',
                        },
                        native: true,
                      }}
                      onPageChange={handleChangePageModif}
                      onRowsPerPageChange={handleChangeRowsPerPageModif}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Box>
        </TabPanel>
        <TabPanel value="Suppressions">
          <Box>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow sx={{ backgroundColor: grey[400] }}>
                    <TableCell>Identifiant base</TableCell>
                    <TableCell align="center">Nom d'utilisateur</TableCell>
                    <TableCell align="center">Date</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(rowsPerPageSupp > 0
                    ? suppressions.slice(pageSupp * rowsPerPageSupp, pageSupp * rowsPerPageSupp + rowsPerPageSupp)
                    : suppressions
                  ).map((row) => (
                    <TableRow
                      key={row.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell sx={{ marginBottom: '0px' }}><strong>{row.base}</strong></TableCell>
                      <TableCell align="center">{row.username}</TableCell>
                      <TableCell align="center">{row.date.replace("T", " ")}</TableCell>
                    </TableRow>
                  ))}
                  {emptyRowsSupp > 0 && (
                    <TableRow style={{ height: 53 * emptyRowsSupp }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                      colSpan={3}
                      count={suppressions.length}
                      rowsPerPage={rowsPerPageSupp}
                      page={pageSupp}
                      SelectProps={{
                        inputProps: {
                          'aria-label': 'rows per page',
                        },
                        native: true,
                      }}
                      onPageChange={handleChangePageSupp}
                      onRowsPerPageChange={handleChangeRowsPerPageSupp}
                      ActionsComponent={TablePaginationActions}
                    />
                  </TableRow>
                </TableFooter>
              </Table>
            </TableContainer>
          </Box>
        </TabPanel>
      </TabContext>
    </BoxContent>
  );
}
