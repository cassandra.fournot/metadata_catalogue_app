import React, { useState, useEffect } from "react";
import FirstConnexion from "../User/FirstConnexion";
import Top from "../Styles/Top";
import { Page, BoxContent } from "../Styles/Styles.js";
import verificationEmail from "../Functions/verificationEmail.js";
import extractParamsUrl from "../Functions/extractParamsUrl.js";

export default function UserPage() {

    const [statusToken, setStatusToken] = useState(0);
    const [isInit, setIsInit] = useState(false);

    // Fonction qui récupère les droits de l'utilisateur
    function StatusToken() {
        if (!extractParamsUrl(window.location.href)) {
            setStatusToken(0);
        }
        else {
            if (verificationEmail(Object.keys(extractParamsUrl(window.location.href))[0])) {
                setStatusToken(1);
            }
        }
    }

    useEffect(() => {
        if (isInit === false) {
            StatusToken();
            setIsInit(true);
        }
    }, [isInit]);

    if (statusToken == 1) {
        return (
            <Page>
                <Top></Top>
                <FirstConnexion />
            </Page>
        );
    }
    else {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <h1>Vous n'avez pas les droits pour accéder à cette page.</h1>
                </BoxContent>
            </Page>
        );
    }

}
