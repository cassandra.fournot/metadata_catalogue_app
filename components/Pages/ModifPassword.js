import React from "react";
import ModifPassword from "../User/ModifPassword";
import Login from "../Login/Login";
import Top from "../Styles/Top";
import { Page } from "../Styles/Styles.js";
import useToken from '../Token/Token';

export default function UserPage() {

    const { token, setToken } = useToken();

    if (!token) {
        return (
            <Page>
                <Top></Top>
                <Login setToken={setToken} />
            </Page>
        );
    }

    return (
        <Page>
            <Top></Top>
            <ModifPassword />
        </Page>
    );

}
