import React from "react"
import User from "../User/User";
import {Page} from "../Styles/Styles.js";
import Login from "../Login/Login";
import Top from "../Styles/Top";
import useToken from '../Token/Token';

export default function UserPage() {

    const { token, setToken } = useToken();
    
    if(!token) {
        return (
            <Page>
                <Top></Top>
                <Login setToken={setToken}/>
            </Page>
        );
      }

    return (
            <Page>
                <Top></Top>
                <User />
            </Page>
    )
    
}
