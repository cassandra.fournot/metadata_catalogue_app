import React from "react";
import LostPassword from "../Login/LostPassword";
import Top from "../Styles/Top";
import { Page } from "../Styles/Styles.js";

export default function UserPage() {

    return (
        <Page>
            <Top></Top>
            <LostPassword />
        </Page>
    );

}
