import React from "react";
import Login from "../Login/Login";
import useToken from '../Token/Token';
import Top from '../Styles/Top';
import { Page } from "../Styles/Styles.js";

export default function Log() {

  const { token, setToken } = useToken();

  return (
    <Page>
      <Top></Top>
      <Login setToken={setToken} />
    </Page>

  );
}
