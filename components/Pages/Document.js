import React from "react";
import DocumentStep1 from "../Document/DocumentStep1";
import DocumentStep2 from "../Document/DocumentStep2";
import DocumentStep3 from "../Document/DocumentStep3";
import Top from "../Styles/Top";
import { Page, BoxContent } from "../Styles/Styles.js";

export default function Document() {
    // Récupération de l'url
    var url = window.location.href;

    if (url.includes("variable")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent><DocumentStep3 /></BoxContent>
            </Page>
        );
    }
    else if (url.includes("table")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent><DocumentStep2 /></BoxContent>
            </Page>
        );
    }
    else if (url.includes("base")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent><DocumentStep1 /></BoxContent>
            </Page>
        );
    }
}
