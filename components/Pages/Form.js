import React from "react";
import NewDocument from "../Form/NewDocument";
import EditDocument from "../Form/EditDocument";
import EditDocumentInProgress from "../Form/EditDocumentInProgress";
import { Page } from "../Styles/Styles.js";
import Login from "../Login/Login";
import Top from "../Styles/Top";
import useToken from '../Token/Token';
import EditDocumentList from '../Listes/EditDocument';
import EditDocumentListInProgress from '../Listes/EditDocumentInProgress';

// Fonction qui permet d'extraire les paramètres d'une url
function extractParamsUrl(url) {
    url = url.split('.+/');
    var result = {};

    url.forEach(function (el) {
        var param = el.split('?');
        param[0] = param[0];
        result[param[0]] = param[1];
    });

    return result;
}

export default function Document() {
    // Récupération de l'url
    var url = window.location.href;

    const { token, setToken } = useToken();


    window.sessionStorage.setItem("Step2", -1);
    window.sessionStorage.setItem("currentStep2", -1);
    window.sessionStorage.setItem("ListStep2", -1);
    window.sessionStorage.setItem("Step3", -1);
    window.sessionStorage.setItem("currentStep3", -1);
    window.sessionStorage.setItem("ListStep3", -1);
    window.sessionStorage.setItem("State", JSON.stringify({}));

    // Fonction qui permet de récupérer les informations des formulaires pour les modifiées
    async function recupLists() {
        var id_document = extractParamsUrl(window.location.href)[Object.keys(extractParamsUrl(window.location.href))[0]];
        const response = await EditDocumentList(id_document);
        window.sessionStorage.setItem("Step2", JSON.stringify(response['Step2']));
        window.sessionStorage.setItem("currentStep2", -1);
        window.sessionStorage.setItem("ListStep2", JSON.stringify(response['ListStep2']));
        window.sessionStorage.setItem("Step3", JSON.stringify(response['Step3']));
        window.sessionStorage.setItem("currentStep3", -1);
        window.sessionStorage.setItem("ListStep3", JSON.stringify(response['ListStep3']));
    }

    // Fonction qui permet de récupérer les informations des formulaires sauvegardés
    async function recupListsInProgress() {
        var id_document = extractParamsUrl(window.location.href)[Object.keys(extractParamsUrl(window.location.href))[0]];
        const response = await EditDocumentListInProgress(id_document);
        window.sessionStorage.setItem("Step2", JSON.stringify(response['Step2']));
        window.sessionStorage.setItem("currentStep2", -1);
        window.sessionStorage.setItem("ListStep2", JSON.stringify(response['ListStep2']));
        window.sessionStorage.setItem("Step3", JSON.stringify(response['Step3']));
        window.sessionStorage.setItem("currentStep3", -1);
        window.sessionStorage.setItem("ListStep3", JSON.stringify(response['ListStep3']));
    }

    if (!token) {
        return (
            <Page>
                <Top></Top>
                <Login setToken={setToken} />
            </Page>
        );
    }

    if (url.includes("new")) {
        return (
            <Page>
                <Top></Top>
                <NewDocument />
            </Page>
        );
    }
    else if (url.includes("edit_base_in_progress")) {
        recupListsInProgress();
        return (
            <Page>
                <Top></Top>
                <EditDocumentInProgress />
            </Page>
        );
    }
    else if (url.includes("edit")) {
        recupLists();
        return (
            <Page>
                <Top></Top>
                <EditDocument />
            </Page>
        );
    }
}
