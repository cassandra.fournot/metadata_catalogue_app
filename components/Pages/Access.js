import React, { useState, useEffect } from "react";
import Access from "../User/Access";
import Login from "../Login/Login";
import Top from "../Styles/Top";
import { Page, BoxContent } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import StatusToken from "../Functions/StatusToken";

export default function UserPage() {

    const { token, setToken } = useToken();
    const [statusToken, setStatusToken] = useState(0);
    const [isInit, setIsInit] = useState(false);

    useEffect(() => {
        if (isInit === false) {
            StatusToken(token, setStatusToken);
            setIsInit(true);
        }
    }, [isInit]);

    if (!token) {
        return (
            <Page>
                <Top></Top>
                <Login setToken={setToken} />
            </Page>
        );
    }

    if (statusToken == 1) {
        return (
            <Page>
                <Top></Top>
                <Access />
            </Page>
        );
    }
    else {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <h1>Vous n'avez pas les droits pour modifier les accèss des utilisateurs.</h1>
                </BoxContent>
            </Page>
        );
    }
}
