import React from "react";
import Export from "../Document/Export";
import Top from "../Styles/Top";
import { Page } from "../Styles/Styles.js";

export default function Exporter() {

    return (
        <Page>
            <Top></Top>
            <Export />
        </Page>
    );

}
