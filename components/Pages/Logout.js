import React from "react";
import Logout from "../Login/Logout";
import useToken from '../Token/Token';
import Top from '../Styles/Top';
import { Page } from "../Styles/Styles.js";

export default function Log() {

  const { token, setToken } = useToken();

  return (
    <Page>
      <Top></Top>
      <Logout setToken={setToken} />
    </Page>

  );
}
