import React from 'react';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { Page, BoxContent } from "../Styles/Styles.js";
import Top from "../Styles/Top";

export default function SuccessPage() {
    // Récupération de l'url
    var url = window.location.href;

    if (url.includes("success_save")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Les données ont bien été sauvegardées.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_user_modif")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Les droits de l'utilisateur ont bien été modifiés.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_user")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>L'utilisateur a bien été ajouté.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_access")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Les modifications d'accès ont été prises en compte.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_modif_perso")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Vos informations personnelles ont bien été modifiés.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_modif_password")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Votre mot de passe a bien été modifié.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_supp")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>La suppression a été prise en compte.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_modif")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Les données ont bien été modifiées.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else if (url.includes("success_first")) {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Votre compte a bien été créé.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
    else {
        return (
            <Page>
                <Top></Top>
                <BoxContent>
                    <MuiThemeProvider>
                        <h1>Les données ont bien été ajoutées.</h1>
                    </MuiThemeProvider>
                </BoxContent>
            </Page>
        );
    }
}
