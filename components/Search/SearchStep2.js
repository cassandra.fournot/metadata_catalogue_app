import React, { useEffect, useState } from "react";
import { Box, Paper, Stack, FormControl, OutlinedInput, Table, TableBody, TableCell, TableContainer, TableFooter, TablePagination, TableRow } from "@mui/material";
import { LinkNav, Titre_section, Texte, Input_search, Button_search, Select_search } from "../Styles/Styles.js";
import TablePaginationActions from '../Functions/TablePaginationActions';
import AllDocuments from '../Listes/AllDocumentsStep2';
import DocumentsSearch from '../Listes/DocumentsSearchStep2';

// Fonction qui permet d'extraire les paramètres d'une url
function extractParamsUrl(url) {
  url = url.split('?');
  url = url[1].split('&');
  var result = {};

  url.forEach(function (el) {
    var param = el.split('=');
    param[0] = param[0];
    result[param[0]] = param[1];
  });

  return result;
}


export default function SearchPage({ step1 }) {
  const [listDocumentAll, setListDocumentAll] = useState([]);
  const [listDocument, setListDocument] = useState([]);
  const [input, setInput] = useState("");
  const [inputLink, setInputLink] = useState("");
  const [isInit, setIsInit] = useState(false);
  const [selectID, setSelectID] = React.useState([]);
  const [options, setOptions] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [rows, setRows] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      // Récupération de l'url
      var url = window.location.href;
      if (url.includes('input')) {
        setInput(extractParamsUrl(url)['input']);
        setInputLink("input=" + extractParamsUrl(url)['input'] + "&");
      }
      ListDocumentAll();
      setIsInit(true);
    }
  }, [isInit]);

  const handleChange = (event) => {
    setSelectID(event);
    if (event.length > 0) {
      var newrows = [];
      for (var doc = 0; doc < listDocument.length; doc++) {
        if (event.includes(listDocument[doc]["id"])) {
          newrows.push(listDocument[doc]);
        }
      }
      setRows(newrows);
    }
    else {
      setRows(listDocument);
    }
  };

  async function ListDocument(input) {
    const response = await DocumentsSearch(input, selectID, step1);
    setListDocument(response);
    setRows(response);
  }

  async function ListDocumentAll() {
    const response = await AllDocuments(step1);
    setListDocumentAll(response);
    setListDocument(response);
    setRows(response);
    for (var opt = 0; opt < response.length; opt++) {
      options.push({
        value: response[opt]["id"],
        label: response[opt]["id"]
      });
    }
  }

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Box>
      <Box>
        <Stack direction="row">
          <Input_search id="Search_input" sx={{ width: "80%" }} placeholder="Saisir un mot ou une phrase" onChange={() => {
            var input = "input=" + document.getElementById("Search_input").value + "&";
            if (document.getElementById("Search_input").value != "") {
              setInputLink(input);
            }
            else {
              setInputLink("");
            }
            if (document.getElementById("Search_input").value != "") {
              ListDocument(document.getElementById("Search_input").value);
            }
            else {
              ListDocumentAll();
            }
          }} />
          <Button_search variant="container" onClick={() => {
            if (document.getElementById("Search_input").value != "") {
              ListDocument(document.getElementById("Search_input").value);
            }
            else {
              ListDocumentAll();
            }
          }}> Recherche</Button_search>
        </Stack>
        <FormControl sx={{ m: 1, width: 300 }}>
          <Select_search
            label="Sélectionner une ou plusieurs table(s)"
            multiple
            value={selectID}
            onChange={handleChange}
            input={<OutlinedInput label="Tag" />}
            renderValue={(selected) => selected.map((x) => x.id).join(', ')}
            SelectProps={{
              isCreatable: true,
              isClearable: true,
              isSearchable: true
            }}
            options={options}
            size="small"
          >
          </Select_search>
        </FormControl>
      </Box>
      <Box>
        <Titre_section variant="h5" sx={{ marginTop: '20px' }}>Réponse</Titre_section>
        <Texte>Nombre de tables trouvées : {listDocument.length}</Texte>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
            <TableBody>
              {(rowsPerPage > 0
                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : rows
              ).map((row) => {
                return (
                  <TableRow
                    key={row.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    onClick={() => window.location.href = "/table?" + inputLink + "base=" + step1 + "&table=" + row.document.split("#")[1]}

                  >
                    <TableCell component="th" scope="row">
                      <Texte sx={{ marginBottom: '0px' }}><strong>{row.id}</strong></Texte>
                      <br></br>
                      {row.description}
                    </TableCell>
                    <TableCell align="right">{row.document}</TableCell>
                  </TableRow>
                );
              })}

              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[10, 20, 50, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: {
                      'aria-label': 'rows per page',
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
}