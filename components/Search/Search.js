import React, { useEffect, useState } from "react";
import { Box, Paper, Stack, FormControl, OutlinedInput, Table, TableBody, TableCell, TableContainer, TableFooter, TablePagination, TableRow } from '@mui/material';
import { BoxContent, Titre_section, Texte, Input_search, Button_search, Select_search, Texte_justify } from "../Styles/Styles.js";
import TablePaginationActions from '../Functions/TablePaginationActions';
import AllDocuments from '../Listes/AllDocuments';
import DocumentsSearch from '../Listes/DocumentsSearch';

export default function SearchPage() {
  const [listDocumentAll, setListDocumentAll] = useState([]);
  const [listDocument, setListDocument] = useState([]);
  const [input, setInput] = useState("");
  const [isInit, setIsInit] = useState(false);
  const [selectID, setSelectID] = useState([]);
  const [options, setOptions] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [rows, setRows] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      ListDocumentAll();
      setIsInit(true);
    }
  }, [isInit]);

  const handleChange = (event) => {
    setSelectID(event);
    if (event.length > 0) {
      var newrows = [];
      for (var doc = 0; doc < listDocument.length; doc++) {
        if (event.includes(listDocument[doc]["id"])) {
          newrows.push(listDocument[doc]);
        }
      }
      setRows(newrows);
    }
    else {
      setRows(listDocument);
    }
  };

  async function ListDocument(input) {
    const response = await DocumentsSearch(input, selectID);
    setListDocument(response);
    setRows(response);
  }

  async function ListDocumentAll() {
    const response = await AllDocuments();
    response.sort(function (a, b) {
      return a.id.localeCompare(b.id);
    });
    setListDocumentAll(response);
    setListDocument(response);
    setRows(response);
    for (var opt = 0; opt < response.length; opt++) {
      options.push({
        value: response[opt]["id"],
        label: response[opt]["id"]
      });
    }
  }

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <BoxContent>
      <Box>
        <Titre_section variant="h5">Recherche</Titre_section>
        <Stack direction="row" >
          <Input_search id="Search_input" placeholder="Saisir un mot ou une phrase" onChange={() => {
            var input = "input=" + document.getElementById("Search_input").value + "&";
            if (document.getElementById("Search_input").value != "") {
              setInput(input);
            }
            else {
              setInput("");
            }
            if (document.getElementById("Search_input").value != "") {
              ListDocument(document.getElementById("Search_input").value);
            }
            else {
              ListDocumentAll();
            }
          }} />
          <Button_search id="SearchButton" variant="container" onClick={() => {
            if (document.getElementById("Search_input").value != "") {
              ListDocument(document.getElementById("Search_input").value);
            }
            else {
              ListDocumentAll();
            }
          }}>Recherche</Button_search>
        </Stack>
        <FormControl sx={{ mt: 1, width: 500 }}>
          <Select_search
            label="Sélectionner une ou plusieurs base(s)"
            multiple
            value={selectID}
            onChange={handleChange}
            input={<OutlinedInput label="Tag" />}
            renderValue={(selected) => selected.map((x) => x.id).join(', ')}
            SelectProps={{
              isCreatable: true,
              isClearable: true,
              isSearchable: true
            }}
            options={options}
            size="small"
          >
          </Select_search>
        </FormControl>
      </Box>
      <Box sx={{ marginTop: "50px", }}>
        <Titre_section variant="h5" sx={{ marginTop: "20px" }}>Réponse</Titre_section>
        <Texte_justify>Le catalogue de métadonnées est un outil qui vous permet de faire une analyse exploratoire parmi les bases de données mises à disposition par le Health Data Hub (HDH). La liste ci-dessous recense les métadonnées sur chacune des bases présentent au catalogue du HDH, autrement dit vous pouvez y trouver les informations descriptives et statistiques sur les bases ainsi que sur les tables et variables qu’elles contiennent.</Texte_justify>
        <Texte><b>Nombre de bases trouvées : {rows.length}</b></Texte>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
            <TableBody>
              {(rowsPerPage > 0
                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : rows
              ).map((row) => (
                <TableRow
                  key={row.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  onClick={() => window.location.href = "/base?" + input + "base=" + row.id}

                >
                  <TableCell component="th" scope="row">
                    <Texte sx={{ marginBottom: '0px' }}><strong>{row.id}</strong></Texte>
                    <br></br>
                    {row.description}
                  </TableCell>
                  <TableCell align="right">{row.document}</TableCell>
                </TableRow>
              ))}

              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[10, 20, 50, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: {
                      'aria-label': 'rows per page',
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </Box>
    </BoxContent>
  );
}
