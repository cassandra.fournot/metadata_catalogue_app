import React, { useState, useEffect } from 'react';
import { Box } from "@mui/material";
import { BoxContent, Titre_section_form, Texte } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import Resum from './Resum';
import ResumStep2 from './ResumStep2';
import InputsStep1 from './InputsStep1';
import InputsStep2 from './InputsStep2';
import InputsStep3 from './InputsStep3';
import EditDocument from '../Listes/EditDocument';

// Fonction qui permet d'extraire les paramètres d'une url
function extractParamsUrl(url) {
    url = url.split('.+/');
    var result = {};

    url.forEach(function (el) {
        var param = el.split('?');
        param[0] = param[0];
        result[param[0]] = param[1];
    });

    return result;
}

export default function Form() {

    // User et Date
    const { token, setToken } = useToken();
    const [today, setToday] = useState();
    const [historique, setHistorique] = useState({});

    // Listes des champs
    const [inputsStep2, setInputsStep2] = useState([]);
    const [inputsStep3, setInputsStep3] = useState([]);

    // Nombre d'étapes
    const [Step2, setStep2] = useState(0);
    const [currentStep2, setCurrentStep2] = useState(Step2);
    const [ListStep2, setListStep2] = useState([]);
    const [Step3, setStep3] = useState(0);
    const [currentStep3, setCurrentStep3] = useState(Step3);
    const [ListStep3, setListStep3] = useState([]);

    // Etape en cours
    const [state, setState] = useState({});
    const [percent, setPercent] = useState(0);
    const [nbInputs, setNbInputs] = useState(143);
    const [prevState, setPrevState] = useState({});
    const [current, setCurrent] = useState("Resum");
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    // Fonction qui permet de récupérer les champs du formulaire
    async function recupLists() {
        var id_document = extractParamsUrl(window.location.href)[Object.keys(extractParamsUrl(window.location.href))[0]];
        const response = await EditDocument(id_document);
        setInputsStep2(response['Step2']);
        setInputsStep3(response['Step3']);
        setState(response['State']);
        setListStep2(response['ListStep2']);
        setListStep3(response['ListStep3']);

        window.sessionStorage.setItem("Step2", JSON.stringify(response['Step2']));
        window.sessionStorage.setItem("currentStep2", JSON.stringify(response['Step2']));
        window.sessionStorage.setItem("ListStep2", JSON.stringify(response['ListStep2']));
        window.sessionStorage.setItem("Step3", JSON.stringify(response['Step3']));
        window.sessionStorage.setItem("currentStep3", JSON.stringify(response['Step3']));
        window.sessionStorage.setItem("ListStep3", JSON.stringify(response['ListStep3']));
        window.sessionStorage.setItem("State", JSON.stringify(response['State']));

        var nb_step2 = Object.keys(response['ListStep2']).length;
        var nb_step3 = Object.keys(response['ListStep3']).length;
        var nb = 143 + (nb_step2 * 19) + (nb_step3 * 44);
        setNbInputs(nb);
        CalculProgress(response['State']);
    }

    function CalculProgress(state) {
        var keys = Object.keys(state);
        var count = 0;
        for (var k = 0; k < keys.length; k++) {
            if (state[keys[k]] != '') {
                count++;
            }
        }
        var per = (count / nbInputs) * 100;
        setPercent(per);
    }

    useEffect(() => {
        setToday(new Date().toLocaleDateString());
        recupLists();
    }, [isInit]);

    if (current == "Resum") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section_form>Informations principales</Titre_section_form>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} prevState={prevState} setPrevState={setPrevState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step1") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section_form>Informations principales</Titre_section_form>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <InputsStep1 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setState={setState} setCurrent={setCurrent}></InputsStep1>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step2") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section_form>Informations principales</Titre_section_form>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} prevState={prevState} setPrevState={setPrevState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <InputsStep2 state={state} setState={setState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setCurrent={setCurrent} Step2={currentStep2} ListStep2={ListStep2}></InputsStep2>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "ResumStep2") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section_form>Informations principales</Titre_section_form>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} prevState={prevState} setPrevState={setPrevState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={currentStep2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <ResumStep2 state={state} current={current} setCurrent={setCurrent} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} Step2={Step2} currentStep2={currentStep2} setStep2={setStep2} setCurrentStep2={setCurrentStep2} ListStep2={ListStep2} Step3={Step3} ListStep3={ListStep3} setStep3={setStep3} currentStep3={currentStep3} setCurrentStep3={setCurrentStep3}></ResumStep2>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step3") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section_form>Informations principales</Titre_section_form>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} prevState={prevState} setPrevState={setPrevState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <ResumStep2 state={state} current={current} setState={setState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setCurrent={setCurrent} Step2={Step2} currentStep2={currentStep2} setStep2={setStep2} setCurrentStep2={setCurrentStep2} ListStep2={ListStep2} Step3={Step3} ListStep3={ListStep3} setStep3={setStep3} currentStep3={currentStep3} setCurrentStep3={setCurrentStep3}></ResumStep2>
                <InputsStep3 state={state} setState={setState} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setCurrent={setCurrent} Step2={currentStep2} Step3={currentStep3} ListStep3={ListStep3}></InputsStep3>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
}