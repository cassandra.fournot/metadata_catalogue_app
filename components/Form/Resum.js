import React, { useState, useEffect } from 'react';
import { Box, Button } from "@mui/material";
import { Page, Titre_section_form, Texte, Button_form } from "../Styles/Styles.js";
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Login from "../Login/Login";
import useToken from '../Token/Token';
import List_inputs from '../Listes/Inputs';
import convert_object_to_string from '../Functions/convert_object_to_string';
import { Line } from 'rc-progress';

const axios = require('axios');

// Fonction qui permet d'extraire les paramètres d'une url
function extractParamsUrl(url) {
    url = url.split('.+/');
    var result = {};

    url.forEach(function (el) {
        var param = el.split('?');
        param[0] = param[0];
        result[param[0]] = param[1];
    });

    return result;
}

export default function Resum(params) {
    // User et Date
    const { token, setToken } = useToken();
    const [today, setToday] = useState();
    const [historique, setHistorique] = useState({});

    // Listes des champs
    const [inputsStep1Required, setInputsStep1Required] = useState([]);
    const [inputsStep1, setInputsStep1] = useState([]);
    const [inputsStep2Required, setInputsStep2Required] = useState([]);
    const [inputsStep2, setInputsStep2] = useState([]);
    const [inputsStep3Required, setInputsStep3Required] = useState([]);
    const [inputsStep3, setInputsStep3] = useState([]);

    // Variables de vérification
    const [validationPost, setValidationPost] = useState([]);
    const [validationHistorique, setValidationHistorique] = useState([]);
    const [validationSupp, setValidationSupp] = useState([]);

    // Etape en cours
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    // Fonction qui permet de récupérer les champs du formulaire
    async function recupInputs() {
        const inputs = await List_inputs();
        setInputsStep1Required(inputs['Step1_required']);
        setInputsStep1(inputs['Step1']);
        setInputsStep2Required(inputs['Step2_required']);
        setInputsStep2(inputs['Step2']);
        setInputsStep3Required(inputs['Step3_required']);
        setInputsStep3(inputs['Step3']);
    }

    useEffect(() => {
        if (isInit == false) {
            recupInputs();
            setToday(new Date().toLocaleDateString());
            setIsInit(true);
        }
    }, [isInit]);

    // Fonction qui permet d'ajouter une nouvelle base
    function Add(state) {
        var required = true;

        for (var requiredStep1 = 0; requiredStep1 < inputsStep1Required.length; requiredStep1++) {
            if (!convert_object_to_string(state).includes(inputsStep1Required[requiredStep1]["id"])) {
                required = false;
            }
        }
        for (var requiredStep2 = 0; requiredStep2 < inputsStep2Required.length; requiredStep2++) {
            if (!convert_object_to_string(state).includes(inputsStep2Required[requiredStep2]["id"])) {
                required = false;
            }
        }
        for (var requiredStep3 = 0; requiredStep3 < inputsStep3Required.length; requiredStep3++) {
            if (!convert_object_to_string(state).includes(inputsStep3Required[requiredStep3]["id"])) {
                required = false;
            }
        }

        var keys = Object.keys(state);

        // Définition de l'URL
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/read_bases/" + state[keys[0]];

        // Envoi de la requête pour vérification que la base n'existe pas déjà
        var axios_resp = axios.get(url)
            // Récupération de la réponse
            .then(function (response) {
                if (response.data == null) {

                    if (!required) {
                        setError("Il faut au moins une table contenant au moins une variable pour enregistrer.");
                    }
                    else {

                        // Définition de l'URL (pour la requête)
                        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/new_base";

                        // Envoi de la requête
                        var axios_resp = axios.post(url, state)
                            // Récupération de la réponse
                            .then(function (response) {
                                if (response.status == 200) {
                                    setValidationPost(true);
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });

                        // Mise à jour des informations
                        setHistorique(historique["username"] = token);
                        setHistorique(historique["base"] = state[keys[0]]);
                        setHistorique(historique["date"] = new Date());

                        // Définition de l'URL (pour la requête)
                        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/insertion";

                        // Envoi de la requête
                        var axios_resp = axios.post(url, historique)
                            // Récupération de la réponse
                            .then(function (response) {
                                if (response.status == 200) {
                                    setValidationHistorique(true);
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });

                        // Définition de l'URL (pour la requête)
                        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/supp_in_progress";

                        var id_document = state[keys[0]];

                        // Envoi de la requête
                        var axios_resp = axios.post(url, { id_document, token })
                            // Récupération de la réponse
                            .then(function (response) {
                                if (response.status == 200) {
                                    setValidationSupp(true);
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });

                        if (validationPost && validationHistorique && validationSupp) {
                            window.sessionStorage.setItem("Step2", -1);
                            window.sessionStorage.setItem("currentStep2", -1);
                            window.sessionStorage.setItem("ListStep2", -1);
                            window.sessionStorage.setItem("Step3", -1);
                            window.sessionStorage.setItem("currentStep3", -1);
                            window.sessionStorage.setItem("ListStep3", -1);
                            document.location.href = "success";
                        }
                        else {
                            setError("Un problème est apparu lors de l'insertion.");
                        }
                    }
                }
                else {
                    setError("Une base existe déjà avec cet indentifiant.");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    // Fonction qui permet d'ajouter une nouvelle base
    function Save(state) {
        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/save_base";

        // Envoi de la requête
        var axios_resp = axios.post(url, { state, token })
            // Récupération de la réponse
            .then(function (response) {
                if (response.data.status == "enqueued") {
                    setValidationPost(true);
                }
            })
            .catch(function (error) {
                console.log(error);
            });

        // Mise à jour des informations
        setHistorique(historique["username"] = token);
        setHistorique(historique["base"] = state[Object.keys(state)[0]]);

        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/insertion_in_progress";

        // Envoi de la requête
        var axios_resp = axios.post(url, historique)
            // Récupération de la réponse
            .then(function (response) {
                if (response.status == 200) {
                    setValidationHistorique(true);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        if (validationPost && validationHistorique) {
            window.sessionStorage.setItem("Step2", -1);
            window.sessionStorage.setItem("currentStep2", -1);
            window.sessionStorage.setItem("ListStep2", -1);
            window.sessionStorage.setItem("Step3", -1);
            window.sessionStorage.setItem("currentStep3", -1);
            window.sessionStorage.setItem("ListStep3", -1);
            document.location.href = "success_save";
        }
        else {
            setError("Un problème est apparu lors de l'insertion.");
        }
    };

    // Fonction qui permet de sauvegarder les modifications d'une base
    function AddModifications(state) {
        var id_document = extractParamsUrl(window.location.href)[Object.keys(extractParamsUrl(window.location.href))[0]];
        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/edit_base/" + id_document;

        // Envoi de la requête
        var axios_resp = axios.post(url)
            // Récupération de la réponse
            .then(function (response) {
                const prevState = response.data;

                var keysState = Object.keys(state);
                var keysPrevState = Object.keys(prevState);
                var modification = {};

                for (var s = 0; s < keysState.length; s++) {
                    if (state[keysState[s]] != prevState[keysPrevState[s]]) {
                        modification[keysState[s]] = state[keysState[s]];
                    }
                }

                if (Object.keys(modification).length == 0) {
                    setError("Aucun champs n'a été modifié.");
                }
                else {
                    // Définition de l'URL (pour la requête)
                    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/new_base";

                    var data = {};
                    var keys = Object.keys(state);
                    var values = Object.values(state);

                    // Envoi de la requête
                    var axios_resp = axios.post(url, state)
                        // Récupération de la réponse
                        .then(function (response) {
                            if (response.status == 200) {
                                setValidationPost(true);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                    var keys = Object.keys(state);

                    // Mise à jour des informations
                    setHistorique(historique["username"] = token);
                    setHistorique(historique["base"] = state[keys[0]]);
                    setHistorique(historique["date"] = new Date());
                    setHistorique(historique["b"] = "");
                    setHistorique(historique["t"] = "");
                    setHistorique(historique["v"] = "");
                    setHistorique(historique["modification"] = modification);

                    // Définition de l'URL (pour la requête)
                    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/modification";

                    // Envoi de la requête
                    var axios_resp = axios.post(url, historique)
                        // Récupération de la réponse
                        .then(function (response) {
                            if (response.status == 200) {
                                setValidationHistorique(true);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                    if (validationPost && validationHistorique) {
                        document.location.href = "success_modif";
                    }
                    else {
                        setError("Un problème est apparu lors de la modification.");
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    // Fonction qui permet de modifier un élément déjà saisi
    function Modify(step, k) {
        if (step == "Step2") {
            params["setCurrentStep2"](k);
            window.sessionStorage.setItem("currentStep2", k);
        }
        params["setCurrent"](step);
    }

    // Fonction qui permet de supprimer un élément déjà saisi
    function Delete(step, k) {
        var state = JSON.parse(window.sessionStorage.getItem("State"));
        var newState = {};

        if (step == "Step2") {
            // Alignement des variables avec un numéro supérieur 
            for (var s = 0; s < Object.keys(state).length; s++) {
                // Step1
                if (Object.keys(state)[s].split('#').length == 1) {
                    newState[Object.keys(state)[s]] = state[Object.keys(state)[s]];
                }
                // Step2
                else if (Object.keys(state)[s].split('#').length == 2) {
                    if (Object.keys(state)[s].split('#')[1] < k) {
                        newState[Object.keys(state)[s]] = state[Object.keys(state)[s]];
                    }
                    else if (Object.keys(state)[s].split('#')[1] > k) {
                        var key = Object.keys(state)[s].split('#')[0] + '#' + String(parseInt(Object.keys(state)[s].split('#')[1]) - 1);
                        newState[key] = state[Object.keys(state)[s]];
                    }
                }
                // Step3
                else {
                    if (Object.keys(state)[s].split('#')[1] < k) {
                        newState[Object.keys(state)[s]] = state[Object.keys(state)[s]];
                    }
                    else if (Object.keys(state)[s].split('#')[1] > k) {
                        var key = Object.keys(state)[s].split('#')[0] + '#' + String(parseInt(Object.keys(state)[s].split('#')[1]) - 1) + '#' + Object.keys(state)[s].split('#')[2];
                        newState[key] = state[Object.keys(state)[s]];
                    }
                }
            }

            // Mise à jour de la variable State
            window.sessionStorage.setItem("State", JSON.stringify(newState));

            // Mise à jour de la variable Step2
            var step2 = window.sessionStorage.getItem("Step2") - 1;
            window.sessionStorage.setItem("Step2", step2);

            // Mise à jour de la variable ListStep2
            var listStep2 = {};
            for (var i = 0; i < Object.keys(JSON.parse(window.sessionStorage.getItem("ListStep2"))).length; i++) {
                if (JSON.parse(window.sessionStorage.getItem("ListStep2"))[i].split("#")[1] < k) {
                    listStep2[i] = JSON.parse(window.sessionStorage.getItem("ListStep2"))[i];
                }
                else if (JSON.parse(window.sessionStorage.getItem("ListStep2"))[i].split("#")[1] > k) {
                    listStep2[i - 1] = JSON.parse(window.sessionStorage.getItem("ListStep2"))[i].split('#')[0] + '#' + String(parseInt(JSON.parse(window.sessionStorage.getItem("ListStep2"))[i].split('#')[1]) - 1);
                }
            }
            window.sessionStorage.setItem("ListStep2", JSON.stringify(listStep2));

            // Mise à jour de la variable Step3
            var step3 = JSON.parse(window.sessionStorage.getItem("Step3"));
            var newStep3 = {};

            for (var s = 0; s < Object.keys(step3).length; s++) {
                if (Object.keys(step3)[s] < k) {
                    newStep3[Object.keys(step3)[s]] = step3[Object.keys(step3)[s]];
                }
                else if (Object.keys(step3)[s] > k) {
                    newStep3[String(parseInt(Object.keys(step3)[s]) - 1)] = step3[Object.keys(step3)[s]];
                }
            }
            window.sessionStorage.setItem("Step3", JSON.stringify(step3));

            // Mise à jour de la variable ListStep3
            var listStep3 = [];
            for (var i = 0; i < JSON.parse(window.sessionStorage.getItem("ListStep3")).length; i++) {
                if (JSON.parse(window.sessionStorage.getItem("ListStep3"))[i].split('#')[0] < k) {
                    listStep3.push(JSON.parse(window.sessionStorage.getItem("ListStep3"))[i]);
                }
                else if (JSON.parse(window.sessionStorage.getItem("ListStep3"))[i].split('#')[0] > k) {
                    listStep3.push(parseInt(JSON.parse(window.sessionStorage.getItem("ListStep3"))[i].split('#')[0]) - 1 + '#' + JSON.parse(window.sessionStorage.getItem("ListStep3"))[i].split('#')[1]);
                }
            }
            window.sessionStorage.setItem("ListStep3", JSON.stringify(listStep3));
        }
    }

    function Progress(state) {
        return (
            <Line percent={params["percent"]} strokeWidth={2} strokeColor="#036287" />
        );
    }

    function buttons() {
        if (params["current"] == "Resum") {
            return (
                <Box>
                    <Button_form id="button" onClick={() => {
                        if (params["Step2"] != 0) {
                            params["setNbInputs"](params["nbInputs"] + 19 + 44);
                        };
                        params["setStep2"](parseInt(params["Step2"]) + 1);
                        params["setCurrentStep2"](params["Step2"]);
                        params["setCurrent"]("Step2");
                        window.sessionStorage.setItem("Step2", parseInt(window.sessionStorage.getItem("Step2")) + 1);
                        window.sessionStorage.setItem("currentStep2", parseInt(window.sessionStorage.getItem("Step2")));
                    }}>Ajouter une table</Button_form>
                    <Button_form id="button" onClick={() => {
                        if (window.location.href.includes("new")) {
                            Save(JSON.parse(window.sessionStorage.getItem("State")));
                        }
                        else if (window.location.href.includes("edit_base_in_progress")) {
                            Save(JSON.parse(window.sessionStorage.getItem("State")));
                        }
                    }}>Sauvegarder</Button_form>
                    <Button_form id="button" onClick={() => {
                        if (window.location.href.includes("new")) {
                            Add(JSON.parse(window.sessionStorage.getItem("State")));
                        }
                        else if (window.location.href.includes("edit_base_in_progress")) {
                            Add(JSON.parse(window.sessionStorage.getItem("State")));
                        }
                        else if (window.location.href.includes("edit_base")) {
                            AddModifications(JSON.parse(window.sessionStorage.getItem("State")));
                        }
                    }}>Terminer</Button_form>
                </Box>
            );
        }
    }

    // Si l'utilisateur n'est pas connecté
    if (!token) {
        return (
            <Login setToken={setToken} />
        );
    }
    // Etapes du formulaire    
    if (params["current"] == "ResumStep2" || params["current"] == "Step3") {
        return (
            <Page>
                <Box>
                    {Progress(JSON.parse(window.sessionStorage.getItem("State")))}
                    <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header">
                            <Titre_section_form>Informations sur la base {JSON.parse(window.sessionStorage.getItem("State"))[inputsStep1Required[0]["id"]]}</Titre_section_form>
                        </AccordionSummary>
                        <AccordionDetails>
                            {inputsStep1Required.map((a) => {
                                return (
                                    <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[a.id]}</Texte>
                                );
                            })}
                            {inputsStep1.map((e) => {
                                return (
                                    <Texte>{e.label} : {JSON.parse(window.sessionStorage.getItem("State"))[e.id]}</Texte>
                                );
                            })}
                        </AccordionDetails>
                    </Accordion>
                    {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep2"))).map((e) => {
                        var k = e.replace(e.match(".+#"), "");
                        if (k != window.sessionStorage.getItem("currentStep2")) {
                            return (
                                <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                    <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                        <Titre_section_form>Informations sur la table {JSON.parse(window.sessionStorage.getItem("State"))[e]}</Titre_section_form>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        {inputsStep2Required.map((a) => {
                                            return (
                                                <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                            );
                                        })}
                                        {inputsStep2.map((a) => {
                                            return (
                                                <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                            );
                                        })}
                                    </AccordionDetails>
                                </Accordion>
                            );
                        }
                    })}
                    <br></br>
                </Box>
                <Texte>{error}</Texte>
            </Page>
        );
    }

    if (inputsStep1Required.length != 0) {
        return (
            <Page>
                <Box>
                    {Progress(JSON.parse(window.sessionStorage.getItem("State")))}
                    <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header">
                            <Titre_section_form>Informations sur la base {JSON.parse(window.sessionStorage.getItem("State"))[inputsStep1Required[0]["id"]]}</Titre_section_form>
                            <Button variant="container" onClick={() => { Modify("Step1"); }}>Modifier</Button>
                        </AccordionSummary>
                        <AccordionDetails>
                            {inputsStep1Required.map((a) => {
                                return (
                                    <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[a.id]}</Texte>
                                );
                            })}
                            {inputsStep1.map((e) => {
                                return (
                                    <Texte>{e.label} : {JSON.parse(window.sessionStorage.getItem("State"))[e.id]}</Texte>
                                );
                            })}
                        </AccordionDetails>
                    </Accordion>
                    {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep2"))).map((e) => {
                        var k = e.replace(e.match(".+#"), "");
                        return (
                            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                    <Titre_section_form>Informations sur la table {JSON.parse(window.sessionStorage.getItem("State"))[e]}</Titre_section_form>
                                    <Button variant="container" onClick={() => { Modify("Step2", k); }}>Modifier</Button>
                                    <Button variant="container" onClick={() => { Delete("Step2", k); }}>Supprimer</Button>
                                </AccordionSummary>
                                <AccordionDetails>
                                    {inputsStep2Required.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                        );
                                    })}
                                    {inputsStep2.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                        );
                                    })}
                                </AccordionDetails>
                            </Accordion>
                        );

                    })}
                    <br></br>
                    {buttons()}
                </Box>
                <Texte>{error}</Texte>
            </Page>
        );
    }
}
