import React, { useState, useEffect } from "react";
import { Box, MenuItem, Select, Grid } from "@mui/material";
import { BoxContent, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import Documents from '../Listes/AllDocuments';

const axios = require('axios');

export default function Supp() {

  const { token, setToken } = useToken();
  const [error, setError] = useState('');
  const [idDocument, setIdDocument] = useState(0);
  const [listDocuments, setListDocuments] = useState([]);
  const [isInit, setIsInit] = useState(false);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    if (isInit === false) {
      ListDocuments();
      setIsInit(true);
    }
  }, [isInit]);

  async function ListDocuments() {
    const response = await Documents();
    setListDocuments(response);
  }

  // Fonction qui permet de sauvegarder les modifications des accèss
  function Suppression(modifications) {
    setError("");
    if (idDocument == "0") {
      setError("Veuillez saisir une base.");
    }
    else {
      // Définition de l'URL (pour la requête)
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/supp";

      // Envoi de la requête
      var axios_resp = axios.post(url, modifications)
        // Récupération de la réponse
        .then(function (response) {
          if (response.status == 200) {
            setValidationPost(true);
          }

        })
        .catch(function (error) {
          console.log(error);
        });

      if (validationPost) {
        document.location.href = "success_supp";
      }
      else {
        setError("Un problème est apparu lors de la suppression.");
      }

    }
  };

  return (
    <BoxContent>
      <Box>
        <Titre_section variant='h5'>Information sur la base</Titre_section>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2}><Texte>Identifiant :</Texte></Grid>
          <Grid item xs={9}>
            <Select
              value={idDocument}
              onChange={(event) => { setIdDocument(event.target.value); }}
              sx={{ width: '200px' }}
              size="small"
            >
              <MenuItem value={0}>ID</MenuItem>
              {listDocuments.map((e) => {
                return (
                  <MenuItem value={e.id}>{e.id}</MenuItem>
                );
              })}
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box>
        <Button_form id="button" onClick={() => Suppression({ "id": idDocument, "username": token, "date": new Date() })}>Valider</Button_form>
        <Texte>{error}</Texte>
      </Box>
    </BoxContent>
  );
}
