import React, { useState, useEffect } from 'react';
import { FormControl, Box, Tab, Grid } from "@mui/material";
import { Texte, Button_form, Input_new } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import List_inputs from '../Listes/Inputs';
import { TabPanel, TabContext, TabList } from '@mui/lab';
import verificationEmail from '../Functions/verificationEmail';
import verificationUrl from '../Functions/verificationUrl';
import CircleRoundedIcon from '@mui/icons-material/CircleRounded';
import { Line } from 'rc-progress';

export default function InputsStep1(params) {

    // User et Date
    const { token, setToken } = useToken();

    // Listes des champs
    const [inputsStep1Required, setSInputsStep1Required] = useState([]);
    const [inputsStep1, setInputsStep1] = useState([]);
    const [categoriesStep1, setCategoriesStep1] = useState([]);
    const [value, setValue] = React.useState('Général');

    // Etape en cours
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    // Fonction qui permet de récupérer les champs du formulaire
    async function recupInputs() {
        const inputs = await List_inputs();
        setSInputsStep1Required(inputs['Step1_required']);
        setInputsStep1(inputs['Step1']);
        setCategoriesStep1(inputs['Categories_Step1']);
    }

    useEffect(() => {
        recupInputs();
    }, [isInit]);

    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
    };

    function ColorCircle(state, categorie) {
        var validCategorie = true;

        for (var c = 0; c < categoriesStep1.length; c++) {
            if (categoriesStep1[c]["categorie"] == categorie) {
                for (var champs = 0; champs < categoriesStep1[c]["listChamps"].length; champs++) {
                    if (!Object.keys(state).includes(categoriesStep1[c]["listChamps"][champs])) {
                        validCategorie = false;
                    }
                    else {
                        if (state[categoriesStep1[c]["listChamps"][champs]] == '') {
                            validCategorie = false;
                        }
                    }
                }
            }
        }
        if (validCategorie) {
            return (
                <Texte sx={{ marginBottom: 0, marginTop: 1, color: 'green' }}><CircleRoundedIcon fontSize='small'></CircleRoundedIcon></Texte>
            );
        }
        else {
            return (
                <Texte sx={{ marginBottom: 0, marginTop: 1, color: 'red' }}><CircleRoundedIcon fontSize='small'></CircleRoundedIcon></Texte>
            );
        }

    }

    function CalculProgress(state) {
        var keys = Object.keys(state);
        var count = 0;
        for (var k = 0; k < keys.length; k++) {
            if (state[keys[k]] != '') {
                count++;
            }
        }
        var per = (count / params["nbInputs"]) * 100;
        params["setPercent"](per);
    }

    function Progress() {
        return (
            <Line percent={params["percent"]} strokeWidth={2} strokeColor="#036287" />
        );
    }

    // Etapes du formulaire
    return (
        <Box>
            {Progress()}
            <FormControl sx={{ width: "100%" }}>
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChangeTab} variant="scrollable" textColor="#036287">
                            {categoriesStep1.map((c) => {
                                return (
                                    <Tab icon={ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie)} label={c.categorie} value={c.categorie} />
                                );
                            })}
                        </TabList>
                    </Box>
                    {categoriesStep1.map((c) => {
                        return (
                            <TabPanel value={c.categorie}>
                                {inputsStep1Required.map((e) => {
                                    if (c.listChamps.includes(e.id)) {
                                        if (JSON.parse(window.sessionStorage.getItem("State"))[String(e.id)]) {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label}* :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new required type={type} id={e.id} placeholder={e.placeholder} defaultValue={JSON.parse(window.sessionStorage.getItem("State"))[String(e.id)]} onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id)] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie);
                                                        CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} />
                                                    </Grid>
                                                </Grid>
                                            );
                                        }
                                        else {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label}* :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new required type={type} id={e.id} placeholder={e.placeholder} defaultValue="" onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id)] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie);
                                                        CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} />
                                                    </Grid>
                                                </Grid>
                                            );
                                        }
                                    }
                                })}
                                {inputsStep1.map((e) => {
                                    if (c.listChamps.includes(e.id)) {
                                        if (JSON.parse(window.sessionStorage.getItem("State"))[String(e.id)]) {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label} :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new type={type} id={e.id} placeholder={e.placeholder} defaultValue={JSON.parse(window.sessionStorage.getItem("State"))[String(e.id)]} onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id)] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie);
                                                        CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} />
                                                    </Grid>
                                                </Grid>
                                            );
                                        }
                                        else {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label} :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new type={type} id={e.id} placeholder={e.placeholder} defaultValue="" onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id)] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie);
                                                        CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} />
                                                    </Grid>
                                                </Grid>
                                            );
                                        }
                                    }
                                })
                                }
                            </TabPanel>
                        );
                    }
                    )}
                </TabContext>
                <Box>
                    <Button_form id="button" onClick={() => {
                        inputsStep1Required.map((e) => {
                            if (!JSON.parse(window.sessionStorage.getItem("State"))[String(e.id)]) {
                                setError("Les champs obligatoires ne sont pas tous remplis !");
                            }
                            else {
                                var verifEmail = true;
                                var verifUrl = true;
                                for (var k = 0; k < Object.keys(JSON.parse(window.sessionStorage.getItem("State"))).length; k++) {
                                    for (var i = 0; i < Object.keys(inputsStep1Required).length; i++) {
                                        if (inputsStep1Required[i]['id'] == Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]) {
                                            if (inputsStep1Required[i]['type'] == 'email') {
                                                verifEmail = verificationEmail(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                            else if (inputsStep1Required[i]['type'] == 'url') {
                                                verifUrl = verificationUrl(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                        }
                                        else if (inputsStep1Required[i]['id'] == Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]) {
                                            if (inputsStep1Required[i]['type'] == 'email') {
                                                verifEmail = verificationEmail(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                            else if (inputsStep1Required[i]['type'] == 'url') {
                                                verifUrl = verificationUrl(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                        }
                                    }
                                }
                                if (!verifEmail) {
                                    setError("Vérifiez les champs d'email.");
                                }
                                else if (!verifUrl) {
                                    setError("Vérifiez les champs d'url.");
                                }
                                else {
                                    params["setCurrent"]("Resum");
                                }
                            }
                        });
                    }
                    }>Valider</Button_form>
                </Box>
            </FormControl>
            <Texte>{error}</Texte>
        </Box>
    );
}
