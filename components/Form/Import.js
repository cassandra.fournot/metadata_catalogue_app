import React, { useState, useEffect } from "react";
import { Box } from "@mui/material";
import { BoxContent, Titre_section, Texte, Button_form, Input_new } from "../Styles/Styles.js";
import useToken from '../Token/Token';

const axios = require('axios');

export default function Import() {

  const { token, setToken } = useToken();
  const [today, setToday] = useState();
  const [error, setError] = useState("");
  const [isInit, setIsInit] = useState(false);

  // Variables de vérification
  const [validationPost, setValidationPost] = useState([]);

  useEffect(() => {
    setToday(new Date().toLocaleDateString());
  }, [isInit]);

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Importation(file) {
    if (file.type != "application/json") {
      setError("Merci de séléctionner un fichier JSON.");
    }
    else {
      let reader = new FileReader();
      reader.readAsText(file);

      reader.onload = function () {
        var data = reader.result;

        // Définition de l'URL (pour la requête)
        var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/importation";

        // Envoi de la requête
        var axios_resp = axios.post(url, JSON.parse(data))
          // Récupération de la réponse
          .then(function (response) {
            if (response.data == true) {
              document.location.href = "success";
            }
            else {
              setError("Une erreur s'est produite lors de l'importation. Merci de vérifier la structure du fichier. " + response.data);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      };
    }
  };

  return (
    <BoxContent>
      <Box id="info">
        <Titre_section variant='h5'>Informations principales</Titre_section>
        <Texte id="user">Nom d'utilisateur : {token}</Texte>
        <Texte id="date">Date de saisie : {String(today)}</Texte>
      </Box>
      <Box sx={{ marginTop: "30px" }}>
        <Titre_section variant='h5' sx={{ marginTop: '10px' }}>Sélectionner un fichier JSON pour l'importation</Titre_section>
        <Input_new sx={{ border: "0px" }} type="file" id="file" accept="application/json"></Input_new>
      </Box>
      <Box><Button_form onClick={() => {
        Importation(document.getElementById("file").files[0]);
      }}>
        Importer
      </Button_form></Box>
      <Texte>{error}</Texte>
    </BoxContent>
  );
}