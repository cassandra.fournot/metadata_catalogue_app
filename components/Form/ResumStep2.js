import React, { useState, useEffect } from 'react';
import { Box, Button } from "@mui/material";
import { Page, Titre_section_form, Texte, Button_form } from "../Styles/Styles.js";
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import useToken from '../Token/Token';
import List_inputs from '../Listes/Inputs';

export default function Resum(params) {

    // User et Date
    const { token, setToken } = useToken();

    // Listes des champs
    const [inputsStep2Required, setInputsStep2Required] = useState([]);
    const [inputsStep2, setInputsStep2] = useState([]);
    const [inputsStep3Required, setInputsStep3Required] = useState([]);
    const [inputsStep3, setInputsStep3] = useState([]);

    // Etape en cours
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    // Fonction qui permet de récupérer les champs du formulaire
    async function recupInputs() {
        const inputs = await List_inputs();
        setInputsStep2Required(inputs['Step2_required']);
        setInputsStep2(inputs['Step2']);
        setInputsStep3Required(inputs['Step3_required']);
        setInputsStep3(inputs['Step3']);
    }

    useEffect(() => {
        if (isInit == false) {
            recupInputs();
            setIsInit(true);
        }
    }, [isInit]);

    // Fonction qui permet de modifier un élément déjà saisi
    function Modify(step, k) {
        params["setCurrent"](step);
        if (step == "Step2") {
            params["setCurrentStep2"](k);
            window.sessionStorage.setItem("currentStep2", k);
        }
        if (step == "Step3") {
            params["setCurrentStep3"](k);
            window.sessionStorage.setItem("currentStep3", k);
        }
    }

    // Fonction qui permet de supprimer un élément déjà saisi
    function Delete(step, k) {
        var state = JSON.parse(window.sessionStorage.getItem("State"));
        var newState = {};

        if (step == "Step3") {
            // Alignement des variables avec un numéro supérieur 
            for (var s = 0; s < Object.keys(state).length; s++) {
                if (!Object.keys(state)[s].includes('#' + window.sessionStorage.getItem("currentStep2") + '#' + k)) {
                    if (Object.keys(state)[s].split('#')[2] && Object.keys(state)[s].split('#')[2] < k) {
                        newState[Object.keys(state)[s]] = state[Object.keys(state)[s]];
                    }
                    else if (Object.keys(state)[s].split('#')[2] && Object.keys(state)[s].split('#')[2] > k) {
                        var key = Object.keys(state)[s].split('#')[0] + '#' + Object.keys(state)[s].split('#')[1] + '#' + String(parseInt(Object.keys(state)[s].split('#')[2]) - 1);
                        newState[key] = state[Object.keys(state)[s]];
                    }
                    else {
                        newState[Object.keys(state)[s]] = state[Object.keys(state)[s]];
                    }
                }
            }

            // Mise à jour de la variable State
            window.sessionStorage.setItem("State", JSON.stringify(newState));

            // Mise à jour de la variable Step3
            var step3 = JSON.parse(window.sessionStorage.getItem("Step3"));
            step3[window.sessionStorage.getItem("currentStep2")] = step3[window.sessionStorage.getItem("currentStep2")] - 1;
            window.sessionStorage.setItem("Step3", JSON.stringify(step3));

            // Mise à jour de la variable ListStep3
            var listStep3 = [];
            for (var i = 0; i < step3[window.sessionStorage.getItem("currentStep2")] + 1; i++) {
                listStep3.push(window.sessionStorage.getItem("currentStep2") + '#' + i);
            }
            window.sessionStorage.setItem("ListStep3", JSON.stringify(listStep3));
        }
    }

    // Etapes du formulaire
    if (params["current"] == "Step3") {
        return (
            <Page>
                <Box>
                    <Titre_section_form>Informations sur la table en cours</Titre_section_form>
                    {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep2"))).map((e) => {
                        var k = e.replace(e.match(".+#"), "");
                        if (k == window.sessionStorage.getItem("currentStep2")) {
                            return (
                                <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                    <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                        <Titre_section_form>Informations sur la table {JSON.parse(window.sessionStorage.getItem("State"))[e]}</Titre_section_form>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        {inputsStep2Required.map((a) => {
                                            return (
                                                <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                            );
                                        })}
                                        {inputsStep2.map((a) => {
                                            return (
                                                <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                            );
                                        })}
                                    </AccordionDetails>
                                </Accordion>
                            );
                        }
                    })}
                    {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep3"))).map((e) => {
                        var k = e.split('#')[2];
                        var k_step2 = e.split('#')[1];
                        if (k_step2 == window.sessionStorage.getItem("currentStep2")) {
                            if (k != window.sessionStorage.getItem("currentStep3")) {
                                return (
                                    <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                            <Titre_section_form>Informations sur la variable {JSON.parse(window.sessionStorage.getItem("State"))[inputsStep3Required[0]['id'] + '#' + e]}</Titre_section_form>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            {inputsStep3Required.map((a) => {
                                                return (
                                                    <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + window.sessionStorage.getItem("currentStep2") + "#" + k]}</Texte>
                                                );
                                            })}
                                            {inputsStep3.map((a) => {
                                                return (
                                                    <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + window.sessionStorage.getItem("currentStep2") + "#" + k]}</Texte>
                                                );
                                            })}
                                        </AccordionDetails>
                                    </Accordion>
                                );
                            }
                        }
                    })}
                    <br></br>
                </Box>
            </Page>
        );
    }

    return (
        <Page>
            <Box>
                <Titre_section_form>Informations sur la table en cours</Titre_section_form>
                {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep2"))).map((e) => {
                    var k = e.replace(e.match(".+#"), "");
                    if (k == window.sessionStorage.getItem("currentStep2")) {
                        return (
                            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                    <Titre_section_form>Informations sur la table {JSON.parse(window.sessionStorage.getItem("State"))[e]}</Titre_section_form>
                                    <Button variant="container" onClick={() => { Modify("Step2", k); }}>Modifier</Button>
                                </AccordionSummary>
                                <AccordionDetails>
                                    {inputsStep2Required.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                        );
                                    })}
                                    {inputsStep2.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + k]}</Texte>
                                        );
                                    })}
                                </AccordionDetails>
                            </Accordion>
                        );
                    }
                })}
                {Object.values(JSON.parse(window.sessionStorage.getItem("ListStep3"))).map((e) => {
                    var k = e.split('#')[1];
                    var k_step2 = e.split('#')[0];
                    if (k_step2 == window.sessionStorage.getItem("currentStep2")) {
                        return (
                            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
                                    <Titre_section_form>Informations sur la variable {JSON.parse(window.sessionStorage.getItem("State"))[inputsStep3Required[0]['id'] + '#' + e]}</Titre_section_form>
                                    <Button variant="container" onClick={() => { Modify("Step3", k); }}>Modifier</Button>
                                    <Button variant="container" onClick={() => { Delete("Step3", k); }}>Supprimer</Button>
                                </AccordionSummary>
                                <AccordionDetails>
                                    {inputsStep3Required.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + window.sessionStorage.getItem("currentStep2") + "#" + k]}</Texte>
                                        );
                                    })}
                                    {inputsStep3.map((a) => {
                                        return (
                                            <Texte>{a.label} : {JSON.parse(window.sessionStorage.getItem("State"))[String(a.id) + "#" + window.sessionStorage.getItem("currentStep2") + "#" + k]}</Texte>
                                        );
                                    })}
                                </AccordionDetails>
                            </Accordion>
                        );
                    }
                })}
                <br></br>
                <Button_form id="button" onClick={() => {
                    if (params["Step3"] != 0) {
                        params["setNbInputs"](params["nbInputs"] + 44);
                    };
                    params["setStep3"](parseInt(params["Step3"]) + 1);
                    params["setCurrentStep3"](params["Step3"]);
                    params["setCurrent"]("Step3");
                    // Première variable ajoutée
                    if (window.sessionStorage.getItem("Step3") == -1) {
                        var value = {};
                        value[window.sessionStorage.getItem("Step2")] = 0;
                        window.sessionStorage.setItem("Step3", JSON.stringify(value));
                        window.sessionStorage.setItem("currentStep3", 0);
                    }
                    else {
                        // Ajout d'une variable à une table qui contient déjà une variable
                        if (JSON.parse(window.sessionStorage.getItem("Step3"))[window.sessionStorage.getItem("currentStep2")] != undefined) {
                            var value = JSON.parse(window.sessionStorage.getItem("Step3"));
                            value[window.sessionStorage.getItem("Step2")] = JSON.parse(window.sessionStorage.getItem("Step3"))[window.sessionStorage.getItem("Step2")] + 1;
                            window.sessionStorage.setItem("Step3", JSON.stringify(value));
                            window.sessionStorage.setItem("currentStep3", JSON.parse(window.sessionStorage.getItem("Step3"))[window.sessionStorage.getItem("Step2")]);
                        }
                        // Ajout d'une variable à une table qui ne contient pas de variable
                        else {
                            var value = JSON.parse(window.sessionStorage.getItem("Step3"));
                            value[window.sessionStorage.getItem("Step2")] = 0;
                            window.sessionStorage.setItem("Step3", JSON.stringify(value));
                            window.sessionStorage.setItem("currentStep3", 0);
                        }
                    }
                }}>Ajouter une variable</Button_form>
                <Button_form id="button" onClick={() => {
                    if (params["currentStep2"] != params["Step2"]) {
                        params["setCurrentStep2"](params["Step2"]);
                    }
                    if (params["currentStep3"] != params["Step3"]) {
                        params["setCurrentStep3"](params["Step3"]);
                    }
                    params["setStep3"](0); params["setCurrentStep3"](0); params["setCurrent"]("Resum");
                }}>Valider la table</Button_form>
            </Box>
        </Page>
    );
}
