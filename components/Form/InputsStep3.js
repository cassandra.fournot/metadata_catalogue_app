import React, { useState, useEffect } from 'react';
import { Box, FormControl, Tab, Grid } from "@mui/material";
import { Texte, Button_form, Input_new } from "../Styles/Styles.js";
import List_inputs from '../Listes/Inputs';
import { TabPanel, TabContext, TabList } from '@mui/lab';
import convert_object_to_string from '../Functions/convert_object_to_string';
import verificationEmail from '../Functions/verificationEmail';
import verificationUrl from '../Functions/verificationUrl';
import CircleRoundedIcon from '@mui/icons-material/CircleRounded';

var dicoStep3 = {};

export default function InputsStep3(params) {

    // Listes des champs
    const [inputsStep3Required, setSInputsStep3Required] = useState([]);
    const [inputsStep3, setInputsStep3] = useState([]);
    const [categoriesStep3, setCategoriesStep3] = useState([]);
    const [value, setValue] = React.useState('Général');

    // Etape en cours
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    // Fonction qui permet de récupérer les champs du formulaire
    async function recupInputs() {
        const inputs = await List_inputs();
        setSInputsStep3Required(inputs['Step3_required']);
        setInputsStep3(inputs['Step3']);
        setCategoriesStep3(inputs['Categories_Step3']);
    }

    useEffect(() => {
        recupInputs();
    }, [isInit]);

    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
    };

    function ColorCircle(state, categorie) {
        var validCategorie = true;

        for (var c = 0; c < categoriesStep3.length; c++) {
            if (categoriesStep3[c]["categorie"] == categorie) {
                for (var champs = 0; champs < categoriesStep3[c]["listChamps"].length; champs++) {
                    var champsStep3 = categoriesStep3[c]["listChamps"][champs] + "#" + String(params["Step2"]) + "#" + String(params["Step3"]);
                    if (!Object.keys(state).includes(champsStep3)) {
                        validCategorie = false;
                    }
                    else {
                        if (state[champsStep3] == '') {
                            validCategorie = false;
                        }
                    }
                }
            }
        }
        if (validCategorie) {
            return (
                <Texte sx={{ marginBottom: 0, marginTop: 1, color: 'green' }}><CircleRoundedIcon fontSize='small'></CircleRoundedIcon></Texte>
            );
        }
        else {
            return (
                <Texte sx={{ marginBottom: 0, marginTop: 1, color: 'red' }}><CircleRoundedIcon fontSize='small'></CircleRoundedIcon></Texte>
            );
        }

    }

    function CalculProgress(state) {
        var keys = Object.keys(state);
        var count = 0;
        for (var k = 0; k < keys.length; k++) {
            if (state[keys[k]] != '') {
                count++;
            }
        }
        var per = (count / params["nbInputs"]) * 100;
        params["setPercent"](per);
    }

    // Etapes du formulaire
    return (
        <Box>
            <FormControl sx={{ width: "100%" }}>
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChangeTab} variant="scrollable" textColor="#036287">
                            {categoriesStep3.map((c) => {
                                return (
                                    <Tab icon={ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie)} label={c.categorie} value={c.categorie} />
                                );
                            })}
                        </TabList>
                    </Box>
                    {categoriesStep3.map((c) => {
                        return (
                            <TabPanel value={c.categorie}>
                                {inputsStep3Required.map((e) => {
                                    // Champs obligatoires
                                    if (c.listChamps.includes(e.id)) {
                                        // Modification 
                                        if (JSON.parse(window.sessionStorage.getItem("State"))[String(e.id + '#' + window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))]) {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label}* :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new required type={type} id={String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))} placeholder={e.placeholder} defaultValue={JSON.parse(window.sessionStorage.getItem("State"))[String(e.id + '#' + window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))]} onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        params["ListStep3"][window.sessionStorage.getItem("currentStep3")][String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2")) + "#" + String(window.sessionStorage.getItem("currentStep3"))] = event.target.value;
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie); CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} /></Grid>
                                                </Grid>
                                            );
                                        }
                                        else {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label}* :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new required type={type} id={String(e.id) + "#" + String(params["Step2"] + "#" + String(window.sessionStorage.getItem("currentStep3")))} placeholder={e.placeholder} defaultValue="" onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        dicoStep3[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie); CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} /></Grid>
                                                </Grid>
                                            );
                                        }
                                    }
                                })}
                                {inputsStep3.map((e) => {
                                    if (c.listChamps.includes(e.id)) {
                                        if (params["state"][String(e.id + '#' + window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))]) {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }

                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label} :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new type={type} id={String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))} placeholder={e.placeholder} label={e.label} defaultValue={JSON.parse(window.sessionStorage.getItem("State"))[String(e.id + '#' + window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))]} onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        params["ListStep3"][window.sessionStorage.getItem("currentStep3")][String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2")) + "#" + String(window.sessionStorage.getItem("currentStep3"))] = event.target.value;
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie); CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} /></Grid>
                                                </Grid>
                                            );
                                        }
                                        else {
                                            var type = "string";
                                            if (e.type == "date") {
                                                type = "date";
                                            }
                                            else if (e.type == "email") {
                                                type = "email";
                                            }
                                            else if (e.type == "url") {
                                                type = "url";
                                            }
                                            else if (e.type == "integer") {
                                                type = "number";
                                            }
                                            return (
                                                <Grid container sx={{ alignItems: "center" }}>
                                                    <Grid item xs={2.6} sx={{ marginRight: "10px" }}><Texte>{e.label} :</Texte></Grid>
                                                    <Grid item xs={9}><Input_new type={type} id={String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))} placeholder={e.placeholder} defaultValue="" onChange={(event) => {
                                                        var value = JSON.parse(window.sessionStorage.getItem("State"));
                                                        value[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        window.sessionStorage.setItem("State", JSON.stringify(value));
                                                        dicoStep3[String(e.id) + "#" + String(window.sessionStorage.getItem("currentStep2") + "#" + String(window.sessionStorage.getItem("currentStep3")))] = event.target.value;
                                                        ColorCircle(JSON.parse(window.sessionStorage.getItem("State")), c.categorie); CalculProgress(JSON.parse(window.sessionStorage.getItem("State")));
                                                    }} /></Grid>
                                                </Grid>
                                            );
                                        }
                                    }
                                })
                                }
                            </TabPanel>
                        );
                    })
                    }
                </TabContext>
                <Box>
                    <Button_form id="button" onClick={() => {
                        inputsStep3Required.map((e) => {
                            if (!convert_object_to_string(JSON.parse(window.sessionStorage.getItem("State"))).includes(e["id"]) || JSON.parse(window.sessionStorage.getItem("State"))[e["id"]] == '') {
                                setError("Les champs obligatoires ne sont pas tous remplis !");
                            }
                            else {
                                var verifEmail = true;
                                var verifUrl = true;
                                for (var k = 0; k < Object.keys(JSON.parse(window.sessionStorage.getItem("State"))).length; k++) {
                                    for (var i = 0; i < Object.keys(inputsStep3Required).length; i++) {
                                        if (inputsStep3Required[i]['id'] + "#" + String(params["Step2"] + "#" + String(params["Step3"])) == Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]) {
                                            if (inputsStep3Required[i]['type'] == 'email') {
                                                verifEmail = verificationEmail(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                            else if (inputsStep3Required[i]['type'] == 'url') {
                                                verifUrl = verificationUrl(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                        }
                                        else if (inputsStep3[i]['id'] + "#" + String(params["Step2"] + "#" + String(params["Step3"])) == Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]) {
                                            if (inputsStep3[i]['type'] == 'email') {
                                                verifEmail = verificationEmail(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                            else if (inputsStep3[i]['type'] == 'url') {
                                                verifUrl = verificationUrl(JSON.parse(window.sessionStorage.getItem("State"))[Object.keys(JSON.parse(window.sessionStorage.getItem("State")))[k]]);
                                            }
                                        }
                                    }
                                }
                                if (!verifEmail) {
                                    setError("Vérifiez les champs d'email.");
                                }
                                else if (!verifUrl) {
                                    setError("Vérifiez les champs d'url.");
                                }
                                else {
                                    params["setCurrent"]("ResumStep2");
                                    var listStep3 = [];
                                    for (var k = 0; k < Object.keys(JSON.parse(window.sessionStorage.getItem("Step3"))).length; k++) {
                                        for (var v = 0; v < JSON.parse(window.sessionStorage.getItem("Step3"))[k] + 1; v++) {
                                            listStep3.push(k + '#' + v);
                                        }
                                    }
                                    window.sessionStorage.setItem("ListStep3", JSON.stringify(listStep3));
                                }
                            }
                        });
                    }}>Valider</Button_form>
                </Box>
            </FormControl>
            <Texte>{error}</Texte>
        </Box>
    );
}
