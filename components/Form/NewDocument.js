import React, { useState, useEffect } from 'react';
import { Box, Stack } from "@mui/material";
import { BoxContent, Titre_section, Texte, Button_form } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import Resum from './Resum';
import ResumStep2 from './ResumStep2';
import InputsStep1 from './InputsStep1';
import InputsStep2 from './InputsStep2';
import InputsStep3 from './InputsStep3';

export default function Form() {

    // User et Date
    const { token, setToken } = useToken();
    const [today, setToday] = useState();
    const [historique, setHistorique] = useState({});

    // Nombre d'étapes
    const [Step2, setStep2] = useState(0);
    const [currentStep2, setCurrentStep2] = useState(0);
    const [ListStep2, setListStep2] = useState([]);
    const [Step3, setStep3] = useState([]);
    const [currentStep3, setCurrentStep3] = useState(0);
    const [ListStep3, setListStep3] = useState([]);

    // Etape en cours
    const [state, setState] = useState({});
    const [percent, setPercent] = useState(0);
    const [nbInputs, setNbInputs] = useState(143);
    const [current, setCurrent] = useState("Step1");
    const [error, setError] = useState("");
    const [expanded, setExpanded] = useState(false);
    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const [isInit, setIsInit] = useState(false);

    useEffect(() => {
        setToday(new Date().toLocaleDateString());
    }, [isInit]);

    if (current == "Resum") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section variant='h5'>Informations principales</Titre_section>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step1") {
        return (
            <BoxContent>
                <Stack>
                    <Box><Button_form onClick={() => {
                        window.location.href = "/import";
                    }}>
                        Importer
                    </Button_form></Box>
                </Stack>
                <InputsStep1 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setState={setState} setCurrent={setCurrent}></InputsStep1>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step2") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section variant='h5'>Informations principales</Titre_section>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <InputsStep2 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setState={setState} setCurrent={setCurrent} Step2={currentStep2} ListStep2={ListStep2}></InputsStep2>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "ResumStep2") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section variant='h5'>Informations principales</Titre_section>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={currentStep2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <ResumStep2 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} currentStep2={currentStep2} setStep2={setStep2} setCurrentStep2={setCurrentStep2} ListStep2={ListStep2} Step3={Step3} ListStep3={ListStep3} setStep3={setStep3} currentStep3={currentStep3} setCurrentStep3={setCurrentStep3}></ResumStep2>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
    else if (current == "Step3") {
        return (
            <BoxContent>
                <Box id="info">
                    <Titre_section variant='h5'>Informations principales</Titre_section>
                    <Texte id="user">Nom d'utilisateur : {token}</Texte>
                    <Texte id="date">Date de saisie : {String(today)}</Texte>
                </Box>
                <Resum state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setCurrent={setCurrent} Step2={Step2} setStep2={setStep2} ListStep2={ListStep2} currentStep2={currentStep2} setCurrentStep2={setCurrentStep2}></Resum>
                <ResumStep2 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} current={current} setState={setState} setCurrent={setCurrent} Step2={Step2} currentStep2={currentStep2} setStep2={setStep2} setCurrentStep2={setCurrentStep2} ListStep2={ListStep2} Step3={Step3} ListStep3={ListStep3} setStep3={setStep3} currentStep3={currentStep3} setCurrentStep3={setCurrentStep3}></ResumStep2>
                <InputsStep3 state={state} percent={percent} setPercent={setPercent} nbInputs={nbInputs} setNbInputs={setNbInputs} setState={setState} setCurrent={setCurrent} Step2={currentStep2} Step3={currentStep3} ListStep3={ListStep3}></InputsStep3>
                <Texte>{error}</Texte>
            </BoxContent>
        );
    }
}