import { useState, useEffect } from "react";
import { Box, Stack, Grid } from "@mui/material";
import HomeIcon from '@mui/icons-material/HomeRounded';
import LoginRoundedIcon from '@mui/icons-material/LoginRounded';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import RemoveCircleOutlineRoundedIcon from '@mui/icons-material/RemoveCircleOutlineRounded';
import ViewListRoundedIcon from '@mui/icons-material/ViewListRounded';
import HistoryRoundedIcon from '@mui/icons-material/HistoryRounded';
import ChevronRightRoundedIcon from '@mui/icons-material/ChevronRightRounded';
import { Navigation, Titre_page, Headers, BoxCenter, BoxLeft, LinkNav, Texte_link, LinkNavTop } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import "./App.css";
import extractParamsUrl from '../Functions/extractParamsUrl.js';

const axios = require('axios');

export default function App() {

  const [statusToken, setStatusToken] = useState(0);
  const [isInit, setIsInit] = useState(false);

  useEffect(() => {
    if (isInit === false) {
      StatusToken();
      setIsInit(true);
    }
  }, [isInit]);

  // Récupération de l'url
  var url = window.location.href;
  var input = "";

  var chemin = "";

  if (url.includes("modif_perso")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/modif_perso"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Modifications des informations personnelles</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("modif_password")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/modif_password"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Modifications du mot de passe</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("new_user")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/new_user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Ajout d'un utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("modif_user")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/modif_user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Modification du status utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("supp_user")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/supp_user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Suppression d'un utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("user")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("access")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/user"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Profil utilisateur</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/access"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Modification des accès utilisateurs</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("new")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/new"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Insertion de métadonnées</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("import")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/new"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Insertion de métadonnées</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/import"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Importation d'un fichier JSON</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("supp")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/supp"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Suppression de métadonnées</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("bases")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/bases"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Liste des bases accessibles</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("historique")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/historique"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Historiques des opérations faites sur les métadonnées</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("variable")) {
    var params = extractParamsUrl(url);
    var base = params['base'];
    var table = params['table'];
    var variable = params['variable'];
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/base?" + input + "base=" + base} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Base {base}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/table?" + input + "base=" + base + "&table=" + table} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Table {table}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/variable?" + input + "base=" + base + "&table=" + table + "&variable=" + variable} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Variable {variable}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("table")) {
    var params = extractParamsUrl(url);
    var base = params['base'];
    var table = params['table'];
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '10px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/base?" + input + "base=" + base} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Base {base}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/table?" + input + "base=" + base + "&table=" + table} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Table {table}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("export_base")) {
    var params = extractParamsUrl(url);
    var base = Object.keys(params)[0];
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/base?" + input + "base=" + base} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Base {base}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/export_base"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Export des métadonnées</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("base")) {
    var params = extractParamsUrl(url);
    var base = params['base'];
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
      <LinkNavTop to={"/base?" + input + "base=" + base} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginTop: "5px" }}><ChevronRightRoundedIcon></ChevronRightRoundedIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Base {base}</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else if (url.includes("logout")) {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }
  else {
    chemin = <Stack direction="row" sx={{ marginLeft: 10 }}>
      <LinkNavTop to={"/"} sx={{ marginLeft: '0px' }}>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={0} sx={{ marginRight: "30px" }}><HomeIcon></HomeIcon></Grid>
          <Grid item xs={0} sx={{ marginTop: "10px" }}><Texte_link>Home</Texte_link></Grid>
        </Grid>
      </LinkNavTop>
    </Stack>;
  }

  const { token, setToken } = useToken();

  if (!token || url.includes("logout")) {
    return (
      <Box>
        <Headers>
          <BoxCenter sx={{ marginTop: 2 }}>
            <Stack direction="row">
              <Titre_page>Catalogue de métadonnées</Titre_page>
            </Stack>
          </BoxCenter>
          {chemin}
        </Headers>
        <Navigation>
          <BoxLeft>
            <BoxCenter sx={{ marginLeft: '10px' }}><img class="logo" src="https://www.health-data-hub.fr/themes/custom/health_data_hub/logo.png" /></BoxCenter>
            <br></br>
            <LinkNav to='/' sx={{ marginLeft: '10px' }}><HomeIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Home</LinkNav>
            <br></br>
            <Box sx={{ marginTop: "25px" }}><LinkNav to="/login" sx={{ marginLeft: '10px' }}><LoginRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Connexion</LinkNav></Box>
          </BoxLeft>
        </Navigation>
      </Box>
    );
  }

  // Fonction qui récupère les droits de l'utilisateur
  function StatusToken() {
    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/list_users";

    // Envoi de la requête
    var axios_resp = axios.get(url)
      // Récupération de la réponse
      .then(function (response) {
        var list_users = response.data;
        for (var user = 0; user < list_users.length; user++) {
          if (list_users[user]["username"] == token) {
            if (list_users[user]["status"] == 1) {
              setStatusToken(1);
            }
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function verifStatus() {
    if (statusToken == 1) {
      return (
        <Box>
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/supp" sx={{ marginLeft: '10px' }}><RemoveCircleOutlineRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Supprimer</LinkNav></Box>
          <br></br>
        </Box>
      );
    }

  }
  return (
    <Box>
      <Headers>
        <BoxCenter sx={{ marginTop: 2 }}>
          <Stack direction="row" >
            <Titre_page>Catalogue de métadonnées</Titre_page>
          </Stack>
        </BoxCenter>
        {chemin}
      </Headers>
      <Navigation>
        <BoxLeft>
          <BoxCenter sx={{ marginLeft: '10px' }}><img class="logo" src="https://www.health-data-hub.fr/themes/custom/health_data_hub/logo.png" /></BoxCenter>
          <br></br>
          <Box sx={{ marginTop: "10px" }}><LinkNav to='/' sx={{ marginLeft: '10px' }}><HomeIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Home</LinkNav></Box>
          <br></br>
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/user" sx={{ marginLeft: '10px' }}><AccountCircleRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Profil</LinkNav></Box>
          <br></br>
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/new" sx={{ marginLeft: '10px' }}><AddCircleOutlineRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Ajouter</LinkNav></Box>
          <br></br>
          {verifStatus()}
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/bases" sx={{ marginLeft: '10px' }}><ViewListRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Bases</LinkNav></Box>
          <br></br>
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/historique" sx={{ marginLeft: '10px' }}><HistoryRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Historique</LinkNav></Box>
          <br></br>
          <Box sx={{ marginTop: "10px" }}><LinkNav to="/logout" sx={{ marginLeft: '10px' }}><LogoutRoundedIcon sx={{ mr: 0.5, fontSize: '15px' }} fontSize="small" />Déconnexion</LinkNav></Box>
        </BoxLeft>
      </Navigation>
    </Box>
  );

}
