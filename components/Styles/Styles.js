import { styled } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import { AppBar, Box, Button, InputBase, Typography, Table, TableBody, TableHead, TableRow, TableCell, Container } from "@mui/material";
import { Link } from "react-router-dom";
import { MultipleSelect } from "react-select-material-ui";

var font = "'Montserrat', sans-serif";
var color_mainBlue = "#036287";
var color_light = "#F7F4F9";
var color_title = "#06303A";
var color_texte = "#1D1D1B";

export const Page = styled(Container)(({ theme }) => ({
  maxWidth: 'lg'
}));

export const Headers = styled(AppBar)(({ theme }) => ({
  elevation: 0,
  position: "fixed",
  backgroundColor: color_mainBlue,
  top: 0,
  left: 60,
  height: "100px"
}));

export const BoxCenter = styled(Box)(({ theme }) => ({
  margin: 'auto',
  alignContent: "center",
  alignItems: "center"
}));

export const BoxLeft = styled(Box)(({ theme }) => ({
  marginTop: "20%"
}));

export const BoxContent = styled(Box)(({ theme }) => ({
  marginTop: "120px",
  marginLeft: "10%"
}));

export const Navigation = styled(AppBar)(({ theme }) => ({
  left: 0,
  width: "130px",
  height: "100%",
  backgroundColor: color_light
}));

export const LinkNav = styled(Link)(({ theme }) => ({
  color: color_mainBlue,
  marginLeft: "18%",
  fontFamily: font,
  textDecoration: 'none'
}));

export const LinkNavTop = styled(Link)(({ theme }) => ({
  color: color_light,
  marginLeft: "18%",
  fontFamily: font,
  textDecoration: 'none'
}));

export const Titre_page = styled(Typography)(({ theme }) => ({
  color: color_light,
  textAlign: "center",
  fontFamily: font,
  fontSize: 30
}));

export const Titre_section = styled(Typography)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  fontSize: "20px",
  fontWeight: 'bold',
  marginTop: "150px",
  color: color_title
}));

export const Titre_section_doc = styled(Typography)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  fontSize: "20px",
  fontWeight: 'bold',
  marginTop: "20px",
  color: color_title
}));

export const Titre_section_form = styled(Typography)(({ theme }) => ({
  marginTop: "10px",
  marginBottom: "10px",
  fontSize: "20px",
  fontFamily: font,
  color: color_title
}));

export const Texte = styled(Typography)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  color: color_texte
}));

export const Texte_link = styled(Typography)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  color: color_light
}));

export const Texte_justify = styled(Typography)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  textAlign: 'justify',
  color: color_texte
}));

export const Input_search = styled(InputBase)(({ theme }) => ({
  '& .MuiInputBase-input': {
    size: 'small',
    fontFamily: font,
    borderRadius: 4,
    backgroundColor: theme.palette.mode === 'light' ? '#fcfcfb' : '#2b2b2b',
    border: '1px solid #ced4da',
    fontSize: 14,
    width: '900px',
    transition: theme.transitions.create([
      'border-color',
      'background-color',
      'box-shadow',
    ]),
  },
}));

export const Button_search = styled(Button)(({ theme }) => ({
  color: color_texte,
  backgroundColor: grey[400],
  fontSize: 16,
  fontFamily: font,
  padding: '0px 60px',
  marginLeft: '10px',
  '&:hover': {
    backgroundColor: grey[600],
  },
}));

export const Select_search = styled(MultipleSelect)(({ theme }) => ({
  size: 'small',
  fontFamily: font,
  color: color_texte,
  borderRadius: 4,
  backgroundColor: theme.palette.mode === 'light' ? '#fcfcfb' : '#2b2b2b',
  border: '1px solid #ced4da',
  fontSize: 14,
  width: '500px',
  height: '40px',
  transition: theme.transitions.create([
    'border-color',
    'background-color',
    'box-shadow',
  ])
}));

export const Input_login = styled(InputBase)(({ theme }) => ({
  '& .MuiInputBase-input': {
    borderRadius: 4,
    backgroundColor: theme.palette.mode === 'light' ? '#fcfcfb' : '#2b2b2b',
    border: '1px solid #ced4da',
    fontSize: 16,
    fontFamily: font,
    color: color_texte,
    width: 'auto',
    padding: '0px 20px',
    marginBottom: '10px',
    transition: theme.transitions.create([
      'border-color',
      'background-color',
      'box-shadow',
    ]),
  },
}));

export const Button_login = styled(Button)(({ theme }) => ({
  color: color_texte,
  backgroundColor: grey[400],
  fontSize: 16,
  fontFamily: font,
  padding: '0px 12px',
  marginTop: '20px',
  marginBottom: '10px',
  '&:hover': {
    backgroundColor: grey[600],
  },
}));

export const Tableau = styled(Table)(({ theme }) => ({
}));

export const Tableau_head = styled(TableHead)(({ theme }) => ({
  backgroundColor: grey[400]
}));

export const Tableau_cell_head = styled(TableCell)(({ theme }) => ({
  textAlign: 'center',
  width: "50%"
}));

export const Tableau_body = styled(TableBody)(({ theme }) => ({
}));

export const Tableau_row = styled(TableRow)(({ theme }) => ({
}));

export const Tableau_cell = styled(TableCell)(({ theme }) => ({
  textAlign: 'left',
  width: '100%'
}));

export const Input_new = styled(InputBase)(({ theme }) => ({
  marginBottom: "10px",
  fontFamily: font,
  color: color_texte,
  width: "100%",
  border: "1px solid grey",
  borderRadius: 4
}));

export const Button_form = styled(Button)(({ theme }) => ({
  color: color_texte,
  backgroundColor: grey[400],
  fontFamily: font,
  marginTop: "20px",
  marginBottom: "10px",
  padding: '0px 12px',
  '&:hover': {
    backgroundColor: grey[600],
  },
}));

export const AppBarHistorique = styled(AppBar)(({ theme }) => ({
  elevation: 0,
  position: "fixed",
  backgroundColor: color_mainBlue,
  top: "80px",
  left: "160px",
  height: "40px",
  width: "90%"
}));