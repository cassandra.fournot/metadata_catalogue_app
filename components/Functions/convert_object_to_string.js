export default function convert_object_to_string(liste){
  var value = "";
  var keys = Object.keys(liste);
  var values = Object.values(liste);
  for (var k=0; k<keys.length; k++){
    if (typeof(values[k]) === 'object'){
      value += convert_object_to_string(values[k]);
    }
    else {
      value += keys[k] + "&emsp;" + values[k] + "<br>";
    }
  }
  
  return value.slice(0,-2);
}