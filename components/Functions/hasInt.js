// Fonction qui retourne true si la chaîne de caractère contient un chiffre
export default function hasInt(str) {
    return str.match(/[0-9]/) != null;
}