// Fonction qui permet d'extraire les paramètres d'une url
export default function extractParamsUrl(url)
{
  url = url.split('?');
  url = url[1].split('&');
  var result = {};
 
  url.forEach(function(el){
        var param = el.split('=');
        param[0] = param[0];
        result[param[0]] = param[1];
    });
 
    return result;
}