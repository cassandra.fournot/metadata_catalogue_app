// Fonction qui retourne true si la chaîne de caractère contient une majuscule
export default function hasHupperCase(str) {
    return str.toLowerCase() != str;
}