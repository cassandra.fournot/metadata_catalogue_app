export default function verificationUrl(input) {
    var re = new RegExp("^((http|https)://){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|?)?]+)+$");
    if (re.test(input)){
        return true
    }
    else {
        return false
    }
}