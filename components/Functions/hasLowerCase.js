// Fonction qui retourne true si la chaîne de caractère contient une minuscule
export default function hasLowerCase(str) {
    return str.toUpperCase() != str;
}