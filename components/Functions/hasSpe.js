// Fonction qui retourne true si la chaîne de caractère contient un caractère spéciale
export default function hasSpe(str) {
    return str.match(/(?=.*[-+!*$@%&_])/) != null;
}