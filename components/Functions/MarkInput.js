export default function MarkInput(value, input){
  
    var valueLower = value.toLowerCase();
    var inputLower = input.toLowerCase();
  
    var occurrences = valueLower.split(inputLower);
    var indexOccurrences = [];
    var indexMark = [];
    var debut = 0;
  
    for (var occurrence=0; occurrence < occurrences.length; occurrence++){
      var indexe = valueLower.indexOf(occurrences[occurrence], debut);
      indexOccurrences.push({"indexe": indexe, "longeur": occurrences[occurrence].length});
      debut = indexe + occurrences[occurrence].length;
    }
  
    for (var i=0; i < Object.keys(indexOccurrences).length; i++){
      for (var add=0; add < indexOccurrences[i].longeur; add++) {
        indexMark.push(indexOccurrences[i].indexe+add)
      }    
    }
   
    var debut = -1;
    return (
      value.split('').map((i)=>{
        debut += 1;
        if (indexMark.includes(value.indexOf(i, debut))){
          return(<spans>{i}</spans>)
        }
        else {
          return(<span><mark>{i}</mark></span>)
        }
      })
    )
    
  }