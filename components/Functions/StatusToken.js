const axios = require('axios');

// Fonction qui récupère les droits de l'utilisateur
export default function StatusToken(token, setStatusToken) {

    // Définition de l'URL (pour la requête)
    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/list_users";

    // Envoi de la requête
    var axios_resp = axios.get(url)
        // Récupération de la réponse
        .then(function (response) {
            var list_users = response.data;
            for (var user = 0; user < list_users.length; user++) {
                if (list_users[user]["username"] == token) {
                    if (list_users[user]["status"] == 1) {
                        setStatusToken(1);
                    }
                }
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}