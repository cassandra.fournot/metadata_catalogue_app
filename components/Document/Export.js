import React, { useState, useEffect } from "react";
import { Box, Grid, Select, MenuItem } from "@mui/material";
import { BoxContent, Titre_section, Texte, Button_form, Texte_justify } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import extractParamsUrl from '../Functions/extractParamsUrl';

const axios = require('axios');
var FileSaver = require('file-saver');

function Export() {

  const { token, setToken } = useToken();
  const [today, setToday] = useState();
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [isInit, setIsInit] = useState(false);
  const [statusType, setStatusType] = useState(0);
  const [listType, setListType] = useState([]);
  const [ID, setID] = useState("");

  useEffect(() => {
    setToday(new Date().toLocaleDateString());
    setListType(["JSON"]);
    setID(Object.keys(extractParamsUrl(window.location.href))[0]);
  }, [isInit]);

  // Fonction qui permet de sauvegarder les modifications d'une base
  function Exportation(type) {
    if (type != 0) {
      setError("");

      // Définition de l'URL
      var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/read_bases/" + ID;

      // Envoi de la requête pour vérification que la base n'existe pas déjà
      var axios_resp = axios.get(url)
        // Récupération de la réponse
        .then(function (response) {
          var data = response.data;
          var fileName = "";
          var fileToSave = "";

          if (type == "JSON") {
            fileName = "Metadata_" + response.data.id + ".json";
            fileToSave = new Blob([JSON.stringify(data, null, 4)], {
              type: 'application/json',
              name: fileName
            });
          }

          FileSaver.saveAs(fileToSave, fileName);
          setSuccess("Votre fichier a bien été téléchargé.");
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    else {
      setSuccess("");
      setError("Merci de séléctionner un type de fichier.");
    }
  };

  return (
    <BoxContent>
      <Box sx={{ marginTop: "30px" }}>
        <Titre_section variant='h5'>Sélectionner un type de fichier pour l'exportation</Titre_section>
        <Texte_justify>Merci de seléctionner le type de fichier pour lequel vous souhaitez exporter les métadonnées et cliquer sur Exporter. Le téléchargement se lancera automatiquement et vous pourrez retrouver le fichier contenant les métadonnées dans vos Téléchargements.</Texte_justify>
        <Grid container sx={{ alignItems: "center" }}>
          <Grid item xs={2.2} sx={{ marginTop: "20px" }}><Texte>Choisir le type de fichier :</Texte></Grid>
          <Grid item xs={4}>
            <Select
              value={statusType}
              onChange={(event) => { setStatusType(event.target.value); }}
              sx={{ width: '200px', marginTop: '10px' }}
              size="small"
            >
              <MenuItem value={0}>Type</MenuItem>
              {listType.map((e) => {
                return (
                  <MenuItem value={e}>{e}</MenuItem>
                );
              })}
            </Select>
          </Grid>
        </Grid>
      </Box>
      <Box><Button_form onClick={() => {
        Exportation(statusType);
      }}>
        Exporter
      </Button_form></Box>
      <Texte>{error}</Texte>
      <Texte>{success}</Texte>
    </BoxContent>
  );

}

export default Export;
