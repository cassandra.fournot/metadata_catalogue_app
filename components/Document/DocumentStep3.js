import React, { useEffect, useState } from 'react';
import { LinkNav, Texte, Titre_section_doc, Texte_justify } from "../Styles/Styles.js";
import { Box, Stack, Grid, Checkbox, FormControlLabel } from "@mui/material";
import List_Step3 from '../Listes/DocumentStep3';
import EssentielsStep3 from '../Listes/EssentielsStep3';
import List_inputs from '../Listes/Inputs';
import useToken from '../Token/Token';
import DocumentsUser from '../Listes/DocumentsUser';
import MarkInput from '../Functions/MarkInput.js';
import extractParamsUrl from '../Functions/extractParamsUrl.js';
import FileUploadRoundedIcon from '@mui/icons-material/FileUploadRounded';
import ModeEditRoundedIcon from '@mui/icons-material/ModeEditRounded';

// Ajout d'axios
const axios = require('axios');

export default function Document() {

  const { token, setToken } = useToken();
  const [listStep, setListStep] = useState([]);
  const [listStepKey, setListStepKey] = useState([]);
  const [listEssentiels, setListEssentiels] = useState([]);
  const [isInit, setIsInit] = useState(false);
  const [essentiel, setEssentiel] = useState(false);
  const [listDocument, setListDocument] = useState([]);
  const [statusModif, setStatusModif] = useState(0);
  const [expanded, setExpanded] = useState(false);


  const [categoriesStep, setCategoriesStep] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  // Récupération de l'url
  var url = window.location.href;

  if (url.includes('bases') == false) {
    if (url.includes('base')) {
      // Récupération des paramètre de l'url
      var params = extractParamsUrl(url);
      var base = params['base'];
      if (url.includes('input')) {
        var input = params['input'];
        var link_input = "input=" + input + "&";
      }
      if (url.includes('table')) {
        var table = params['table'];
      }
      if (url.includes('variable')) {
        var variable = params['variable'];
      }
    }
  }

  async function setStep() {
    const inputs = await List_inputs();
    setCategoriesStep(inputs['Categories_Step3']);

    var data = { base: base, table: table, variable: variable };
    const response = await List_Step3(data);
    setListStep(response);
    setListStepKey(Object.keys(response));
  }

  async function setEssentielsStep() {
    const response = await EssentielsStep3();
    setListEssentiels(response[0].data);
  }

  async function ListDocument() {
    const response = await DocumentsUser(token);
    for (var document = 0; document < response.length; document++) {
      if (response[document]["id"] == base) {
        setStatusModif(1);
      }
    }
    setListDocument(response);
  }

  useEffect(() => {
    if (isInit == false) {
      setStep();
      setEssentielsStep();
      ListDocument();
      setIsInit(true);
    }
  }, [isInit]);

  function verifCategorieEssentielles(c) {
    if (c.listChamps.filter(value => listEssentiels.includes(value)).length > 0) {
      return (
        <Titre_section_doc>{c.categorie}</Titre_section_doc>
      );
    }
  }

  function Content() {
    if (essentiel == true) {
      return (
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          {categoriesStep.map((c) => {
            return (
              <Box>
                {verifCategorieEssentielles(c)}
                <Box>
                  {listStepKey.map((e) => {
                    if (c.listChamps.includes(e)) {
                      console.log(listEssentiels);
                      if (listEssentiels.includes(e)) {
                        if (input) {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                              <Grid item xs={7.8}><Texte_justify>{MarkInput(listStep[e]["value"], input)}</Texte_justify></Grid>
                            </Grid>
                          );
                        }
                        else {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                              <Grid item xs={7.8}><Texte_justify>{listStep[e]["value"]}</Texte_justify></Grid>
                            </Grid>
                          );
                        }
                      }
                    }
                  })}
                </Box>
              </Box>
            );
          })}
        </Box>
      );
    }
    else {
      return (
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          {categoriesStep.map((c) => {
            return (
              <Box>
                <Titre_section_doc>{c.categorie}</Titre_section_doc>
                <Box>
                  {listStepKey.map((e) => {
                    if (c.listChamps.includes(e)) {
                      if (input) {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                            <Grid item xs={7.8}><Texte_justify>{MarkInput(listStep[e]["value"], input)}</Texte_justify></Grid>
                          </Grid>
                        );
                      }
                      else {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                            <Grid item xs={7.8}><Texte_justify>{listStep[e]["value"]}</Texte_justify></Grid>
                          </Grid>
                        );
                      }
                    }
                  })}
                </Box>
              </Box>
            );
          })}
        </Box>
      );
    }
  }

  function verifStatusModif(statusModif) {
    if (statusModif == 1) {
      return (
        <LinkNav to={"/edit_base?" + base} sx={{ marginLeft: '10px' }}>
          <Grid container sx={{ alignItems: "center" }}>
            <Grid item xs={1} sx={{ marginRight: "15px" }}><ModeEditRoundedIcon></ModeEditRoundedIcon></Grid>
            <Grid item xs={1} sx={{ marginTop: "6px" }}><Texte_justify>Modifier</Texte_justify></Grid>
          </Grid>
        </LinkNav>
      );
    }
  }

  return (
    <Box>
      <Stack direction="row" alignItems="center" spacing="auto" sx={{ marginRight: '0px' }}>
        <FormControlLabel control={<Checkbox id="essentiels" onChange={() => { setEssentiel(!essentiel); }} />} label="Essentiels" />
        <Stack direction="row" alignItems="center" justifyContent="flex-end">
          {verifStatusModif(statusModif)}
          <LinkNav to={"/export_base?" + base} sx={{ marginLeft: '10px' }}>
            <Grid container sx={{ alignItems: "center" }}>
              <Grid item xs={1} sx={{ marginRight: "15px" }}><FileUploadRoundedIcon></FileUploadRoundedIcon></Grid>
              <Grid item xs={1} sx={{ marginTop: "6px" }}><Texte_justify>Exporter</Texte_justify></Grid>
            </Grid>
          </LinkNav>
        </Stack>
      </Stack>
      {Content()}
    </Box>
  );

}
