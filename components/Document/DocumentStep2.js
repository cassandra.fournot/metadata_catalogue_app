import React, { useEffect, useState } from 'react';
import { LinkNav, Texte, Titre_section_doc, Texte_justify } from "../Styles/Styles.js";
import { Box, Stack, Grid, Checkbox, FormControlLabel } from "@mui/material";
import List_Step2 from '../Listes/DocumentStep2';
import EssentielsStep2 from '../Listes/EssentielsStep2';
import List_inputs from '../Listes/Inputs';
import useToken from '../Token/Token';
import DocumentsUser from '../Listes/DocumentsUser';
import MarkInput from '../Functions/MarkInput.js';
import convert_object_to_string from '../Functions/convert_object_to_string.js';
import extractParamsUrl from '../Functions/extractParamsUrl.js';
import FileUploadRoundedIcon from '@mui/icons-material/FileUploadRounded';
import ModeEditRoundedIcon from '@mui/icons-material/ModeEditRounded';

// Ajout d'axios
const axios = require('axios');

export default function Document() {

  const { token, setToken } = useToken();
  const [listStep, setListStep] = useState([]);
  const [listStepKey, setListStepKey] = useState([]);
  const [listEssentiels, setListEssentiels] = useState([]);
  const [isInit, setIsInit] = useState(false);
  const [essentiel, setEssentiel] = useState(false);
  const [listDocument, setListDocument] = useState([]);
  const [statusModif, setStatusModif] = useState(0);
  const [expanded, setExpanded] = useState(false);


  const [categoriesStep2, setCategoriesStep2] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  // Récupération de l'url
  var url = window.location.href;

  if (url.includes('bases') == false) {
    if (url.includes('base')) {
      // Récupération des paramètre de l'url
      var params = extractParamsUrl(url);
      var base = params['base'];
      if (url.includes('input')) {
        var input = params['input'];
        var link_input = "input=" + input + "&";
      }
      else {
        var link_input = "";
      }
      if (url.includes('table')) {
        var table = params['table'];
      }
    }
  }

  async function setStep2() {
    const inputs = await List_inputs();
    setCategoriesStep2(inputs['Categories_Step2']);

    var data = { base: base, table: table };
    const response = await List_Step2(data);
    setListStep(response);
    setListStepKey(Object.keys(response));
  }

  async function setEssentielsStep2() {
    const response = await EssentielsStep2();
    setListEssentiels(response[0].data);
  }

  async function ListDocument() {
    const response = await DocumentsUser(token);
    for (var document = 0; document < response.length; document++) {
      if (response[document]["id"] == base) {
        setStatusModif(1);
      }
    }
    setListDocument(response);
  }

  useEffect(() => {
    if (isInit == false) {
      setStep2();
      setEssentielsStep2();
      ListDocument();
      setIsInit(true);
    }
  }, [isInit]);

  function verifCategorieEssentielles(c) {
    if (c.listChamps.filter(value => listEssentiels.includes(value)).length > 0) {
      return (
        <Titre_section_doc>{c.categorie}</Titre_section_doc>
      );
    }
  }

  function Content() {
    if (essentiel == true) {
      return (
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          {categoriesStep2.map((c) => {
            return (
              <Box>
                {verifCategorieEssentielles(c)}
                <Box>
                  {listStepKey.map((e) => {
                    if (c.listChamps.includes(e)) {
                      console.log(listEssentiels);
                      if (listEssentiels.includes(e)) {
                        if (input) {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                              <Grid item xs={7.8}><Texte_justify>{MarkInput(listStep[e]["value"], input)}</Texte_justify></Grid>
                            </Grid>
                          );
                        }
                        else {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                              <Grid item xs={7.8}><Texte_justify>{listStep[e]["value"]}</Texte_justify></Grid>
                            </Grid>
                          );
                        }
                      }
                    }
                  })}
                </Box>
              </Box>
            );
          })}
          {listStepKey.map((e) => {
            if (listStepKey.indexOf(e) == listStepKey.length - 1) {
              return (
                <Box>
                  <Titre_section_doc>{listStep[e]["categorie"]}</Titre_section_doc>
                  <Grid container sx={{ alignItems: "centtoper" }}>
                    <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                    <Grid item xs={7}>{listStep[e]["value"].map((l) => {
                      var link = "/variable?" + link_input + "base=" + base + "&table=" + table + "&variable=" + l["id"];
                      if (input) {
                        if (convert_object_to_string(l["data"]).toLowerCase().includes(input.toLowerCase())) {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte><mark>{l['id']}</mark></Texte></LinkNav></Grid>
                            </Grid>
                          );
                        }
                        else {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte>{l['id']}</Texte></LinkNav></Grid>
                            </Grid>
                          );
                        }
                      }
                      else {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte>{l['id']}</Texte></LinkNav></Grid>
                          </Grid>
                        );
                      }
                    })}</Grid>
                  </Grid>
                </Box>
              );
            }
          })}
        </Box>
      );
    }
    else {
      return (
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          {categoriesStep2.map((c) => {
            return (
              <Box>
                <Titre_section_doc>{c.categorie}</Titre_section_doc>
                <Box>
                  {listStepKey.map((e) => {
                    if (c.listChamps.includes(e)) {
                      if (input) {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                            <Grid item xs={7.8}><Texte_justify>{MarkInput(listStep[e]["value"], input)}</Texte_justify></Grid>
                          </Grid>
                        );
                      }
                      else {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                            <Grid item xs={7.8}><Texte_justify>{listStep[e]["value"]}</Texte_justify></Grid>
                          </Grid>
                        );
                      }
                    }
                  })}
                </Box>
              </Box>
            );
          })}
          {listStepKey.map((e) => {
            if (listStepKey.indexOf(e) == listStepKey.length - 1) {
              return (
                <Box>
                  <Titre_section_doc>{listStep[e]["categorie"]}</Titre_section_doc>
                  <Grid container sx={{ alignItems: "centtoper" }}>
                    <Grid item xs={4} sx={{ marginRight: "10px" }}><Texte>{listStep[e]["Metadata"]} ({listStep[e]["RDF"]}) :</Texte></Grid>
                    <Grid item xs={7}>{listStep[e]["value"].map((l) => {
                      var link = "/variable?" + link_input + "base=" + base + "&table=" + table + "&variable=" + l["id"];
                      if (input) {
                        if (convert_object_to_string(l["data"]).toLowerCase().includes(input.toLowerCase())) {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte><mark>{l['id']}</mark></Texte></LinkNav></Grid>
                            </Grid>
                          );
                        }
                        else {
                          return (
                            <Grid container sx={{ alignItems: "center" }}>
                              <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte>{l['id']}</Texte></LinkNav></Grid>
                            </Grid>
                          );
                        }
                      }
                      else {
                        return (
                          <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={3.5} sx={{ marginRight: "10px" }}><LinkNav to={link} sx={{ backgroundColor: '#FFFF00' }}><Texte>{l['id']}</Texte></LinkNav></Grid>
                          </Grid>
                        );
                      }
                    })}</Grid>
                  </Grid>
                </Box>
              );
            }
          })}
        </Box>
      );
    }
  }

  function verifStatusModif(statusModif) {
    if (statusModif == 1) {
      return (
        <LinkNav to={"/edit_base?" + base} sx={{ marginLeft: '10px' }}>
          <Grid container sx={{ alignItems: "center" }}>
            <Grid item xs={1} sx={{ marginRight: "15px" }}><ModeEditRoundedIcon></ModeEditRoundedIcon></Grid>
            <Grid item xs={1} sx={{ marginTop: "6px" }}><Texte_justify>Modifier</Texte_justify></Grid>
          </Grid>
        </LinkNav>
      );
    }
  }

  return (
    <Box>
      <Stack direction="row" alignItems="center" spacing="auto" sx={{ marginRight: '0px' }}>
        <FormControlLabel control={<Checkbox id="essentiels" onChange={() => { setEssentiel(!essentiel); }} />} label="Essentiels" />
        <Stack direction="row" alignItems="center" justifyContent="flex-end">
          {verifStatusModif(statusModif)}
          <LinkNav to={"/export_base?" + base} sx={{ marginLeft: '10px' }}>
            <Grid container sx={{ alignItems: "center" }}>
              <Grid item xs={1} sx={{ marginRight: "15px" }}><FileUploadRoundedIcon></FileUploadRoundedIcon></Grid>
              <Grid item xs={1} sx={{ marginTop: "6px" }}><Texte_justify>Exporter</Texte_justify></Grid>
            </Grid>
          </LinkNav>
        </Stack>
      </Stack>
      {Content()}
    </Box>
  );

}
