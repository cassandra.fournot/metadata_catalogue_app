import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { BoxContent, Titre_section, Texte, Input_login, Button_login, LinkNav } from "../Styles/Styles.js";
import { Box } from "@mui/material";

async function loginUser(credentials) {
  return fetch('http://workmetadata.francecentral.cloudapp.azure.com:5004/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json());
}

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [error, setError] = useState();

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password
    });
    setToken(token);

    if (Object.keys(token).length == 0) {
      setError("Nom d'utilisateur ou mot de passe incorrect.");
    }
    else {
      if (!document.location.href.includes("login")) {
        document.location.href = document.location.href;
      }
      else {
        document.location.href = "/";
      }
    }

  };

  return (
    <BoxContent>
      <Titre_section variant="h5">Connexion à votre compte personnel</Titre_section>
      <form onSubmit={handleSubmit}>
        <Box>
          <Texte>Nom d'utilisateur</Texte>
          <Input_login type="text" onChange={e => setUserName(e.target.value)} />
          <Texte>Mot de passe</Texte>
          <Input_login type="password" onChange={e => setPassword(e.target.value)} />
        </Box>
        <Box><LinkNav to='/lost_password' sx={{ marginLeft: '0px' }}>Mot de passe oublié</LinkNav></Box>
        <Box>
          <Button_login type="submit">Connexion</Button_login>
        </Box>
        <Texte>{error}</Texte>
      </form>
    </BoxContent>
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
};
