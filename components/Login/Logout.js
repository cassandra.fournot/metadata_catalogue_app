import React from 'react';
import { BoxContent } from "../Styles/Styles.js";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import useToken from '../Token/Token';

function removeToken() {
  sessionStorage.removeItem('token');
};

export default function Logout() {

  const { token, setToken } = useToken();
  const username = token;

  removeToken();

  return (
    <BoxContent>
      <MuiThemeProvider>
        <h1>À bientôt {username.toUpperCase()}</h1>
      </MuiThemeProvider>
    </BoxContent>
  );
}