import React, { useState, useEffect } from "react";
import { Box, TextField, Grid } from "@mui/material";
import { BoxContent, Texte_justify, Texte, Button_form, Titre_section } from "../Styles/Styles.js";
import useToken from '../Token/Token';
import hasLowerCase from "../Functions/hasLowerCase.js";
import hasUpperCase from "../Functions/hasUpperCase.js";
import hasInt from "../Functions/hasInt.js";
import hasSpe from "../Functions/hasSpe.js";
import verificationEmail from "../Functions/verificationEmail.js";
import extractParamsUrl from "../Functions/extractParamsUrl.js";
import Users from "../Listes/Users";

const axios = require('axios');

export default function LostPassword() {

    const [error, setError] = useState("");
    const [confirm, setConfirm] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [mail, setMail] = useState("");
    const [statusToken, setStatusToken] = useState(0);
    const [listUsers, setListUsers] = useState([]);
    const [isInit, setIsInit] = useState(false);

    const [color_nb, setColorNb] = useState("red");
    const [color_min, setColorMin] = useState("red");
    const [color_maj, setColorMaj] = useState("red");
    const [color_int, setColorInt] = useState("red");
    const [color_spe, setColorSpe] = useState("red");

    // Variables de vérification
    const [validationPost, setValidationPost] = useState([]);

    useEffect(() => {
        if (isInit === false) {
            StatusToken();
            ListUsers();
            setIsInit(true);
        }
    }, [isInit]);

    async function ListUsers() {
        const response = await Users();
        setListUsers(response[0]);
    }

    // Fonction qui envoie un mail à l'utilisateur si son adresse mail est associée à un compte utilisateur
    function SendMail(mail) {
        // Vérification qu'il s'agit bien d'une adresse mail valide
        if (!verificationEmail(mail)) {
            setError("Merci de bien vouloir saisir une adresse mail valide.");
        }
        else {
            for (var user = 0; user < listUsers.length; user++) {
                // Vérification que l'adresse mail est associée à un compte utilisateur
                if (listUsers[user]["mail"] == mail) {
                    // Définition de l'URL (pour la requête)
                    var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/lost_password";

                    // Envoi de la requête
                    var axios_resp = axios.post(url, { "mail": mail, "username": listUsers[user]["username"] })
                        // Récupération de la réponse
                        .then(function (response) {
                            if (response.status == 200) {
                                setValidationPost(true);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    setConfirm("Un mail vous a été envoyé pour récupérer votre mot de passe.");
                }
                else {
                    setError("Aucun compte n'est associé à cette adresse mail.");
                }
            }
        }
    }

    // Fonction qui récupère les droits de l'utilisateur
    function StatusToken() {
        if (!extractParamsUrl(window.location.href)) {
            setStatusToken(0);
        }
        else {
            if (verificationEmail(Object.keys(extractParamsUrl(window.location.href))[0])) {
                setStatusToken(1);
            }
        }
    }

    // Fonction qui permet de sauvegarder les modifications du nouveau mot de passe
    function Modification(state) {
        if (state["password"] === state["passwordConfirm"]) {
            if (!state["password"].match(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%&_])([-+!*$@%&_\w]{8,50})$/)) {
                setError("Le mot de passe doit contenir respecter les conditions ci-dessus.");
            }
            else {
                // Définition de l'URL (pour la requête)
                var url = "http://workmetadata.francecentral.cloudapp.azure.com:5004/modif_password";

                // Envoi de la requête
                var axios_resp = axios.post(url, state)
                    // Récupération de la réponse
                    .then(function (response) {
                        if (response.status == 200) {
                            setValidationPost(true);
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                if (validationPost) {
                    document.location.href = "success_modif_password";
                }
                else {
                    setError("Un problème est apparu lors de la modification.");
                }
            }
        }
        else {
            setError("Les deux mots de passe saisis ne sont pas identiques.");
        }
    };

    if (confirm != "") {
        return (
            <BoxContent>
                <Texte>{confirm}</Texte>
            </BoxContent>
        );
    }
    else {
        if (statusToken == 0) {
            return (
                <BoxContent>
                    <Texte_justify>Merci de bien vouloir saisir votre adresse mail.</Texte_justify>
                    <Box>
                        <Titre_section variant='h5' sx={{ marginTop: "20px" }}>Informations de connexion</Titre_section>
                        <Grid container sx={{ alignItems: "center" }}>
                            <Grid item xs={2}><Texte>Adresse mail :</Texte></Grid>
                            <Grid item xs={9}>
                                <TextField required type="mail" label="Adresse mail" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => {
                                    setMail(event.target.value);
                                }} />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box>
                        <Button_form id="button" onClick={() => { SendMail(mail); }}>Valider</Button_form>
                        <Texte>{error}</Texte>
                    </Box>
                </BoxContent>
            );
        }
        else {
            return (
                <BoxContent>
                    <Texte_justify>Vous pouvez maintenant saisir votre nouveau mot de passe.</Texte_justify>
                    <Texte_justify>Le mot de passe doit contenir respecter les conditions suivantes :</Texte_justify>
                    <Texte_justify sx={{ color: color_nb }}>- entre 8 et 50 catactères,</Texte_justify>
                    <Texte_justify sx={{ color: color_min }}>- au moins une lettre minuscule,</Texte_justify>
                    <Texte_justify sx={{ color: color_maj }}>- au moins une lettre majuscule,</Texte_justify>
                    <Texte_justify sx={{ color: color_int }}>- au moins un chiffre</Texte_justify>
                    <Texte_justify sx={{ color: color_spe }}>- au moins un de ces caractères spéciaux : & $ @ % * + - _ !</Texte_justify>
                    <br></br>
                    <Grid container sx={{ alignItems: "center" }}>
                        <Grid item xs={3.2}><Texte>Saisir votre nouveau mot de passe :</Texte></Grid>
                        <Grid item xs={4}>
                            <TextField type="password" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => {
                                setPassword(event.target.value);
                                if (event.target.value.length >= 8 && event.target.value.length <= 50) {
                                    setColorNb("green");
                                }
                                else {
                                    setColorNb("red");
                                }
                                if (hasLowerCase(event.target.value)) {
                                    setColorMin("green");
                                }
                                else {
                                    setColorMin("red");
                                }
                                if (hasUpperCase(event.target.value)) {
                                    setColorMaj("green");
                                }
                                else {
                                    setColorMaj("red");
                                }
                                if (hasInt(event.target.value)) {
                                    setColorInt("green");
                                }
                                else {
                                    setColorInt("red");
                                }
                                if (hasSpe(event.target.value)) {
                                    setColorSpe("green");
                                }
                                else {
                                    setColorSpe("red");
                                }
                            }} />
                        </Grid>
                    </Grid>
                    <Grid container sx={{ alignItems: "center" }}>
                        <Grid item xs={4.05}><Texte>Confirmation de votre nouveau mot de passe :</Texte></Grid>
                        <Grid item xs={4}>
                            <TextField type="password" sx={{ marginBottom: "10px" }} size="small" onChange={(event) => { setPasswordConfirm(event.target.value); }} />
                        </Grid>
                    </Grid>
                    <Box>
                        <Button_form id="button" onClick={() => { Modification({ "username": Object.keys(extractParamsUrl(window.location.href))[0], "password": password, "passwordConfirm": passwordConfirm }); }}>Modifier</Button_form>
                        {error.split("\n").map((e) => {
                            return (<Texte>{e}</Texte>);
                        })}
                    </Box>
                </BoxContent>
            );
        }
    }
}