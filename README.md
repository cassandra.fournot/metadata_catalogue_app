# Description
L’application Catalogue de Métadonnées est un outil qui permet de stocker les métadonnées (au format JSON) d’un certain nombre de bases de données, de les afficher et de faire des recherches dessus. L’application prend en compte trois niveaux hiérarchiques, ici : table, base et variable (mais vous pouvez en ajouter autant que vous souhaitez). 

L'utilisateur a la possibilité de se connecter à un compte, qui lui permettra de modifier les métadonnées des bases pour lesquelles il est autorisé à le faire, ou naviguer en lecture seule parmi les métadonnées de toutes les bases enregistrées au catalogue de métadonnées. Les utilisateurs administrateurs ont aussi la possibilité d'ajouter de nouvelles bases au catalogue à l'aide d'un formulaire et d'ajouter de nouveau utilisateur. Ces derniers peuvent sauvegarder un formulaire commencé mais non terminé afin de e compléter plus tard. Un historique des insertions et modifications des métadonnées est fait et visible pour les utilisateurs. L'ensemble des utilisateurs peut faire des recherches parmi les bases du catalogue (avec la possibilité de filtrer la recherche parmi certaines bases) et des recherches parmi les tables d'une base (avec la possibilité de filtrer la recherche parmi certaines tables). Les occurrences trouvées seront sURLignées en jaune dans les trois niveaux base, table et variable. 
 
L'application est créée sur la base d'une architecture en micro-services. L'interface de l'application est créé à partir d'une application React. Le stockage des métadonnées se fait avec l'API MeiliSearch et le stockage des informations utilisateurs (informations de connexion, informations personnelles, informations sur les accès des utilisateurs à certaines bases) et les informations de l'historique (insertion et modification) sont stockées sur un serveur MySQL. Le lien entre tous ces services se fait à l'aide d'une API Python créée avec FastAPI.

# MeiliSearch
L’API de recherche MeiliSearch est utilisée pour stocker les métadonnées et pour voir faire des recherches en texte libre. 
 
## Usage
Pour lancer votre API de recherche, utilisez la commande suivante en remplaçant 5002 par le port sur lequel vous souhaitez lancer votre API : 
`sudo docker run -p 5002:7700 -v "$(pwd)/data.ms:/data.ms" getmeili/meilisearch`
 
Vous pouvez accéder au serveur de l’API à l’adresse 0.0.0.0/5002 (en remplaçant 5002 par le port sur lequel vous avez lancé votre API). 

# MySQL
Le serveur MySQL es tutilisé pour stocker les informations concernant les utilisateurs et l'historique des insertions et des modifications. L'historique ne prend pas en compte les formulaires qui sont commencés et sauvegardés mais non terminés.
 
## Usage
Pour lancer votre serveur MySQL, utilisez la commande suivante en remplaçant mysql--server par le nom que vous avez donné à votre image docker lors de la création de votre serveur MySQL : 
`sudo docker restart mysql-server`
 
Vous pouvez ouvrir votre serveur MySQL afin de visualiser les informations stockées sur le serveur et faire des actions dessus à l'aide de la commande suivante en remplaçant mysql--server par le nom que vous avez donné à votre image docker lors de la création de votre serveur MySQL :
`sudo docker exec -it mysql-server mysql -u root -p`

## Tables 
Le serveur MySQL contient plusieurs tables : 
- **login** : contient le nom d'utilisateur et le mot de passe permettant aux utilisateurs de se connecter
- **users** : contient le nom d'utilisateur, l'adresse mail et le status (0 pour administrateur et 1 pour utilisateur standard) de chaque utilisateur
- **bases** : contient l'identifiant des bases et le nom d'utilisateur des personnes ayant accès à la modification de ses bases
- **insertion** : contient le nom d'utilisateur ayant inséré une nouvelle base, la date de l'insertion, l'identifiant de la base qui a été insérée
- **moficiation** : contient le nom d'utilisateur ayant effectué une modification, la date de modification, l'identifiant de la base qui a été modifiée, le champ modifié et la valeur de la modification

# API Python (FastAPI)
L'API Python est créé avec FastAPI et permet de faire le lien entre les différents services et notamment entre l'application React et l'API MeiliSearch ou le serveur MySQL afin de récupérer les informations que l'application doit afficher. 

## Usage
Pour lancer votre API, utilisez la commande suivante en vous plaçant dans le dossier contenant votre fichier Python et en remplaçant python3.9 par votre version de Python : 
`python3.9 main.py`

# Application React
L'application est une application React basé sur des composants appartenant à la librairie Material UI (MUI). 

## Usage
Pour utiliser l’application, vous devez créer un projet React et utiliser la ligne de commande `PORT=5003 npm start` en remplaçant 5003 par le port sur lequel vous souhaitez lancer votre application. 
 
## Architecture 
AJOUTER SCHEMA
 
## src
Le dossier **src** est le seul dossier que vous avez à modifier pour personnaliser l’application à votre usage.
 
### api
Le dossier **api** contient le fichier Python de l'API (ici créée avec FastAPI). 

### images
Le dossier **images** contient les images au format .png intégrées dans l'application. Ici nous avons une seule image qui est le logo de l'entreprise.

### components
Le dossier **components** contient plusieurs dossiers dans lesquels se trouvent les fichiers des différentes pages et composants de l'application.

#### Pages
Le dossier **Pages** contient les fichiers vers les différentes pages de l’application. Les routes vers ces pages sont définies dans le fichier `index.js`. Dans la majeure partie des cas, ces fichiers appellent d'autres composants contenus dans les dossiers suivants. Dans certains cas, l'appel à d'autres composants peut dépendre de l'url. 
- **Document.js** : appel aux composants permettant la visualisation des métadonnées en fonction de l'url (niveau base, table, variable)
- **Form.js** : appel aux composants permettant d'afficher un formulaire de saisie des métadonnées en fonction de l'url (nouveau formulaire, formulaire de modification des métadonnées, reprise d'un formulaire sauvegardé)
- **Access.js** : appel aux composants de la page de modification des accès utilisateurs, accessible uniquement pour les administrateurs
- **Historique.js** : appel aux composants de la page de visualisation des historiques (insertions et modifications)
- **Import.js** : appel aux composants de la page d'importation des métadonnées via un fichier JSON (structuré selon certaines règles)
- **ListDocumentsUser.js** : appel aux composants de la page de visualisation des métadonnées modifiables par l'utilisateur (selon ces droits d'accès) et des formulaires sauvegardés par l'utilisateur
- **Login.js** : appel aux composants de la page de connexion
- **Logout.js** : appel aux composants de la page de déconnexion
- **User.js** : appel aux composants de la page du profil de l'utilisateur
- **ModifPerso.js** : appel aux composants de la page de modification des informations personnelles de l'utilisateur
- **ModifUser.js** : appel aux composants de la page de modification des droits d'un utilisateur (administrateur ou simple utilisateur), accessible uniquement pour les administrateurs
- **NewUser.js** : appel aux composants de la page d'ajout d'un nouvel utilisateur, accessible uniquement pour les administrateurs
- **SuppUser.js** : appel aux composants de la page de suppression d'un utilisateur, accessible uniquement pour les administrateurs

#### Search
Le dossier search contient deux fichiers affichant les pages de moteurs de rechercher parmi les bases et les tables d'une base. 
- **Search.js** : barre de recherche permettant de saisir un ou plusieurs mot(s) et d'afficher dans un tableau la liste des bases pour lesquelles au moins une occurrence a été trouvée dans les métadonnées de la base, de ses tabes et/ou de toutes les variables de la base, possibilité de sélectionner seulement certaines bases pour lancer la recherche, la recherche se fait avec un appel à l'API MeiliSearch qui retourne les bases ayant au moins une occurrence
- **SearchStep2.js** : barre de recherche permettant de saisir un ou plusieurs mot(s) et d'afficher dans un tableau la liste des tables d'une base pour lesquelles au moins une occurrence a été trouvée dans les métadonnées des tabes et/ou de toutes les variables de la table, possibilité de sélectionner seulement certaines tables pour lancer la recherche, la recherche se fait avec un appel à l'API Python qui retourne les tables ayant au moins une occurrence

#### Login
Le dossier **Login** contient les fichiers permettant la gestion de connexion et déconnexion des utilisateurs.
- **Login.js** : formulaire de connexion permettant à l'utilisateur de s'identifier (et donc d'avoir accès à son compte personnel et à la modification de certaines bases) à l'aide d'un nom d'utilisateur et d'un mot de passe
- **Logout.js** : function permettant la déconnexion de l'utilisateur

#### Document
Le dossier **Document** contient trois fichiers qui représente les pages contenant la visualisation sous forme de tableaux des métadonnées des trois niveaux base, table et variable. 
- **DocumentStep1.js** : visualisation des métadonnées d'une base
- **DocumentStep2.js** : visualisation des métadonnées de la table d'une base
- **DocumentStep3.js** : visualisation des métadonnées d'une variable appartenant à la table d'une base

#### Form
Le dossier **Form** contient l'ensemble des formulaires d'insertion et/ou de modification de métadonnées et leurs composants. 
Voici les fichiers contenant la structure des différents formulaires disponibles : 
- **NewDocument.js** : structure du formulaire permettant d'insérer de nouvelles métadonnées
- **EditDocument.js** : structure du formulaire permettant de modifier les métadonnés d'une base (modifications des trois niveaux possibles)
- **EditDocumentInProgress.js** : structure du formulaire permettant de modifier et/ou terminer la saisie des métadonnés d'une base qui a été sauvegardé mais non inscrit dans le catalogue de métadonnées (modifications des trois niveaux possibles)
- **Import.js** : composants permettant à l'utilisateur d'importer un fichier JSON (structuré selon certaines règles) afin d'importer les métadonnées sans passer par le remplissage du formulaire

Voici les différents composants utilisés par les fichiers précédents de formulaire :
- **InputsSteps1.js** : partie du formulaire contenant les champs à remplir pour la partie base
- **InputsSteps2.js** : partie du formulaire contenant les champs à remplir pour la partie table
- **InputsSteps3.js** : partie du formulaire contenant les champs à remplir pour la partie variable
- **Resum.js** : partie du formulaire qui résume les informations de la base et des tables déjà remplies
- **ResumStep2.js** : partie du formulaire qui résume les informations de la table en cours et des variables de cette table qui ont déjà été remplies

### Historique
Le dossier **Historique** contient un fichier permettant de créer deux tableaux différents : un tableau avec l'historique des insertions et un second tableau avec l'historique des modifications effectuées sur les métadonnées. 

#### Success
Le dossier **Success** contient un fichier permettant d'afficher un message en fonction de l'url en fonction de divers évènements : données sauvegardées, données ajoutées, données modifiées,  utilisateur ajouté, utilisateur supprimé, informations personnelles modifiées, mot de passe modifié...

#### User
Le dossier **User** contient les composants relatifs à la gestion des utilisateurs.
- **Access.js** : formulaire qui permet d'attribuer les accès d'une base à un utilisateur, accessible uniquement pour les administrateurs
- **ListDocumentsUser.js** : visualisation des bases accessibles par l'utilisateur et des formulaires sauvegardés par l'utilisateur
- **ModifUser.js** : formulaire qui permet de modifier les droits d'un utilisateur (administrateur ou simple utilisateur), accessible uniquement pour les administrateurs
- **ModifPerso.js** : formulaire qui permet de modifier les informations personnelles de l'utilisateur
- **ModifPassword.js** : formulaire qui permet de modifier le mot de passe de l'utilisateur (avec vérification de l'ancien mot de passe)
- **NewUser.js** : formulaire qui permet d'ajouter un nouvel utilisateur, accessible uniquement pour les administrateurs
- **Supp.js** : formulaire qui permet de supprimer un utilisateur, accessible uniquement pour les administrateurs
- **User.js** : visualisation du profil utilisateur avec les informations personnelles et liens vers les modifications de celles-ci, liens vers les pages d'ajout, suppression et modification des accès utilisateurs uniquement pour les administrateurs

#### Listes
Le dossier **Listes** contient des fichiers qui permettent de récupérer des listes utilisées dans le mapping de certains composants de pages. 
- **Access.js** : liste des accès aux bases des utilisateurs (identifiant de la base, nom d'utilisateur)
- **AllDocuments.js** : liste de toutes les bases enregistrées (identifiant de la base, nom de la base, description)
- **AllDocumentsStep.js** : liste de toutes les tables d'une base (identifiant de la table, nom de la table)
- **DocumentsInProgressUser.js** : liste des formulaires sauvegardés par l'utilisateur (identifiant de la base, nom de la base, description)
- **DocumentsSearch.js** : liste des bases retournées par le moteur de recherche suite à une recherche (identifiant de la base, nom de la base, description)
- **DocumentsSearchStep2.js** : liste des tables retournées par l'api suite à une recherche (identifiant de la table, nom de la table)
- **DocumentsStep1.js** : liste des métadonnées d'une base
- **DocumentsStep2.js** : liste des métadonnées d'une table
- **DocumentsStep3.js** : liste des métadonnées d'une variable
- **DocumentsUser.js** : liste des bases modifiables par l'utilisateur (identifiant de la base, nom de la base, description)
- **EditDocument.js** : récupération des métadonnées d'une base pour les afficher dans un formulaire permettant de modifier les métadonnées de cette base en tant que valeur par défaut
- **EditDocumentInProgress.js** : récupération des métadonnées d'un formulaire sauvegardé pour les afficher dans un formulaire permettant de continuer l'ajout des métadonnées en tant que valeur par défaut
- **EssentielsStep1.js** : liste des champs essentiels du niveau base
- **EssentielsStep2.js** : liste des champs essentiels du niveau table
- **EssentielsStep3.js** : liste des champs essentiels du niveau variable
- **Historique.js** : liste des insertions et des modifications effectuées sur les métadonnées 
- **Inputs.js** : liste des champs des formulaires par niveaux structurés (base, table, variable), des champs essentiels à chaque niveaux et des catégories dans lesquels sont rangés ces champs
- **User.js** : liste de tous les utilisateurs

#### Token
Le dossier **Token.js** ne contient qu'un seul fichier qui contient une fonction permettant de récupérer vérifier la connexion d'un utilisateur et de récupérer son nom d'utilisateur qui est stocké dans le stockage local du navigateur lors de sa connexion à l'application

#### Styles
Le dossier **Styles.js** contient les fichiers dans lesquels se trouvent les styles des composants utilisés dans l'application. C'est dans ces fichiers que vous pouvez personnaliser visuellement les composants de l'application. 
- **Styles.js** : personnalisation des composants (textes, tableaux, liens, boutons...) 
- **Top.js** : personnalisation de l'en-tête de l'application et de la barre de navigation

#### Functions
Le dossier **Functions** contient un ensemble de fonctions utiles dans plusieurs fichiers.
- **convert_object_to_string.js** : fonction qui permet de convertir une variable de type Oject en type String
- **verificationEmail.js** : fonction qui permet de vérifier, à l'aide d'une expression régulière, qu'une variable est bien structurée sous la forme d'une adresse mail
- **verificationUrl.js** : fonction qui permet de vérifier, à l'aide d'une expression régulière, qu'une variable est bien structurée sous la forme d'une url

#### App
Le fichier **App.js** permet de créer la première page de l’application, ici il s'agit de la page permettant de faire une recherche parmi les bases du catalogue dont la structure se trouve dans le fichier **Search.js**. Le fichier **App.css** permet de personnaliser le design de l’application (elle est vide dans cet exemple car nous utilisons le ficheir **Styles.js** pour personnaliser nos composants).
 
#### index
Le fichier **index.js** contient les routes vers les différentes pages de l’application. 
