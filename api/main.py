from email.mime import base
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import logging
from fastapi.logger import logger
import requests
from requests.structures import CaseInsensitiveDict
import re
import mysql.connector
from mysql.connector import errorcode
from SPARQLWrapper import SPARQLWrapper, JSON
import json
import meilisearch
import hashlib
from mailjet_rest import Client
import os

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logger = logging.getLogger("api")
logger.setLevel(logging.DEBUG)
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler("api.log")
c_handler.setLevel(logging.DEBUG)
f_handler.setLevel(logging.DEBUG)
c_format = logging.Formatter("[%(levelname)s] - %(message)s")
f_format = logging.Formatter("[%(levelname)s] - %(message)s")
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)
logger.addHandler(c_handler)
logger.addHandler(f_handler)


@app.get("/")
def read_root():
    return {"Root": "Hello"}


# Cherche une base en particulier
@app.get("/read_bases/{base_id}")
def read_base(base_id: str):
    url = (
        "http://workmetadata.francecentral.cloudapp.azure.com:5002/indexes/Metadata/documents/"
        + base_id
    )

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"

    resp = requests.get(url)
    resp = resp.json()

    return resp


# Contenu d'une base
@app.get("/bases/{base_id}")
def get_base(base_id: str):
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_document(base_id)

    data = get["dcat:Catalog#" + base_id]
    data_struc = {}

    for d in data:
        if d == "rdf:ID":
            data_struc["base_ID"] = {
                "RDF": d,
                "Metadata": "Identifiant de la base",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:title":
            data_struc["base_titre"] = {
                "RDF": d,
                "Metadata": "Titre",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "skos:altLabel":
            data_struc["base_acronyme"] = {
                "RDF": d,
                "Metadata": "Acronyme",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "adms:identifier":
            data_struc["base_identifiant_DOI"] = {
                "RDF": d,
                "Metadata": "Identifiant DOI",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:identifier":
            data_struc["base_reference_HDH"] = {
                "RDF": d,
                "Metadata": "Référence catalogue HDH",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:MedicalDomain":
            data_struc["base_domaine_medical"] = {
                "RDF": d,
                "Metadata": "Domaine médical",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dcat:keyword":
            data_struc["base_keyword"] = {
                "RDF": d,
                "Metadata": "Mots-clés",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:description":
            data_struc["base_description"] = {
                "RDF": d,
                "Metadata": "Description",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Financing":
            data_struc["base_financement"] = {
                "RDF": d,
                "Metadata": "Financement",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:publisher":
            for pub in get["dcat:Catalog#" + base_id][d]["foaf:Organization"]:
                if pub == "foaf:name":
                    data_struc["base_publisher_organisation"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Organisation getonsable",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

                elif pub == "locn:Adress":
                    data_struc["base_publisher_adresse"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Adresse",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

                elif pub == "hdhrdf:OrganizationStatus":
                    data_struc["base_publisher_status"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Status de l'organisation",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

                elif pub == "dct:identifier":
                    data_struc["base_publisher_ROR"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Identifiant numérique ROR",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

                elif pub == "hdhrdf:ScientificCommittee":
                    data_struc["base_publisher_comite"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Existence d'un comité scientifique",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

                elif pub == "foaf:homepage":
                    data_struc["base_publisher_site"] = {
                        "RDF": d + " / " + pub,
                        "Metadata": "Site internet",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            pub
                        ],
                        "type": "string",
                    }

        elif d == "dct:creator":
            for crea in get["dcat:Catalog#" + base_id][d]["foaf:Organization"]:
                if crea == "foaf:name":
                    data_struc["base_creator_institut"] = {
                        "RDF": d + " / " + crea,
                        "Metadata": "Institut",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            crea
                        ],
                        "type": "string",
                    }

                elif crea == "xds:anyURI":
                    data_struc["base_creator_site"] = {
                        "RDF": d + " / " + crea,
                        "Metadata": "Site internet",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            crea
                        ],
                        "type": "url",
                    }

                elif crea == "locn:Adress":
                    data_struc["base_creator_adresse"] = {
                        "RDF": d + " / " + crea,
                        "Metadata": "Adresse",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            crea
                        ],
                        "type": "string",
                    }

                elif crea == "hdhrdf:Scientist":
                    data_struc["base_creator_getonsable"] = {
                        "RDF": d + " / " + crea,
                        "Metadata": "getonsable scientifique",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            crea
                        ],
                        "type": "string",
                    }

                elif crea == "dct:identifier":
                    data_struc["base_creator_identifiant"] = {
                        "RDF": d + " / " + crea,
                        "Metadata": "Identifiant numérique",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            crea
                        ],
                        "type": "string",
                    }

        elif d == "hdhrdf:Contributors":
            for cont in get["dcat:Catalog#" + base_id][d]["foaf:Organization"]:
                if cont == "foaf:name":
                    data_struc["base_contributors_institut"] = {
                        "RDF": d + " / " + cont,
                        "Metadata": "Institut",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            cont
                        ],
                        "type": "string",
                    }

                elif cont == "xds:anyURI":
                    data_struc["base_contributors_site"] = {
                        "RDF": d + " / " + cont,
                        "Metadata": "Site internet",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            cont
                        ],
                        "type": "url",
                    }

                elif cont == "locn:Adress":
                    data_struc["base_contributors_adresse"] = {
                        "RDF": d + " / " + cont,
                        "Metadata": "Adresse",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            cont
                        ],
                        "type": "string",
                    }

                elif cont == "hdhrdf:Scientist":
                    data_struc["base_contributors_getonsable"] = {
                        "RDF": d + " / " + cont,
                        "Metadata": "getonsable scientifique",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            cont
                        ],
                        "type": "string",
                    }

                elif cont == "dct:identifier":
                    data_struc["base_contributors_identifiant"] = {
                        "RDF": d + " / " + cont,
                        "Metadata": "Identifiant numérique",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            cont
                        ],
                        "type": "string",
                    }

        elif d == "dcat:contactPoint":
            for contact in get["dcat:Catalog#" + base_id][d]["foaf:Person"]:
                if contact == "foaf:name":
                    data_struc["base_contact_nom"] = {
                        "RDF": d + " / " + contact,
                        "Metadata": "Nom",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Person"][
                            contact
                        ],
                        "type": "string",
                    }

                elif contact == "locn:Adress":
                    data_struc["base_contact_adresse"] = {
                        "RDF": d + " / " + contact,
                        "Metadata": "Adresse",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Person"][
                            contact
                        ],
                        "type": "string",
                    }

                elif contact == "foaf:mbox":
                    data_struc["base_contact_mail"] = {
                        "RDF": d + " / " + contact,
                        "Metadata": "Adresse mail",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Person"][
                            contact
                        ],
                        "type": "email",
                    }

        elif d == "hdhrdf:Droits":
            for droits in get["dcat:Catalog#" + base_id][d]["foaf:Organization"]:
                if droits == "xds:anyURI":
                    data_struc["base_droits_site"] = {
                        "RDF": d + " / " + droits,
                        "Metadata": "Site internet",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            droits
                        ],
                        "type": "url",
                    }

                elif droits == "locn:Adress":
                    data_struc["base_droits_adresse"] = {
                        "RDF": d + " / " + droits,
                        "Metadata": "Adresse",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            droits
                        ],
                        "type": "string",
                    }

                elif droits == "foaf:mbox":
                    data_struc["base_droits_mail"] = {
                        "RDF": d + " / " + droits,
                        "Metadata": "Adresse mail",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Organization"][
                            droits
                        ],
                        "type": "email",
                    }

        elif d == "hdhrdf:ScientificValorization":
            data_struc["base_valorisation_scientifique"] = {
                "RDF": d,
                "Metadata": "Valorisation scientifique",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:issued":
            data_struc["base_annee_publication"] = {
                "RDF": d,
                "Metadata": "Année de publication",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "date",
            }

        elif d == "dct:language":
            data_struc["base_langue"] = {
                "RDF": d,
                "Metadata": "Langue",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "langage",
            }

        elif d == "hdhrdf:Editor":
            for edit in get["dcat:Catalog#" + base_id][d]["foaf:Person"]:
                if edit == "foaf:name":
                    data_struc["base_editor_nom"] = {
                        "RDF": d + " / " + edit,
                        "Metadata": "Nom",
                        "value": get["dcat:Catalog#" + base_id][d]["foaf:Person"][edit],
                        "type": "string",
                    }

        elif d == "hdhrdf:ResourceType":
            data_struc["base_type_ressource"] = {
                "RDF": d,
                "Metadata": "Type de ressource",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:DataCategory":
            data_struc["base_categorie_donnees"] = {
                "RDF": d,
                "Metadata": "Catégorie de données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:BaseType":
            data_struc["base_type_base"] = {
                "RDF": d,
                "Metadata": "Type de base",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:DataType":
            data_struc["base_type_donnees"] = {
                "RDF": d,
                "Metadata": "Type de données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Objectives":
            data_struc["base_objectifs"] = {
                "RDF": d,
                "Metadata": "Objectifs de la base de données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:source":
            data_struc["base_source"] = {
                "RDF": d,
                "Metadata": "Source de données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:spatial":
            data_struc["base_perimetre_geographique"] = {
                "RDF": d,
                "Metadata": "Périmètre géographique",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:GeographicalPrecision":
            data_struc["base_precision_geographique"] = {
                "RDF": d,
                "Metadata": "Niveau de précision géographique",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:PopulationOfInterest":
            data_struc["base_population_interet"] = {
                "RDF": d,
                "Metadata": "Population d'intérêt",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:NumberOfParticipants":
            data_struc["base_nombre_participants"] = {
                "RDF": d,
                "Metadata": "Nombre de participants",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:graph":
            for graph in get["dcat:Catalog#" + base_id][d]["hdhrdf:Graph"]:
                if graph == "xds:anyURI":
                    data_struc["base_distribution_population_lien"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Lien",
                        "value": get["dcat:Catalog#" + base_id][d]["hdhrdf:Graph"][
                            graph
                        ],
                        "type": "url",
                    }

                elif graph == "hdhrdf:Mean":
                    data_struc["base_distribution_population_moyenne"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Moyenne",
                        "value": get["dcat:Catalog#" + base_id][d]["hdhrdf:Graph"][
                            graph
                        ],
                        "type": "integer",
                    }

                elif graph == "hdhrdf:Dispersion":
                    data_struc["base_distribution_population_dispersion"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Dispersion",
                        "value": get["dcat:Catalog#" + base_id][d]["hdhrdf:Graph"][
                            graph
                        ],
                        "type": "integer",
                    }

        elif d == "dct:format":
            data_struc["base_format_base"] = {
                "RDF": d,
                "Metadata": "Format de la base",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:DataStandard":
            data_struc["base_standard_donnees"] = {
                "RDF": d,
                "Metadata": "Standard des données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dcat:byteSize":
            data_struc["base_taille_base"] = {
                "RDF": d,
                "Metadata": "Taille de la base",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:NumberOfTables":
            data_struc["base_nombre_tables"] = {
                "RDF": d,
                "Metadata": "Nombre de tables",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "integer",
            }

        elif d == "hdhrdf:NumberOfVariables":
            data_struc["base_nombre_variables"] = {
                "RDF": d,
                "Metadata": "Nombre de variables",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "integer",
            }

        elif d == "dct:temporal#start_recueil":
            data_struc["base_start_recueil"] = {
                "RDF": d + " / " + "schema:startDate",
                "Metadata": "Date du premier recueil",
                "value": get["dcat:Catalog#" + base_id][d]["dct:PeriodOfTime"][
                    "schema:startDate"
                ],
                "type": "date",
            }

        elif d == "hdhrdf:CollectionStatus":
            data_struc["base_status_collecte"] = {
                "RDF": d,
                "Metadata": "Status de la collecte des données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:CompletenessRate":
            data_struc["base_taux_exhaustivite"] = {
                "RDF": d,
                "Metadata": "Taux d'exhaustivité",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dct:temporal#end_recueil":
            data_struc["base_end_recueil"] = {
                "RDF": d + " / " + "schema:endDate",
                "Metadata": "Date du dernier recueil",
                "value": get["dcat:Catalog#" + base_id][d]["dct:PeriodOfTime"][
                    "schema:endDate"
                ],
                "type": "date",
            }

        elif d == "hdhrdf:Historical":
            data_struc["base_profondeur_historique"] = {
                "RDF": d,
                "Metadata": "Profondeur historique",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dcat:modified":
            data_struc["base_derniere_modification"] = {
                "RDF": d,
                "Metadata": "Date de dernière modification",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "date",
            }

        elif d == "dct:accrualPeriodicity":
            data_struc["base_frequence_modification"] = {
                "RDF": d,
                "Metadata": "Fréquence de mise à jour",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:MethodOfCollection":
            data_struc["base_mode_recueil"] = {
                "RDF": d,
                "Metadata": "Mode de recueil des données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:MethodOfApproval":
            data_struc["base_modalite_approbation"] = {
                "RDF": d,
                "Metadata": "Modalité d'approbation du patient pour le receuil de ses données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:CollectionProcess":
            data_struc["base_processus_collecte"] = {
                "RDF": d,
                "Metadata": "Processus de collecte",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:CollectionProtocol":
            data_struc["base_protocole_collecte"] = {
                "RDF": d,
                "Metadata": "Protocole de collecte",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:QualityProcedure":
            data_struc["base_procedure_qualite"] = {
                "RDF": d,
                "Metadata": "Procédure qualité utilisée",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:CollectionTools":
            data_struc["base_outils_collecte"] = {
                "RDF": d,
                "Metadata": "Outils utilisés pour la collecte",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:ParticipantsFollow":
            data_struc["base_suivi_participants"] = {
                "RDF": d,
                "Metadata": "Suivi des participants",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Follow-upModalities":
            data_struc["base_modalites_suivi"] = {
                "RDF": d,
                "Metadata": "Modalités de suivi des participants",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Follow-upDetails":
            data_struc["base_details_suivi"] = {
                "RDF": d,
                "Metadata": "Détails du suivi",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Paring":
            data_struc["base_appariement"] = {
                "RDF": d,
                "Metadata": "Appariement au SNDS",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:ParingType":
            data_struc["base_type_appariement"] = {
                "RDF": d,
                "Metadata": "Type d'appariement",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:ParingRate":
            data_struc["base_taux_appariement"] = {
                "RDF": d,
                "Metadata": "Taux d'appariement",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:AccessConditions":
            data_struc["base_conditions_access_HDH"] = {
                "RDF": d,
                "Metadata": "Conditions d'accès au HDH",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:ExternalAccessConditions":
            data_struc["base_conditions_access_externe"] = {
                "RDF": d,
                "Metadata": "Conditions d'accès externe au HDH",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Sample":
            data_struc["base_echantillon"] = {
                "RDF": d,
                "Metadata": "Echantillon synthétique",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Variables":
            data_struc["base_variables"] = {
                "RDF": d,
                "Metadata": "Dictionnaire de variables",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:OperatingPrograms":
            data_struc["base_programmes_exploitation"] = {
                "RDF": d,
                "Metadata": "Programmes facilitant l'exploitation des données",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:ValuationDocument":
            data_struc["base_document_valorisation"] = {
                "RDF": d,
                "Metadata": "Document de valorisation",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:Expertise":
            data_struc["base_expertise"] = {
                "RDF": d,
                "Metadata": "Formations et expertises",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "hdhrdf:UsedInProjects":
            data_struc["base_projets"] = {
                "RDF": d,
                "Metadata": "Liste des projets utilisant la base de donnée",
                "value": get["dcat:Catalog#" + base_id][d],
                "type": "string",
            }

        elif d == "dcat:dataset":
            dataset = []

            for k in range(len(get["dcat:Catalog#" + base_id][d].keys())):
                dataset.append(
                    {
                        "id": get["dcat:Catalog#" + base_id][d][
                            list(get["dcat:Catalog#" + base_id][d].keys())[k]
                        ]["rdf:ID"],
                        "data": get["dcat:Catalog#" + base_id][d][
                            list(get["dcat:Catalog#" + base_id][d].keys())[k]
                        ],
                    }
                )

            data_struc["base_tables"] = {
                "categorie": "Tables de la base",
                "RDF": d,
                "Metadata": "Liste des tables de la base de donnée",
                "value": dataset,
                "type": "string",
            }

    return data_struc


# Contenu d'une base (formulaire en cours de saisis)
@app.get("/bases_inprogress/{base_id}")
def get_base_in_progress(base_id: str):
    url = (
        "http://workmetadata.francecentral.cloudapp.azure.com:5002/indexes/Metadata_in_progress/documents/"
        + base_id
    )

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"

    resp = requests.get(url)
    resp = resp.json()

    return resp


# Contenu d'une table
@app.post("/tables")
def get_table(data: dict):
    base_id = data["base"]
    table_id = data["table"]

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_document(base_id)

    data = get["dcat:Catalog#" + base_id]["dcat:dataset"]["dcat:Dataset#" + table_id]

    data_struc = {}
    variables = []
    variable_struc = {}

    for d in data:
        if d == "rdf:ID":
            data_struc["table_ID"] = {
                "RDF": d,
                "Metadata": "Identifiant de la table",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dct:title":
            data_struc["table_titre"] = {
                "RDF": d,
                "Metadata": "Titre",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dct:description":
            data_struc["table_description"] = {
                "RDF": d,
                "Metadata": "Description",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dct:identifier":
            data_struc["table_reference"] = {
                "RDF": d,
                "Metadata": "Référence",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dct:format":
            data_struc["table_format"] = {
                "RDF": d,
                "Metadata": "Format",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dcat:byteSize":
            data_struc["table_taille"] = {
                "RDF": d,
                "Metadata": "Taille",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "hdhrdf:Variables":
            data_struc["table_variables"] = {
                "RDF": d,
                "Metadata": "Liste des variables",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "dct:temporal#mise_en_prod":
            data_struc["table_mise_en_production"] = {
                "RDF": d + " / " + "schema:startDate",
                "Metadata": "Date de mise en production",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d]["dct:PeriodOfTime"]["schema:startDate"],
                "type": "date",
            }

        elif d == "dct:temporal#arret":
            data_struc["table_arret"] = {
                "RDF": d + " / " + "schema:endDate",
                "Metadata": "Date d'arrêt",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d]["dct:PeriodOfTime"]["schema:endDate"],
                "type": "date",
            }

        elif d == "hdhrdf:MissingDates":
            data_struc["table_dates_manquantes"] = {
                "RDF": d,
                "Metadata": "Dates manquantes",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "hdhrdf:graph#Evolution_table_par_années":
            for graph in get["dcat:Catalog#" + base_id]["dcat:dataset"][
                "dcat:Dataset#" + table_id
            ][d]["hdhrdf:Graph"]:
                if graph == "xds:anyURI":
                    data_struc["table_graph_table_lien"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Lien graphique évolution table par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "url",
                    }

                elif graph == "hdhrdf:Mean":
                    data_struc["table_graph_table_moyenne"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Moyenne graphique évolution table par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "integer",
                    }

                elif graph == "hdhrdf:Dispersion":
                    data_struc["table_graph_table_dispersion"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Dispersion graphique évolution table par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "integer",
                    }

        elif d == "hdhrdf:graph#Evolution_variable_par_années":
            for graph in get["dcat:Catalog#" + base_id]["dcat:dataset"][
                "dcat:Dataset#" + table_id
            ][d]["hdhrdf:Graph"]:
                if graph == "xds:anyURI":
                    data_struc["table_graph_variables_lien"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Lien graphique évolution variables par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "url",
                    }

                elif graph == "hdhrdf:Mean":
                    data_struc["table_graph_variables_moyenne"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Moyenne graphique variables table par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "integer",
                    }

                elif graph == "hdhrdf:Dispersion":
                    data_struc["table_graph_variables_dispersion"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Dispersion graphique évolution variables par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Graph"][graph],
                        "type": "integer",
                    }

        elif d == "hdhrdf:PrimaryKey":
            data_struc["table_cle_primaire"] = {
                "RDF": d,
                "Metadata": "Clé primaire",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        elif d == "hdhrdf:ForeignKeys":
            data_struc["table_cles_secondaires"] = {
                "RDF": d,
                "Metadata": "Clés secondaires",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ][d],
                "type": "string",
            }

        else:

            if "hdhrdf:variable" in d:
                variables.append(
                    {
                        "id": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Variable"]["rdf:ID"],
                        "data": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ][d]["hdhrdf:Variable"],
                    }
                )

            variable_struc["table_variables_list"] = {
                "categorie": "Variables de la table",
                "RDF": "hdhrdf:variable#",
                "Metadata": "Liste des variables de la table",
                "value": variables,
                "type": "string",
            }

    data_struc.update(variable_struc)
    return data_struc


# Contenu d'une variable
@app.post("/variables")
def get_variable(data: dict):
    base_id = data["base"]
    table_id = data["table"]
    variable_id = data["variable"]

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_document(base_id)

    data = get["dcat:Catalog#" + base_id]["dcat:dataset"]["dcat:Dataset#" + table_id][
        "hdhrdf:variable#" + variable_id
    ]["hdhrdf:Variable"]
    data_struc = {}

    for d in data:
        if d == "rdf:ID":
            data_struc["variable_ID"] = {
                "RDF": d,
                "Metadata": "Identifiant de la variable",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dct:identifier":
            data_struc["variable_identifiant"] = {
                "RDF": d,
                "Metadata": "Indentifiant",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dct:title":
            data_struc["variable_titre"] = {
                "RDF": d,
                "Metadata": "Titre",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dct:description":
            data_struc["variable_description"] = {
                "RDF": d,
                "Metadata": "Description",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Type":
            data_struc["variable_type"] = {
                "RDF": d,
                "Metadata": "Type",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Format":
            data_struc["variable_format"] = {
                "RDF": d,
                "Metadata": "Format",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Unit":
            data_struc["variable_unite"] = {
                "RDF": d,
                "Metadata": "Unité",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:ObjectType":
            data_struc["variable_type_objet"] = {
                "RDF": d,
                "Metadata": "Type d'objet",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Terminology":
            data_struc["variable_terminologie"] = {
                "RDF": d,
                "Metadata": "Terminologie de références",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:graph#Evolution_terminologie_par_annees":
            for graph in get["dcat:Catalog#" + base_id]["dcat:dataset"][
                "dcat:Dataset#" + table_id
            ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d]["hdhrdf:Graph"]:
                if graph == "xds:anyURI":
                    data_struc["variable_graph_terminologie_lien"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Lien graphique évolution terminologie par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "url",
                    }

                elif graph == "hdhrdf:Mean":
                    data_struc["variable_graph_terminologie_moyenne"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Moyenne graphique évolution terminologie par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "integer",
                    }

                elif graph == "hdhrdf:Dispersion":
                    data_struc["variable_graph_terminologie_dispersion"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Dispersion graphique évolution terminologie par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "integer",
                    }

        elif d == "hdhrdf:Classification":
            data_struc["variable_classification"] = {
                "RDF": d,
                "Metadata": "Classification de la variable",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dct:issued":
            data_struc["variable_mise_en_production"] = {
                "RDF": d,
                "Metadata": "Date de mise en production",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "date",
            }

        elif d == "dcat:modified":
            data_struc["variable_fin"] = {
                "RDF": d,
                "Metadata": "Date d'arrêt",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "date",
            }

        elif d == "hdhrdf:CompletenessDate":
            data_struc["variable_exhaustivite"] = {
                "RDF": d,
                "Metadata": "Date d'exhaustivité",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "date",
            }

        elif d == "hdhrdf:MissingDates":
            data_struc["variable_dates_manquantes"] = {
                "RDF": d,
                "Metadata": "Dates manquantes",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:PresenceInOtherDataset":
            data_struc["variable_tables_presence"] = {
                "RDF": d,
                "Metadata": "Liste des tables où la variable est présente",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:NumberOfUniqueValues":
            data_struc["variable_nombre_valeurs_uniques"] = {
                "RDF": d,
                "Metadata": "Nombre de valeurs uniques",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:MaxValue":
            data_struc["variable_valeur_max"] = {
                "RDF": d,
                "Metadata": "Valeur maximale",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:MinValue":
            data_struc["variable_valeur_min"] = {
                "RDF": d,
                "Metadata": "Valeur minimale",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:Mean":
            data_struc["variable_moyenne"] = {
                "RDF": d,
                "Metadata": "Moyenne",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:Median":
            data_struc["variable_mediane"] = {
                "RDF": d,
                "Metadata": "Médiane",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:Variance":
            data_struc["variable_variance"] = {
                "RDF": d,
                "Metadata": "Variance",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:StandardDeviation":
            data_struc["variable_ecart_type"] = {
                "RDF": d,
                "Metadata": "Ecart-type",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "integer",
            }

        elif d == "hdhrdf:PourcentageMissingValues":
            data_struc["variable_pourcentage_valeurs_manquantes"] = {
                "RDF": d,
                "Metadata": "Pourcentage de valeurs manquantes",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:MissingValuesCode":
            data_struc["variable_code_valeurs_manquantes"] = {
                "RDF": d,
                "Metadata": "Code pour les valeurs manquantes",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:PourcentageNotAvailableValues":
            data_struc["variable_pourcentage_valeurs_manquantes"] = {
                "RDF": d,
                "Metadata": "Pourcentage de valeurs non disponibles",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:NotAvailableValuesCode":
            data_struc["variable_code_valeurs_non_dispo"] = {
                "RDF": d,
                "Metadata": "Code pour les valeurs non disponibles",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:graph#Evolution_completude_par_annees":
            for graph in get["dcat:Catalog#" + base_id]["dcat:dataset"][
                "dcat:Dataset#" + table_id
            ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d]["hdhrdf:Graph"]:
                if graph == "xds:anyURI":
                    data_struc["variable_graph_completude_lien"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Lien graphique complétude variables par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "url",
                    }

                elif graph == "hdhrdf:Mean":
                    data_struc["variable_graph_completude_moyenne"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Moyenne graphique complétude table par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "integer",
                    }

                elif graph == "hdhrdf:Dispersion":
                    data_struc["variable_graph_completude_dispersion"] = {
                        "RDF": d + " / " + graph,
                        "Metadata": "Dispersion graphique complétude variables par années",
                        "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                            "dcat:Dataset#" + table_id
                        ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d][
                            "hdhrdf:Graph"
                        ][
                            graph
                        ],
                        "type": "integer",
                    }

        elif d == "hdhrdf:ComplianceWithTerminologie":
            data_struc["variable_conformite_terminologies"] = {
                "RDF": d,
                "Metadata": "Conformité avec les terminologies de références",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:ComplianceWithObjectType":
            data_struc["variable_conformite_type"] = {
                "RDF": d,
                "Metadata": "Conformité avec le type d'objet",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:ComplianceWithUnit":
            data_struc["variable_conformite_unite"] = {
                "RDF": d,
                "Metadata": "Conformité avec l'unité",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:ComplianceWithFormat":
            data_struc["variable_conformite_format"] = {
                "RDF": d,
                "Metadata": "Conformité avec le format",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:MeasurementMethodology":
            data_struc["variable_methodologie_mesure"] = {
                "RDF": d,
                "Metadata": "Méthodologie de mesure et description",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Plausibility":
            data_struc["variable_pausibilite"] = {
                "RDF": d,
                "Metadata": "Pausibilité",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Terminals":
            data_struc["variable_bornes"] = {
                "RDF": d,
                "Metadata": "Bornes",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:MediumResolution":
            data_struc["variable_resolution_moyenne"] = {
                "RDF": d,
                "Metadata": "Résolution moyenne",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:PointsDensity":
            data_struc["variable_densite"] = {
                "RDF": d,
                "Metadata": "Densité des points",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dcat:byteSize":
            data_struc["variable_taille"] = {
                "RDF": d,
                "Metadata": "Taille de fichier",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "hdhrdf:Sample":
            data_struc["variable_echantillon"] = {
                "RDF": d,
                "Metadata": "Echantillon synthétique",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

        elif d == "dcat:mediaType":
            data_struc["variable_format_images"] = {
                "RDF": d,
                "Metadata": "Format des images",
                "value": get["dcat:Catalog#" + base_id]["dcat:dataset"][
                    "dcat:Dataset#" + table_id
                ]["hdhrdf:variable#" + variable_id]["hdhrdf:Variable"][d],
                "type": "string",
            }

    return data_struc


# Listes des champs essentiels base
@app.get("/essentiels_bases")
def essentiels_base():
    return ["base_ID", "base_titre", "base_keyword", "base_description"]


# Listes des champs essentiels table
@app.get("/essentiels_tables")
def essentiels_base():
    return ["table_ID", "table_titre", "table_description"]


# Listes des champs essentiels variable
@app.get("/essentiels_variables")
def essentiels_base():
    return ["variable_ID", "variable_titre", "variable_description"]


# Listes des bases
@app.get("/list_bases")
def list_bases():
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_documents({"limit": 1000})

    return get


# Connexion de l'utilisateur
@app.post("/login")
def login(credentials: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `login`"
    cursor.execute(query)

    log = {}

    # Récupération des bases dont l'utilisateur est amdin
    for (id, password) in cursor:
        if id == credentials["username"]:
            if password == hashlib.sha256(credentials["password"].encode()).hexdigest():
                log["token"] = credentials["username"]

    cursor.close()
    cnx.close()

    return log


# Vérification du mot de passe
@app.post("/verif_password")
def verif_password(state: dict):

    if (
        state["lastPassword"]
        == hashlib.sha256(state["lastPasswordSaisi"].encode()).hexdigest()
    ):
        return True

    else:
        return False


# Insertion d'un nouvel utilisateur
@app.post("/new_user")
def new_user(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    insertion = (
        "INSERT INTO users "
        "VALUES ('"
        + state["username"]
        + "', '"
        + state["mail"]
        + "', '"
        + str(state["status"])
        + "')"
    )
    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(insertion)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()

    api_key = "ae9c84204f775f1e9f0ff3d2433692d0"
    api_secret = "93adf3856c91ebed131b3124e51e9e80"
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "cassandra.fournot@health-data-hub.fr",
                    "Name": "Catalogue de métadonnées - Health Data Hub",
                },
                "To": [
                    {
                        "Email": state["mail"],
                        "Name": state["username"],
                    }
                ],
                "Subject": "Bienvenue sur le catalogue de métadonnées du Health Data Hub",
                "TextPart": "Inscription",
                "HTMLPart": "<img src='https://www.health-data-hub.fr/themes/custom/health_data_hub/logo.png' alt='LogoHDH'/> <h3>Bonjour "
                + state["username"]
                + ", </h3><br /> Nous vous souhaitons la bienvenue sur le catalogue de métadonnées du Health Data Hub ! <br /> Pour activer votre compte, cliquer sur le lien suivant : <a href='http://workmetadata.francecentral.cloudapp.azure.com:5003/first_connexion?"
                + state["mail"]
                + "'>'http://workmetadata.francecentral.cloudapp.azure.com:5003/first_connexion?"
                + state["mail"]
                + "'</a> <br /> <br /> Bonne fin de journée de la part de toute l'équipe du Health Data Hub.",
                "CustomID": "Inscription",
            }
        ]
    }
    result = mailjet.send.create(data=data)
    logger.info(result.status_code)
    logger.info(result.json())

# Mot de passe oublié - Envoi d'un mail pour changer le mot de passe
@app.post("/lost_password")
def lost_password(state: dict ): 
    api_key = "ae9c84204f775f1e9f0ff3d2433692d0"
    api_secret = "93adf3856c91ebed131b3124e51e9e80"
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "cassandra.fournot@health-data-hub.fr",
                    "Name": "Catalogue de métadonnées - Health Data Hub",
                },
                "To": [
                    {
                        "Email": state["mail"],
                        "Name": state["username"],
                    }
                ],
                "Subject": "Mot de passe oublié",
                "TextPart": "Catalogue de métadonnées - Health Data Hub - Mot de passe oublié",
                "HTMLPart": "<img src='https://www.health-data-hub.fr/themes/custom/health_data_hub/logo.png' alt='LogoHDH'/> <h3>Bonjour "
                + state["username"]
                + ", </h3><br /> Vous nous avez signalé avoir oublié votre mot de passe. <br /> Pas d'inquiétude, pour changer votre mot de passe, cliquer sur le lien suivant : <a href='http://workmetadata.francecentral.cloudapp.azure.com:5003/lost_password?"
                + state["mail"]
                + "'>'http://workmetadata.francecentral.cloudapp.azure.com:5003/lost_password?"
                + state["mail"]
                + "'</a> <br /> <br /> Bonne fin de journée de la part de toute l'équipe du Health Data Hub.",
                "CustomID": "Mot de passe oublié",
            }
        ]
    }
    result = mailjet.send.create(data=data)
    logger.info(result.status_code)
    logger.info(result.json())

# Insertion d'un nouvel utilisateur
@app.post("/new_login")
def new_login(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    insertion_login = (
        "INSERT INTO login "
        "VALUES ('" + state["username"] + "', SHA2('" + state["password"] + "', 256))"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(insertion_login)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()

    api_key = "ae9c84204f775f1e9f0ff3d2433692d0"
    api_secret = "93adf3856c91ebed131b3124e51e9e80"
    mailjet = Client(auth=(api_key, api_secret), version="v3.1")
    data = {
        "Messages": [
            {
                "From": {
                    "Email": "cassandra.fournot@health-data-hub.fr",
                    "Name": "Catalogue de métadonnées - Health Data Hub",
                },
                "To": [
                    {
                        "Email": state["username"],
                        "Name": state["username"],
                    }
                ],
                "Subject": "Création d'un compte sur le catalogue de métadonnées du Health Data Hub",
                "TextPart": "Création compte",
                "HTMLPart": "<img src='https://www.health-data-hub.fr/themes/custom/health_data_hub/logo.png' alt='LogoHDH'/> <h3>Bonjour "
                + state["username"]
                + ", </h3><br /> Nous vous souhaitons la bienvenue sur le catalogue de métadonnées du Health Data Hub ! <br /> <br />  Vous pouvez désormais vous connecter au <a href='http://workmetadata.francecentral.cloudapp.azure.com:5003/'>catalogue de métadonnées</a>. <br /> Votre login est votre adresse mail : "
                + state["username"]
                + ".<br /> Votre mot de passe est celui que vous avez choisi lorsque vous avez activé votre compte. <br /> <br /> Bonne fin de journée de la part de toute l'équipe du Health Data Hub.",
                "CustomID": "Création compte",
            }
        ]
    }
    result = mailjet.send.create(data=data)
    logger.info(result.status_code)
    logger.info(result.json())


# Modification des droits utilisateurs
@app.post("/modif_user")
def modif_user(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    modification = (
        "UPDATE users SET status="
        + str(state["status"])
        + " WHERE username='"
        + state["username"]
        + "'"
    )

    try:
        print("Insert {}: ".format("modification"), end="")
        cursor.execute(modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Modification des informations personnelles d'un utilisateur
@app.post("/modif")
def modif(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    modification = (
        "UPDATE users SET mail='"
        + state["mail"]
        + "' WHERE username='"
        + state["username"]
        + "'"
    )

    try:
        print("Insert {}: ".format("modification"), end="")
        cursor.execute(modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Modification des informations personnelles d'un utilisateur
@app.post("/modif_password")
def modif_password(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    modification = (
        "UPDATE login SET password=SHA2('"
        + state["password"]
        + "', 256) WHERE id='"
        + state["username"]
        + "'"
    )

    try:
        print("Insert {}: ".format("modification"), end="")
        cursor.execute(modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Suppression d'un utilisateur
@app.post("/supp_user")
def supp_user(state: dict):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    suppression_users = (
        "DELETE FROM users WHERE `username` = '" + state["username"] + "'"
    )
    suppression_login = "DELETE FROM login WHERE `id` = '" + state["username"] + "'"
    suppression_access = "DELETE FROM bases WHERE `admin` = '" + state["username"] + "'"
    suppression_insertion = (
        "UPDATE insertion SET username='Utilisateur supprimé' WHERE username = '"
        + state["username"]
        + "'"
    )
    suppression_modification = (
        "UPDATE modification SET user='Utilisateur supprimé' WHERE user = '"
        + state["username"]
        + "'"
    )
    suppression_modification = (
        "UPDATE suppression SET username='Utilisateur supprimé' WHERE user = '"
        + state["username"]
        + "'"
    )

    try:
        print("Insert {}: ".format("suppression"), end="")
        cursor.execute(suppression_users)
        cursor.execute(suppression_login)
        cursor.execute(suppression_access)
        cursor.execute(suppression_insertion)
        cursor.execute(suppression_modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Liste des utilisateurs
@app.get("/list_users")
def list_users():
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `users`"
    cursor.execute(query)

    users = []

    # Récupération des bases dont l'utilisateur est amdin
    for (username, mail, status) in cursor:
        users.append({"username": username, "mail": mail, "status": status})

    cursor.close()
    cnx.close()

    return users


# Mot de passe d'un utilisateur
@app.post("/password/{username}")
def password(username: str):
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `login` WHERE id='" + username + "'"
    cursor.execute(query)

    users = []

    # Récupération des bases dont l'utilisateur est amdin
    for (username, password) in cursor:
        users.append({"username": username, "password": password})

    cursor.close()
    cnx.close()

    return users


# Liste des access des utilisateurs
@app.get("/list_access")
def list_access():
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `bases`"
    cursor.execute(query)

    users = []

    # Récupération des bases dont l'utilisateur est amdin
    for (bases, username) in cursor:
        users.append({"document_id": bases, "username": username})

    cursor.close()
    cnx.close()

    return users


# Ajout des access
@app.post("/ajout_access")
def add_access(modification: dict):

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    add_modification = (
        "INSERT INTO bases "
        "VALUES ('"
        + modification["document_id"]
        + "', '"
        + modification["username"]
        + "')"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(add_modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Suppression des access
@app.post("/supp_access")
def supp_access(modification: dict):

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    add_modification = (
        "DELETE FROM bases WHERE `admin`='"
        + modification["username"]
        + "' AND `bases`='"
        + modification["document_id"]
        + "'"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(add_modification)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Liste des bases accessibles par un utilisateur
@app.get("/list_bases/user/{user_id}")
def list_bases_user(user_id: str):

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_documents({"limit": 1000})

    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `bases` "
    cursor.execute(query)

    # Récupération des bases dont l'utilisateur est amdin
    admin_bases = []
    for (bases, admin) in cursor:
        if admin == user_id:
            admin_bases.append(bases)

    cursor.close()
    cnx.close()

    id_bases = []
    for base in get:
        if base["id"] in admin_bases:
            id_bases.append(base)

    return id_bases


# Liste des bases en cours accessibles par un utilisateur
@app.get("/list_bases_inprogress/user/{user_id}")
def list_bases_inprogress_user(user_id: str):
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata_in_progress")

    get = index.get_documents({"limit": 1000})

    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `bases_in_progress` "
    query = 'SELECT * FROM `bases_in_progress` WHERE username="' + user_id + '"'
    cursor.execute(query)

    # Récupération des bases dont l'utilisateur est amdin
    admin_bases = []
    for (bases, admin) in cursor:
        if admin == user_id:
            admin_bases.append(bases)

    cursor.close()
    cnx.close()

    id_bases = []
    for base in get:
        if base["id"].split("_")[0] in admin_bases:
            id_bases.append(base)

    return id_bases


# Liste des tables d'une base
@app.get("/list_tables/{base_id}")
def list_tables(base_id: str):
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    get = index.get_document(base_id)

    tables = []

    keys = list(get.keys())
    dataset = get[keys[1]]["dcat:dataset"]
    for d in dataset:
        tables.append(
            {
                "id": get[keys[1]]["dcat:dataset"][d]["rdf:ID"],
                d: get[keys[1]]["dcat:dataset"][d],
            }
        )

    return tables


# Recherche d'un input dans les tables d'une base
@app.post("/search_tables")
def read_list_tables(data: dict):
    url = "http://workmetadata.francecentral.cloudapp.azure.com:5002/indexes/Metadata/documents"

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"

    resp = requests.get(url, headers=headers)
    resp = resp.json()

    tables = []

    for base in resp:
        if base["id"] == data["base_id"]:
            keys = list(base.keys())
            dataset = base[keys[1]]["dcat:dataset"]
            for d in dataset:
                if data["input"] in str(base[keys[1]]["dcat:dataset"][d]):
                    tables.append(
                        {
                            "id": base[keys[1]]["dcat:dataset"][d]["rdf:ID"],
                            d: base[keys[1]]["dcat:dataset"][d],
                        }
                    )

    return tables


# Insertion d'une nouvelle base
@app.post("/new_base")
def new_base(data: dict):
    # Récupération des clés / valeurs des données reçues
    keys = data.keys()
    values = data.values()

    datasets_name = []
    datasets_title = []
    variables_name = []
    variables_nb = []

    datasets = []

    # Parcours des clés
    for k in keys:

        # Informations de la base
        if k == "base_ID":
            catalogue_name = "dcat:Catalog#" + data[k]
            catalogue_name_jena = data[k]

        # Informations des tables
        elif "table_ID" in k:
            datasets_name.append("dcat:Dataset#" + data[k])
            datasets_title.append(data[k])

        # Informations des variables
        elif "variable_ID" in k:
            variables_name.append("hdhrdf:variable#" + data[k])
            variables_nb.append(k)

    # Parcours des tables
    for t in range(len(datasets_name)):
        variables = []
        variables_t_names = []
        # Parcours des variables de la table
        for v in variables_nb:
            nb = re.split("#", v)
            # Si la variable appartient à la table en cours
            if int(nb[1]) == t:
                variable_name = "hdhrdf:variable#" + verif(
                    data, "variable_ID#" + str(t) + "#" + nb[2]
                )
                variables_t_names.append(variable_name)

                graph_terminologie_var = (
                    '"hdhrdf:graph#Evolution_terminologie_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_terminologie_par_annees", "xds:anyURI": "'
                    + verif(
                        data, "variable_graph_terminologie_lien#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Mean": "'
                    + verif(
                        data,
                        "variable_graph_terminologie_moyenne#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:Dispersion": "'
                    + verif(
                        data,
                        "variable_graph_terminologie_dispersion#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '"} }'
                )
                graph_completude_var = (
                    '"hdhrdf:graph#Evolution_completude_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_completude_par_annees", "xds:anyURI": "'
                    + verif(
                        data, "variable_graph_completude_lien#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Mean": "'
                    + verif(
                        data,
                        "variable_graph_completude_moyenne#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:Dispersion": "'
                    + verif(
                        data,
                        "variable_graph_completude_dispersion#" + str(t) + "#" + nb[2],
                    )
                    + '"} }'
                )

                variable = (
                    '{"rdf:ID": "'
                    + verif(data, "variable_ID#" + str(t) + "#" + nb[2])
                    + '", "dct:title": "'
                    + verif(data, "variable_titre#" + str(t) + "#" + nb[2])
                    + '", "dct:identifier": "'
                    + verif(data, "variable_identifiant#" + str(t) + "#" + nb[2])
                    + '", "dct:description": "'
                    + verif(data, "variable_description#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Type": "'
                    + verif(data, "variable_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Format": "'
                    + verif(data, "variable_format#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Unit": "'
                    + verif(data, "variable_unite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ObjectType": "'
                    + verif(data, "variable_type_objet#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Terminology": "'
                    + verif(data, "variable_terminologie#" + str(t) + "#" + nb[2])
                    + '", '
                    + graph_terminologie_var
                    + ', "hdhrdf:Classification": "'
                    + verif(data, "variable_classification#" + str(t) + "#" + nb[2])
                    + '", "dct:issued": "'
                    + verif(data, "variable_mise_en_production#" + str(t) + "#" + nb[2])
                    + '", "dcat:modified": "'
                    + verif(data, "variable_fin#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:CompletenessDate": "'
                    + verif(data, "variable_exhaustivite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MissingDates": "'
                    + verif(data, "variable_dates_manquantes#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PresenceInOtherDataset": "'
                    + verif(data, "variable_tables_presence#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:NumberOfUniqueValues": "'
                    + verif(
                        data, "variable_nombre_valeurs_uniques#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:MaxValue": "'
                    + verif(data, "variable_valeur_max#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MinValue": "'
                    + verif(data, "variable_valeur_min#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Mean": "'
                    + verif(data, "variable_moyenne#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Median": "'
                    + verif(data, "variable_mediane#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Variance": "'
                    + verif(data, "variable_variance#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:StandardDeviation":"'
                    + verif(data, "variable_ecart_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PourcentageMissingValues": "'
                    + verif(
                        data,
                        "variable_pourcentage_valeurs_manquantes#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '", "hdhrdf:MissingValuesCode": "'
                    + verif(
                        data, "variable_code_valeurs_manquantes#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:PourcentageNotAvailableValues": "'
                    + verif(
                        data,
                        "variable_pourcentage_valeurs_non_dispo#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '", "hdhrdf:NotAvailableValuesCode": "'
                    + verif(
                        data, "variable_code_valeurs_non_dispo#" + str(t) + "#" + nb[2]
                    )
                    + '", '
                    + graph_completude_var
                    + ', "hdhrdf:ComplianceWithTerminologie": "'
                    + verif(
                        data,
                        "variable_conformite_terminologies#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:ComplianceWithObjectType": "'
                    + verif(data, "variable_conformite_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ComplianceWithUnit": "'
                    + verif(data, "variable_conformite_unite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ComplianceWithFormat": "'
                    + verif(data, "variable_conformite_format#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MeasurementMethodology": "'
                    + verif(
                        data, "variable_methodologie_mesure#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Plausibility": "'
                    + verif(data, "variable_pausibilite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Terminals": "'
                    + verif(data, "variable_bornes#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MediumResolution": "'
                    + verif(data, "variable_resolution_moyenne#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PointsDensity": "'
                    + verif(data, "variable_densite#" + str(t) + "#" + nb[2])
                    + '", "dcat:byteSize": "'
                    + verif(data, "variable_taille#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Sample": "'
                    + verif(data, "variable_echantillon#" + str(t) + "#" + nb[2])
                    + '", "dcat:mediaType": "'
                    + verif(data, "variable_format_images#" + str(t) + "#" + nb[2])
                    + '"}'
                )
                variables.append(variable)

        mise_en_prod_dat = (
            '"dct:temporal#mise_en_prod":{  "dct:PeriodOfTime":{ "rdf:ID": "mise_en_prod", "schema:startDate": "'
            + verif(data, "table_mise_en_production#" + str(t))
            + '"} }'
        )
        arret_dat = (
            '"dct:temporal#arret":{  "dct:PeriodOfTime":{ "rdf:ID": "arret", "schema:endDate": "'
            + verif(data, "table_arret#" + str(t))
            + '"} }'
        )
        graph_table_dat = (
            '"hdhrdf:graph#Evolution_table_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_table_par_annees", "xds:anyURI": "'
            + verif(data, "table_graph_table_lien#" + str(t))
            + '", "hdhrdf:Mean": "'
            + verif(data, "table_graph_table_moyenne#" + str(t))
            + '", "hdhrdf:Dispersion": "'
            + verif(data, "table_graph_table_dispersion#" + str(t))
            + '"} }'
        )
        graph_variables_dat = (
            '"hdhrdf:graph#Evolution_variable_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_variable_par_annees", "xds:anyURI": "'
            + verif(data, "table_graph_variables_lien#" + str(t))
            + '", "hdhrdf:Mean": "'
            + verif(data, "table_graph_variables_moyenne#" + str(t))
            + '", "hdhrdf:Dispersion": "'
            + verif(data, "table_graph_variables_dispersion#" + str(t))
            + '"} }'
        )

        dataset = (
            '{"rdf:ID": "'
            + verif(data, "table_ID#" + str(t))
            + '", "dct:title": "'
            + verif(data, "table_titre#" + str(t))
            + '", "dct:description": "'
            + verif(data, "table_description#" + str(t))
            + '", "dct:identifier": "'
            + verif(data, "table_reference#" + str(t))
            + '", "dct:format": "'
            + verif(data, "table_format#" + str(t))
            + '", "dcat:byteSize": "'
            + verif(data, "table_taille#" + str(t))
            + '", "hdhrdf:Variables": "'
            + verif(data, "table_variables#" + str(t))
            + '", '
            + mise_en_prod_dat
            + ", "
            + arret_dat
            + ', "hdhrdf:MissingDates": "'
            + verif(data, "table_dates_manquantes#" + str(t))
            + '", '
            + graph_table_dat
            + ", "
            + graph_variables_dat
            + ', "hdhrdf:PrimaryKey": "'
            + verif(data, "table_cle_primaire#" + str(t))
            + '", "hdhrdf:ForeignKeys": "'
            + verif(data, "table_cles_secondaires#" + str(t))
            + '", "'
        )

        for v in range(len(variables_t_names)):
            dataset += (
                variables_t_names[v] + '": {"hdhrdf:Variable": ' + variables[v] + '}, "'
            )

        dataset = dataset[:-3] + "}"
        datasets.append(dataset)

    publisher_cat = (
        '"dct:publisher":{   "foaf:Organization":{   "rdf:ID": "org-publisher", "foaf:name": "'
        + verif(data, "base_publisher_organisation")
        + '", "locn:Adress": "'
        + verif(data, "base_publisher_adresse")
        + '", "hdhrdf:OrganizationStatus": "'
        + verif(data, "base_publisher_status")
        + '", "dct:identifier": "'
        + verif(data, "base_publisher_ROR")
        + '", "hdhrdf:ScientificCommittee": "'
        + verif(data, "base_publisher_comite")
        + '", "foaf:homepage": "'
        + verif(data, "base_publisher_site")
        + '"} }'
    )
    creator_cat = (
        '"dct:creator":{   "foaf:Organization":{   "rdf:ID": "org-creator", "foaf:name": "'
        + verif(data, "base_creator_institut")
        + '", "xds:anyURI": "'
        + verif(data, "base_creator_site")
        + '", "locn:Adress": "'
        + verif(data, "base_creator_adresse")
        + '", "hdhrdf:Scientist": "'
        + verif(data, "base_creator_responsable")
        + '", "dct:identifier": "'
        + verif(data, "base_creator_identifiant")
        + '"} }'
    )
    contributors_cat = (
        '"hdhrdf:Contributors":{   "foaf:Organization":{   "rdf:ID": "org-contributors", "foaf:name": "'
        + verif(data, "base_contributors_institut")
        + '", "xds:anyURI": "'
        + verif(data, "base_contributors_site")
        + '", "locn:Adress": "'
        + verif(data, "base_contributors_adresse")
        + '", "hdhrdf:Scientist": "'
        + verif(data, "base_contributors_responsable")
        + '", "dct:identifier": "'
        + verif(data, "base_contributors_identifiant")
        + '"} }'
    )
    contact_cat = (
        '"dcat:contactPoint":{   "foaf:Person":{   "rdf:ID": "contactPoint", "foaf:name": "'
        + verif(data, "base_contact_nom")
        + '", "locn:Adress": "'
        + verif(data, "base_contact_adresse")
        + '", "foaf:mbox": "'
        + verif(data, "base_contact_mail")
        + '"} }'
    )
    droits_cat = (
        '"hdhrdf:Droits":{   "foaf:Organization":{   "rdf:ID": "org-droits", "xds:anyURI": "'
        + verif(data, "base_droits_site")
        + '", "locn:Adress": "'
        + verif(data, "base_droits_adresse")
        + '", "foaf:mbox": "'
        + verif(data, "base_droits_mail")
        + '"} }'
    )
    editor_cat = (
        '"hdhrdf:Editor":{   "foaf:Person":{   "rdf:ID": "editor", "foaf:name": "'
        + verif(data, "base_editor_nom")
        + '"} }'
    )
    graph_distribution_population_cat = (
        '"hdhrdf:graph":{   "hdhrdf:Graph":{   "rdf:ID": "Distribution_population", "xds:anyURI": "'
        + verif(data, "base_distribution_population_lien")
        + '", "hdhrdf:Mean": "'
        + verif(data, "base_distribution_population_moyenne")
        + '", "hdhrdf:Dispersion": "'
        + verif(data, "base_distribution_population_dispersion")
        + '"} }'
    )
    start_recueil_cat = (
        '"dct:temporal#start_recueil":{   "dct:PeriodOfTime":{   "rdf:ID": "start_recueil", "schema:startDate": "'
        + verif(data, "base_start_recueil")
        + '"} }'
    )
    end_recueil_cat = (
        '"dct:temporal#end_recueil":{   "dct:PeriodOfTime":{   "rdf:ID": "end_recueil", "schema:endDate": "'
        + verif(data, "base_end_recueil")
        + '"} }'
    )

    catalogue = (
        '{"rdf:ID": "'
        + data["base_ID"]
        + '", "dct:title": "'
        + verif(data, "base_titre")
        + '", "skos:altLabel": "'
        + verif(data, "base_acronyme")
        + '", "adms:identifier": "'
        + verif(data, "base_identifiant_DOI")
        + '", "dct:identifier": "'
        + verif(data, "base_reference_HDH")
        + '", "hdhrdf:MedicalDomain": "'
        + verif(data, "base_domaine_medical")
        + '", "dcat:keyword": "'
        + verif(data, "base_keyword")
        + '", "dct:description": "'
        + verif(data, "base_description")
        + '", "hdhrdf:Financing": "'
        + verif(data, "base_financement")
        + '", '
        + publisher_cat
        + ", "
        + creator_cat
        + ", "
        + contributors_cat
        + ", "
        + contact_cat
        + ", "
        + droits_cat
        + ', "hdhrdf:ScientificValorization": "'
        + verif(data, "base_valorisation_scientifique")
        + '", "dct:issued": "'
        + verif(data, "base_annee_publication")
        + '", "dct:language": "'
        + verif(data, "base_langue")
        + '", '
        + editor_cat
        + ', "hdhrdf:ResourceType": "'
        + verif(data, "base_type_ressource")
        + '", "hdhrdf:DataCategory": "'
        + verif(data, "base_categorie_donnees")
        + '", "hdhrdf:BaseType": "'
        + verif(data, "base_type_base")
        + '", "hdhrdf:DataType": "'
        + verif(data, "base_type_donnees")
        + '", "hdhrdf:Objectives": "'
        + verif(data, "base_objectifs")
        + '", "dct:source": "'
        + verif(data, "base_source")
        + '", "dct:spatial": "'
        + verif(data, "base_perimetre_geographique")
        + '", "hdhrdf:GeographicalPrecision": "'
        + verif(data, "base_precision_geographique")
        + '", "hdhrdf:PopulationOfInterest": "'
        + verif(data, "base_population_interet")
        + '", "hdhrdf:NumberOfParticipants": "'
        + verif(data, "base_nombre_participants")
        + '", '
        + graph_distribution_population_cat
        + ', "dct:format": "'
        + verif(data, "base_format_base")
        + '", "hdhrdf:DataStandard": "'
        + verif(data, "base_standard_donnees")
        + '", "dcat:byteSize": "'
        + verif(data, "base_taille_base")
        + '", "hdhrdf:NumberOfTables": "'
        + verif(data, "base_nombre_tables")
        + '", "hdhrdf:NumberOfVariables": "'
        + verif(data, "base_nombre_variables")
        + '", '
        + start_recueil_cat
        + ', "hdhrdf:CollectionStatus": "'
        + verif(data, "base_status_collecte")
        + '", "hdhrdf:CompletenessRate": "'
        + verif(data, "base_taux_exhaustivite")
        + '", '
        + end_recueil_cat
        + ', "hdhrdf:Historical": "'
        + verif(data, "base_profondeur_historique")
        + '", "dcat:modified": "'
        + verif(data, "base_derniere_modification")
        + '", "dct:accrualPeriodicity": "'
        + verif(data, "base_frequence_modification")
        + '", "hdhrdf:MethodOfCollection": "'
        + verif(data, "base_mode_recueil")
        + '", "hdhrdf:MethodOfApproval": "'
        + verif(data, "base_modalite_approbation")
        + '", "hdhrdf:CollectionProcess": "'
        + verif(data, "base_processus_collecte")
        + '", "hdhrdf:CollectionProtocol": "'
        + verif(data, "base_protocole_collecte")
        + '", "hdhrdf:QualityProcedure": "'
        + verif(data, "base_procedure_qualite")
        + '", "hdhrdf:CollectionTools": "'
        + verif(data, "base_outils_collecte")
        + '", "hdhrdf:ParticipantsFollow-up": "'
        + verif(data, "base_suivi_participants")
        + '", "hdhrdf:Follow-upModalities": "'
        + verif(data, "base_modalites_suivi")
        + '", "hdhrdf:Follow-upDetails": "'
        + verif(data, "base_details_suivi")
        + '", "hdhrdf:Paring": "'
        + verif(data, "base_appariement")
        + '", "hdhrdf:ParingType": "'
        + verif(data, "base_type_appariement")
        + '", "hdhrdf:ParingRate": "'
        + verif(data, "base_taux_appariement")
        + '", "hdhrdf:AccessConditions": "'
        + verif(data, "base_conditions_access_HDH")
        + '", "hdhrdf:ExternalAccessConditions": "'
        + verif(data, "base_conditions_access_externe")
        + '", "hdhrdf:Sample": "'
        + verif(data, "base_echantillon")
        + '", "hdhrdf:Variables": "'
        + verif(data, "base_variables")
        + '", "hdhrdf:OperatingPrograms": "'
        + verif(data, "base_programmes_exploitation")
        + '", "hdhrdf:ValuationDocument": "'
        + verif(data, "base_document_valorisation")
        + '", "hdhrdf:Expertise": "'
        + verif(data, "base_expertise")
        + '", "hdhrdf:UsedInProjects": "'
        + verif(data, "base_projets")
        + '"'
    )

    if len(datasets) != 0:
        catalogue += ', "dcat:dataset": {"'

        for d in range(len(datasets)):
            catalogue += datasets_name[d] + '": ' + datasets[d] + ',"'

        catalogue = catalogue[:-2] + "}"

    catalogue += "}"

    data = (
        '{ "id": "'
        + data["base_ID"]
        + '", "'
        + catalogue_name
        + '": '
        + catalogue
        + "}"
    )

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")
    add = index.add_documents(json.loads(data))

    catalogue = ""
    data = json.loads(data)

    publisher = ""
    creator = ""
    Contributors = ""
    contactPoint = ""
    Droits = ""
    Editor = ""
    Distribution_population = ""
    start_recueil = ""
    end_recueil = ""

    for key, value in data.items():
        if "dcat:Catalog#" in key:
            catalogue_name_jena = key.replace("dcat:Catalog#", "")
            catalogue += (
                "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "> a dcat:Catalog; "
            )
            datasets = ""

            for key_cat, val_cat in value.items():
                if "dct:publisher" in key_cat:
                    for k, v in val_cat["foaf:Organization"].items():
                        publisher += str(k) + ' "' + str(v) + '"; '
                elif "dct:creator" in key_cat:
                    for k, v in val_cat["foaf:Organization"].items():
                        creator += str(k) + ' "' + str(v) + '"; '

                elif "hdhrdf:Contributors" in key_cat:
                    for k, v in val_cat["foaf:Organization"].items():
                        Contributors += str(k) + ' "' + str(v) + '"; '

                elif "dcat:contactPoint" in key_cat:
                    for k, v in val_cat["foaf:Person"].items():
                        contactPoint += str(k) + ' "' + str(v) + '"; '

                elif "hdhrdf:Droits" in key_cat:
                    for k, v in val_cat["foaf:Organization"].items():
                        Droits += str(k) + ' "' + str(v) + '"; '

                elif "hdhrdf:Editor" in key_cat:
                    for k, v in val_cat["foaf:Person"].items():
                        Editor += str(k) + ' "' + str(v) + '"; '

                elif "hdhrdf:graph" in key_cat:
                    for k, v in val_cat["hdhrdf:Graph"].items():
                        Distribution_population += str(k) + ' "' + str(v) + '"; '

                elif "dct:temporal#start_recueil" in key_cat:
                    for k, v in val_cat["dct:PeriodOfTime"].items():
                        start_recueil += str(k) + ' "' + str(v) + '"; '

                elif "dct:temporal#end_recueil" in key_cat:
                    for k, v in val_cat["dct:PeriodOfTime"].items():
                        end_recueil += str(k) + ' "' + str(v) + '"; '

                elif "dcat:dataset" in key_cat:
                    variables = ""
                    mise_en_prod = ""
                    arret = ""
                    Evolution_table = ""
                    Evolution_variable = ""

                    for key_dat, value_dat in val_cat.items():
                        dataset_name = key_dat.replace("dcat:Dataset#", "")
                        dataset = (
                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                            + catalogue_name_jena
                            + "/"
                            + dataset_name
                            + "> a dcat:Dataset; "
                        )
                        catalogue = (
                            catalogue[:-2]
                            + "; "
                            + str(key_dat).split("#")[0]
                            + " "
                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                            + catalogue_name_jena
                            + "/"
                            + dataset_name
                            + '>". '
                        )

                        for k_dat, v_dat in value_dat.items():
                            if "dct:temporal#mise_en_prod" in k_dat:
                                for k, v in v_dat["dct:PeriodOfTime"].items():
                                    mise_en_prod += str(k) + ' "' + str(v) + '"; '
                                datasets += (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#mise_en_prod> a dct:PeriodOfTime; "
                                    + mise_en_prod
                                    + ".\n"
                                )
                                dataset += (
                                    str(k_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#mise_en_prod"
                                    + '>"; '
                                )

                            elif "dct:temporal#arret" in k_dat:
                                for k, v in v_dat["dct:PeriodOfTime"].items():
                                    arret += str(k) + ' "' + str(v) + '"; '
                                datasets += (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#arret> a dct:PeriodOfTime; "
                                    + arret
                                    + ".\n"
                                )
                                dataset += (
                                    str(k_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#arret"
                                    + '>"; '
                                )

                            elif "hdhrdf:graph#Evolution_table" in k_dat:
                                for k, v in v_dat["hdhrdf:Graph"].items():
                                    Evolution_table += str(k) + ' "' + str(v) + '"; '
                                datasets += (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#Evolution_table_par_années> a hdhrdf:Graph; "
                                    + Evolution_table
                                    + ".\n"
                                )
                                dataset += (
                                    str(k_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#Evolution_table_par_années"
                                    + '>"; '
                                )

                            elif "hdhrdf:graph#Evolution_variable" in k_dat:
                                for k, v in v_dat["hdhrdf:Graph"].items():
                                    Evolution_variable += str(k) + ' "' + str(v) + '"; '
                                datasets += (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#Evolution_variable_par_années> a hdhrdf:Graph; "
                                    + Evolution_variable
                                    + ".\n"
                                )
                                dataset += (
                                    str(k_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "#Evolution_variable_par_années"
                                    + '>"; '
                                )

                            elif "hdhrdf:variable#" in k_dat:
                                variable_name = k_dat.replace("hdhrdf:variable#", "")
                                variable = (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "/"
                                    + variable_name
                                    + "> a hdhrdf:Variable; "
                                )
                                dataset = (
                                    dataset[:-2]
                                    + "; "
                                    + str(k_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "/"
                                    + variable_name
                                    + '>". '
                                )

                                Evolution_terminologie = ""
                                Evolution_completude = ""

                                for key_var, val_var in v_dat[
                                    "hdhrdf:Variable"
                                ].items():
                                    if "hdhrdf:graph#Evolution_terminologie" in key_var:
                                        for k, v in val_var["hdhrdf:Graph"].items():
                                            Evolution_terminologie += (
                                                str(k) + ' "' + str(v) + '"; '
                                            )
                                        variables += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + "#Evolution_terminologie_par_années> a hdhrdf:Graph; "
                                            + Evolution_terminologie
                                            + ".\n"
                                        )
                                        variable += (
                                            str(key_var).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + "#Evolution_terminologie_par_années"
                                            + '>"; '
                                        )

                                    elif "hdhrdf:graph#Evolution_compl" in key_var:
                                        for k, v in val_var["hdhrdf:Graph"].items():
                                            Evolution_completude += (
                                                str(k) + ' "' + str(v) + '"; '
                                            )
                                        variables += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + "#Evolution_complétude_par_années> a hdhrdf:Graph; "
                                            + Evolution_completude
                                            + ".\n"
                                        )
                                        variable += (
                                            str(key_var).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + "#Evolution_complétude_par_années"
                                            + '>"; '
                                        )

                                    else:
                                        variable += (
                                            str(key_var) + ' "' + str(val_var) + '"; '
                                        )

                                variables += variable[:-2] + ". \n"

                            else:
                                dataset += str(k_dat) + ' "' + str(v_dat) + '"; '

                        datasets += dataset + "\n"

                    datasets += variables

                else:
                    catalogue += str(key_cat) + ' "' + str(val_cat) + '"; '

    prefixes = "prefix dct: <http://purl.org/dc/terms/>\n \
                prefix dcat: <http://www.w3.org/ns/dcat#>\n \
                prefix foaf: <http://xmlns.com/foaf/0.1/>\n \
                prefix skos: <http://www.w3.org/2004/02/skos/core#>\n \
                prefix adms: <http://www.w3.org/ns/adms#>\n \
                prefix xds: <http://www.w3.org/2001/XMLSchema#>\n \
                prefix locn: <https://www.w3.org/ns/locn#>\n \
                prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n \
                prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n \
                prefix schema: <http://schema.org/>\n \
                prefix hdhrdf: <http://workmetadata.francecentral.cloudapp.azure.com:5005/hdh_rdf#>\n"

    query = (
        prefixes
        + "\n \
            INSERT DATA { \n \
                GRAPH <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "/>{\n \
                    "
        + catalogue[:-2]
        + ". \n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#publisher> a foaf:Organization; "
        + publisher[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#creator> a foaf:Organization; "
        + creator[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#Contributors> a foaf:Organization; "
        + Contributors[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#contactPoint> a foaf:Person; "
        + contactPoint[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#Droits> a foaf:Organization; "
        + Droits[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#Editor> a foaf:Person; "
        + Editor[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#Distribution_population> a hdhrdf:Graph; "
        + Distribution_population[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#start_recueil> a dct:PeriodOfTime; "
        + start_recueil[:-2]
        + ".\n \
                    <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
        + catalogue_name_jena
        + "#end_recueil> a dct:PeriodOfTime; "
        + end_recueil[:-2]
        + ".\n \
                    "
        + datasets
        + "\
                }\n \
            }"
    )

    sparql = SPARQLWrapper(
        "http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
    )
    sparql.setQuery(query)
    sparql.setMethod("POST")
    sparql.setReturnFormat(JSON)
    sparql.query()

    return add


# Suppression d'une base
@app.post("/supp")
def supp_base(data: dict):
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    supp = index.delete_document(data["id"])

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    date = str(data["date"])[:-5].replace("T", " ")

    insertion = (
        "INSERT INTO suppression "
        "VALUES ('" + data["username"] + "', '" + data["id"] + "', '" + date + "')"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(insertion)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()

    if supp["status"] == "enqueued":
        return True
    else:
        return False


def verif(data: dict, input: str):
    if input in data.keys():
        return data[input]
    else:
        return ""


# Insertion d'une nouvelle base
@app.post("/save_base")
def save_base(state: dict):
    data = state["state"]
    token = state["token"]

    # Récupération des clés / valeurs des données reçues
    keys = data.keys()
    values = data.values()

    datasets_name = []
    datasets_title = []
    variables_name = []
    variables_nb = []

    datasets = []

    # Parcours des clés
    for k in keys:

        # Informations de la base
        if k == "base_ID":
            catalogue_name = "dcat:Catalog#" + data[k]
            catalogue_name_jena = data[k]

        # Informations des tables
        elif "table_ID" in k:
            datasets_name.append("dcat:Dataset#" + data[k])
            datasets_title.append(data[k])

        # Informations des variables
        elif "variable_ID" in k:
            variables_name.append("hdhrdf:variable#" + data[k])
            variables_nb.append(k)

    # Parcours des tables
    for t in range(len(datasets_name)):
        variables = []
        variables_t_names = []
        # Parcours des variables de la table
        for v in variables_nb:
            nb = re.split("#", v)
            # Si la variable appartient à la table en cours
            if int(nb[1]) == t:
                variable_name = "hdhrdf:variable#" + verif(
                    data, "variable_ID#" + str(t) + "#" + nb[2]
                )
                variables_t_names.append(variable_name)

                graph_terminologie_var = (
                    '"hdhrdf:graph#Evolution_terminologie_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_terminologie_par_annees", "xds:anyURI": "'
                    + verif(
                        data, "variable_graph_terminologie_lien#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Mean": "'
                    + verif(
                        data,
                        "variable_graph_terminologie_moyenne#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:Dispersion": "'
                    + verif(
                        data,
                        "variable_graph_terminologie_dispersion#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '"} }'
                )
                graph_completude_var = (
                    '"hdhrdf:graph#Evolution_completude_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_completude_par_annees", "xds:anyURI": "'
                    + verif(
                        data, "variable_graph_completude_lien#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Mean": "'
                    + verif(
                        data,
                        "variable_graph_completude_moyenne#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:Dispersion": "'
                    + verif(
                        data,
                        "variable_graph_completude_dispersion#" + str(t) + "#" + nb[2],
                    )
                    + '"} }'
                )

                variable = (
                    '{"rdf:ID": "'
                    + verif(data, "variable_ID#" + str(t) + "#" + nb[2])
                    + '", "dct:title": "'
                    + verif(data, "variable_titre#" + str(t) + "#" + nb[2])
                    + '", "dct:identifier": "'
                    + verif(data, "variable_identifiant#" + str(t) + "#" + nb[2])
                    + '", "dct:description": "'
                    + verif(data, "variable_description#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Type": "'
                    + verif(data, "variable_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Format": "'
                    + verif(data, "variable_format#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Unit": "'
                    + verif(data, "variable_unite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ObjectType": "'
                    + verif(data, "variable_type_objet#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Terminology": "'
                    + verif(data, "variable_terminologie#" + str(t) + "#" + nb[2])
                    + '", '
                    + graph_terminologie_var
                    + ', "hdhrdf:Classification": "'
                    + verif(data, "variable_classification#" + str(t) + "#" + nb[2])
                    + '", "dct:issued": "'
                    + verif(data, "variable_mise_en_production#" + str(t) + "#" + nb[2])
                    + '", "dcat:modified": "'
                    + verif(data, "variable_fin#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:CompletenessDate": "'
                    + verif(data, "variable_exhaustivite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MissingDates": "'
                    + verif(data, "variable_dates_manquantes#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PresenceInOtherDataset": "'
                    + verif(data, "variable_tables_presence#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:NumberOfUniqueValues": "'
                    + verif(
                        data, "variable_nombre_valeurs_uniques#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:MaxValue": "'
                    + verif(data, "variable_valeur_max#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MinValue": "'
                    + verif(data, "variable_valeur_min#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Mean": "'
                    + verif(data, "variable_moyenne#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Median": "'
                    + verif(data, "variable_mediane#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Variance": "'
                    + verif(data, "variable_variance#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:StandardDeviation":"'
                    + verif(data, "variable_ecart_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PourcentageMissingValues": "'
                    + verif(
                        data,
                        "variable_pourcentage_valeurs_manquantes#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '", "hdhrdf:MissingValuesCode": "'
                    + verif(
                        data, "variable_code_valeurs_manquantes#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:PourcentageNotAvailableValues": "'
                    + verif(
                        data,
                        "variable_pourcentage_valeurs_non_dispo#"
                        + str(t)
                        + "#"
                        + nb[2],
                    )
                    + '", "hdhrdf:NotAvailableValuesCode": "'
                    + verif(
                        data, "variable_code_valeurs_non_dispo#" + str(t) + "#" + nb[2]
                    )
                    + '", '
                    + graph_completude_var
                    + ', "hdhrdf:ComplianceWithTerminologie": "'
                    + verif(
                        data,
                        "variable_conformite_terminologies#" + str(t) + "#" + nb[2],
                    )
                    + '", "hdhrdf:ComplianceWithObjectType": "'
                    + verif(data, "variable_conformite_type#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ComplianceWithUnit": "'
                    + verif(data, "variable_conformite_unite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:ComplianceWithFormat": "'
                    + verif(data, "variable_conformite_format#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MeasurementMethodology": "'
                    + verif(
                        data, "variable_methodologie_mesure#" + str(t) + "#" + nb[2]
                    )
                    + '", "hdhrdf:Plausibility": "'
                    + verif(data, "variable_pausibilite#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Terminals": "'
                    + verif(data, "variable_bornes#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:MediumResolution": "'
                    + verif(data, "variable_resolution_moyenne#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:PointsDensity": "'
                    + verif(data, "variable_densite#" + str(t) + "#" + nb[2])
                    + '", "dcat:byteSize": "'
                    + verif(data, "variable_taille#" + str(t) + "#" + nb[2])
                    + '", "hdhrdf:Sample": "'
                    + verif(data, "variable_echantillon#" + str(t) + "#" + nb[2])
                    + '", "dcat:mediaType": "'
                    + verif(data, "variable_format_images#" + str(t) + "#" + nb[2])
                    + '"}'
                )
                variables.append(variable)

        mise_en_prod_dat = (
            '"dct:temporal#mise_en_prod":{  "dct:PeriodOfTime":{ "rdf:ID": "mise_en_prod", "schema:startDate": "'
            + verif(data, "table_mise_en_production#" + str(t))
            + '"} }'
        )
        arret_dat = (
            '"dct:temporal#arret":{  "dct:PeriodOfTime":{ "rdf:ID": "arret", "schema:endDate": "'
            + verif(data, "table_arret#" + str(t))
            + '"} }'
        )
        graph_table_dat = (
            '"hdhrdf:graph#Evolution_table_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_table_par_annees", "xds:anyURI": "'
            + verif(data, "table_graph_table_lien#" + str(t))
            + '", "hdhrdf:Mean": "'
            + verif(data, "table_graph_table_moyenne#" + str(t))
            + '", "hdhrdf:Dispersion": "'
            + verif(data, "table_graph_table_dispersion#" + str(t))
            + '"} }'
        )
        graph_variables_dat = (
            '"hdhrdf:graph#Evolution_variable_par_annees":{  "hdhrdf:Graph":{ "rdf:ID": "Evolution_variable_par_annees", "xds:anyURI": "'
            + verif(data, "table_graph_variables_lien#" + str(t))
            + '", "hdhrdf:Mean": "'
            + verif(data, "table_graph_variables_moyenne#" + str(t))
            + '", "hdhrdf:Dispersion": "'
            + verif(data, "table_graph_variables_dispersion#" + str(t))
            + '"} }'
        )

        dataset = (
            '{"rdf:ID": "'
            + verif(data, "table_ID#" + str(t))
            + '", "dct:title": "'
            + verif(data, "table_titre#" + str(t))
            + '", "dct:description": "'
            + verif(data, "table_description#" + str(t))
            + '", "dct:identifier": "'
            + verif(data, "table_reference#" + str(t))
            + '", "dct:format": "'
            + verif(data, "table_format#" + str(t))
            + '", "dcat:byteSize": "'
            + verif(data, "table_taille#" + str(t))
            + '", "hdhrdf:Variables": "'
            + verif(data, "table_variables#" + str(t))
            + '", '
            + mise_en_prod_dat
            + ", "
            + arret_dat
            + ', "hdhrdf:MissingDates": "'
            + verif(data, "table_dates_manquantes#" + str(t))
            + '", '
            + graph_table_dat
            + ", "
            + graph_variables_dat
            + ', "hdhrdf:PrimaryKey": "'
            + verif(data, "table_cle_primaire#" + str(t))
            + '", "hdhrdf:ForeignKeys": "'
            + verif(data, "table_cles_secondaires#" + str(t))
            + '", "'
        )

        for v in range(len(variables_t_names)):
            dataset += (
                variables_t_names[v] + '": {"hdhrdf:Variable": ' + variables[v] + '}, "'
            )

        dataset = dataset[:-3] + "}"
        datasets.append(dataset)

    publisher_cat = (
        '"dct:publisher":{   "foaf:Organization":{   "rdf:ID": "org-publisher", "foaf:name": "'
        + verif(data, "base_publisher_organisation")
        + '", "locn:Adress": "'
        + verif(data, "base_publisher_adresse")
        + '", "hdhrdf:OrganizationStatus": "'
        + verif(data, "base_publisher_status")
        + '", "dct:identifier": "'
        + verif(data, "base_publisher_ROR")
        + '", "hdhrdf:ScientificCommittee": "'
        + verif(data, "base_publisher_comite")
        + '", "foaf:homepage": "'
        + verif(data, "base_publisher_site")
        + '"} }'
    )
    creator_cat = (
        '"dct:creator":{   "foaf:Organization":{   "rdf:ID": "org-creator", "foaf:name": "'
        + verif(data, "base_creator_institut")
        + '", "xds:anyURI": "'
        + verif(data, "base_creator_site")
        + '", "locn:Adress": "'
        + verif(data, "base_creator_adresse")
        + '", "hdhrdf:Scientist": "'
        + verif(data, "base_creator_responsable")
        + '", "dct:identifier": "'
        + verif(data, "base_creator_identifiant")
        + '"} }'
    )
    contributors_cat = (
        '"hdhrdf:Contributors":{   "foaf:Organization":{   "rdf:ID": "org-contributors", "foaf:name": "'
        + verif(data, "base_contributors_institut")
        + '", "xds:anyURI": "'
        + verif(data, "base_contributors_site")
        + '", "locn:Adress": "'
        + verif(data, "base_contributors_adresse")
        + '", "hdhrdf:Scientist": "'
        + verif(data, "base_contributors_responsable")
        + '", "dct:identifier": "'
        + verif(data, "base_contributors_identifiant")
        + '"} }'
    )
    contact_cat = (
        '"dcat:contactPoint":{   "foaf:Person":{   "rdf:ID": "contactPoint", "foaf:name": "'
        + verif(data, "base_contact_nom")
        + '", "locn:Adress": "'
        + verif(data, "base_contact_adresse")
        + '", "foaf:mbox": "'
        + verif(data, "base_contact_mail")
        + '"} }'
    )
    droits_cat = (
        '"hdhrdf:Droits":{   "foaf:Organization":{   "rdf:ID": "org-droits", "xds:anyURI": "'
        + verif(data, "base_droits_site")
        + '", "locn:Adress": "'
        + verif(data, "base_droits_adresse")
        + '", "foaf:mbox": "'
        + verif(data, "base_droits_mail")
        + '"} }'
    )
    editor_cat = (
        '"hdhrdf:Editor":{   "foaf:Person":{   "rdf:ID": "editor", "foaf:name": "'
        + verif(data, "base_editor_nom")
        + '"} }'
    )
    graph_distribution_population_cat = (
        '"hdhrdf:graph":{   "hdhrdf:Graph":{   "rdf:ID": "Distribution_population", "xds:anyURI": "'
        + verif(data, "base_distribution_population_lien")
        + '", "hdhrdf:Mean": "'
        + verif(data, "base_distribution_population_moyenne")
        + '", "hdhrdf:Dispersion": "'
        + verif(data, "base_distribution_population_dispersion")
        + '"} }'
    )
    start_recueil_cat = (
        '"dct:temporal#start_recueil":{   "dct:PeriodOfTime":{   "rdf:ID": "start_recueil", "schema:startDate": "'
        + verif(data, "base_start_recueil")
        + '"} }'
    )
    end_recueil_cat = (
        '"dct:temporal#end_recueil":{   "dct:PeriodOfTime":{   "rdf:ID": "end_recueil", "schema:endDate": "'
        + verif(data, "base_end_recueil")
        + '"} }'
    )

    catalogue = (
        '{"rdf:ID": "'
        + data["base_ID"]
        + '", "dct:title": "'
        + verif(data, "base_titre")
        + '", "skos:altLabel": "'
        + verif(data, "base_acronyme")
        + '", "adms:identifier": "'
        + verif(data, "base_identifiant_DOI")
        + '", "dct:identifier": "'
        + verif(data, "base_reference_HDH")
        + '", "hdhrdf:MedicalDomain": "'
        + verif(data, "base_domaine_medical")
        + '", "dcat:keyword": "'
        + verif(data, "base_keyword")
        + '", "dct:description": "'
        + verif(data, "base_description")
        + '", "hdhrdf:Financing": "'
        + verif(data, "base_financement")
        + '", '
        + publisher_cat
        + ", "
        + creator_cat
        + ", "
        + contributors_cat
        + ", "
        + contact_cat
        + ", "
        + droits_cat
        + ', "hdhrdf:ScientificValorization": "'
        + verif(data, "base_valorisation_scientifique")
        + '", "dct:issued": "'
        + verif(data, "base_annee_publication")
        + '", "dct:language": "'
        + verif(data, "base_langue")
        + '", '
        + editor_cat
        + ', "hdhrdf:ResourceType": "'
        + verif(data, "base_type_ressource")
        + '", "hdhrdf:DataCategory": "'
        + verif(data, "base_categorie_donnees")
        + '", "hdhrdf:BaseType": "'
        + verif(data, "base_type_base")
        + '", "hdhrdf:DataType": "'
        + verif(data, "base_type_donnees")
        + '", "hdhrdf:Objectives": "'
        + verif(data, "base_objectifs")
        + '", "dct:source": "'
        + verif(data, "base_source")
        + '", "dct:spatial": "'
        + verif(data, "base_perimetre_geographique")
        + '", "hdhrdf:GeographicalPrecision": "'
        + verif(data, "base_precision_geographique")
        + '", "hdhrdf:PopulationOfInterest": "'
        + verif(data, "base_population_interet")
        + '", "hdhrdf:NumberOfParticipants": "'
        + verif(data, "base_nombre_participants")
        + '", '
        + graph_distribution_population_cat
        + ', "dct:format": "'
        + verif(data, "base_format_base")
        + '", "hdhrdf:DataStandard": "'
        + verif(data, "base_standard_donnees")
        + '", "dcat:byteSize": "'
        + verif(data, "base_taille_base")
        + '", "hdhrdf:NumberOfTables": "'
        + verif(data, "base_nombre_tables")
        + '", "hdhrdf:NumberOfVariables": "'
        + verif(data, "base_nombre_variables")
        + '", '
        + start_recueil_cat
        + ', "hdhrdf:CollectionStatus": "'
        + verif(data, "base_status_collecte")
        + '", "hdhrdf:CompletenessRate": "'
        + verif(data, "base_taux_exhaustivite")
        + '", '
        + end_recueil_cat
        + ', "hdhrdf:Historical": "'
        + verif(data, "base_profondeur_historique")
        + '", "dcat:modified": "'
        + verif(data, "base_derniere_modification")
        + '", "dct:accrualPeriodicity": "'
        + verif(data, "base_frequence_modification")
        + '", "hdhrdf:MethodOfCollection": "'
        + verif(data, "base_mode_recueil")
        + '", "hdhrdf:MethodOfApproval": "'
        + verif(data, "base_modalite_approbation")
        + '", "hdhrdf:CollectionProcess": "'
        + verif(data, "base_processus_collecte")
        + '", "hdhrdf:CollectionProtocol": "'
        + verif(data, "base_protocole_collecte")
        + '", "hdhrdf:QualityProcedure": "'
        + verif(data, "base_procedure_qualite")
        + '", "hdhrdf:CollectionTools": "'
        + verif(data, "base_outils_collecte")
        + '", "hdhrdf:ParticipantsFollow-up": "'
        + verif(data, "base_suivi_participants")
        + '", "hdhrdf:Follow-upModalities": "'
        + verif(data, "base_modalites_suivi")
        + '", "hdhrdf:Follow-upDetails": "'
        + verif(data, "base_details_suivi")
        + '", "hdhrdf:Paring": "'
        + verif(data, "base_appariement")
        + '", "hdhrdf:ParingType": "'
        + verif(data, "base_type_appariement")
        + '", "hdhrdf:ParingRate": "'
        + verif(data, "base_taux_appariement")
        + '", "hdhrdf:AccessConditions": "'
        + verif(data, "base_conditions_access_HDH")
        + '", "hdhrdf:ExternalAccessConditions": "'
        + verif(data, "base_conditions_access_externe")
        + '", "hdhrdf:Sample": "'
        + verif(data, "base_echantillon")
        + '", "hdhrdf:Variables": "'
        + verif(data, "base_variables")
        + '", "hdhrdf:OperatingPrograms": "'
        + verif(data, "base_programmes_exploitation")
        + '", "hdhrdf:ValuationDocument": "'
        + verif(data, "base_document_valorisation")
        + '", "hdhrdf:Expertise": "'
        + verif(data, "base_expertise")
        + '", "hdhrdf:UsedInProjects": "'
        + verif(data, "base_projets")
        + '"'
    )

    if len(datasets) != 0:
        catalogue += ', "dcat:dataset": {"'

        for d in range(len(datasets)):
            catalogue += datasets_name[d] + '": ' + datasets[d] + ',"'

        catalogue = catalogue[:-2] + "}"

    catalogue += "}"

    data = (
        '{ "id": "'
        + data["base_ID"]
        + "_"
        + token
        + '", "'
        + catalogue_name
        + '": '
        + catalogue
        + "}"
    )

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata_in_progress")
    add = index.add_documents(json.loads(data))

    return add


# Fonction qui permet de vérifier si un dictionnaire contient les clés d'un autre dictionnaire
def verificationKeys(data: dict, structure: dict):
    structure_keys = structure.keys()
    data_keys = data.keys()
    global error
    error = ""

    for k in structure_keys:
        if k in data_keys:
            if type(data[k]) == dict:
                verificationKeys(data[k], structure[k])
            else:
                if structure[k] != "":
                    if data[k] != structure[k]:
                        error = (
                            "Le champs "
                            + str(data[k])
                            + " n'est pas égal à "
                            + structure[k]
                            + "."
                        )
                        break

        elif "#" in k:
            if k not in str(list(data_keys)[list(structure_keys).index(k)]):
                error = (
                    "Le champs "
                    + str(list(data_keys)[list(structure_keys).index(k)])
                    + " devrait être "
                    + k
                    + "."
                )
                break

            else:
                if type(data[list(data_keys)[list(structure_keys).index(k)]]) == dict:
                    verificationKeys(
                        data[list(data_keys)[list(structure_keys).index(k)]],
                        structure[k],
                    )
                else:
                    if structure[k] != "":
                        if (
                            data[list(data_keys)[list(structure_keys).index(k)]]
                            != structure[k]
                        ):
                            error = (
                                "Le champs "
                                + str(
                                    data[list(data_keys)[list(structure_keys).index(k)]]
                                )
                                + " n'est pas égal à "
                                + structure[k]
                                + "."
                            )
                            break

        else:
            error = (
                "Le champs "
                + str(list(data_keys)[list(structure_keys).index(k)])
                + " devrait être "
                + k
                + "."
            )
            break

    return error


# Import d'un fichier JSON
@app.post("/importation")
def importation(data: dict):

    structure = {
        "id": "",
        "dcat:Catalog#": {
            "rdf:ID": "",
            "dct:title": "",
            "skos:altLabel": "",
            "adms:identifier": "",
            "dct:identifier": "",
            "hdhrdf:MedicalDomain": "",
            "dcat:keyword": "",
            "dct:description": "",
            "hdhrdf:Financing": "",
            "dct:publisher": {
                "foaf:Organization": {
                    "rdf:ID": "",
                    "foaf:name": "",
                    "locn:Adress": "",
                    "hdhrdf:OrganizationStatus": "",
                    "dct:identifier": "",
                    "hdhrdf:ScientificCommittee": "",
                    "foaf:homepage": "",
                }
            },
            "dct:creator": {
                "foaf:Organization": {
                    "rdf:ID": "org-creator",
                    "foaf:name": "",
                    "xds:anyURI": "",
                    "locn:Adress": "",
                    "hdhrdf:Scientist": "",
                    "dct:identifier": "",
                }
            },
            "hdhrdf:Contributors": {
                "foaf:Organization": {
                    "rdf:ID": "org-contributors",
                    "foaf:name": "",
                    "xds:anyURI": "",
                    "locn:Adress": "",
                    "hdhrdf:Scientist": "",
                    "dct:identifier": "",
                }
            },
            "dcat:contactPoint": {
                "foaf:Person": {
                    "rdf:ID": "contactPoint",
                    "foaf:name": "",
                    "locn:Adress": "",
                    "foaf:mbox": "",
                }
            },
            "hdhrdf:Droits": {
                "foaf:Organization": {
                    "rdf:ID": "org-droits",
                    "xds:anyURI": "",
                    "locn:Adress": "",
                    "foaf:mbox": "",
                }
            },
            "hdhrdf:ScientificValorization": "",
            "dct:issued": "",
            "dct:language": "",
            "hdhrdf:Editor": {"foaf:Person": {"rdf:ID": "editor", "foaf:name": ""}},
            "hdhrdf:ResourceType": "",
            "hdhrdf:DataCategory": "",
            "hdhrdf:BaseType": "",
            "hdhrdf:DataType": "",
            "hdhrdf:Objectives": "",
            "dct:source": "",
            "dct:spatial": "",
            "hdhrdf:GeographicalPrecision": "",
            "hdhrdf:PopulationOfInterest": "",
            "hdhrdf:NumberOfParticipants": "",
            "hdhrdf:graph": {
                "hdhrdf:Graph": {
                    "rdf:ID": "Distribution_population",
                    "xds:anyURI": "",
                    "hdhrdf:Mean": "",
                    "hdhrdf:Dispersion": "",
                }
            },
            "dct:format": "",
            "hdhrdf:DataStandard": "",
            "dcat:byteSize": "",
            "hdhrdf:NumberOfTables": "",
            "hdhrdf:NumberOfVariables": "",
            "dct:temporal#start_recueil": {
                "dct:PeriodOfTime": {"rdf:ID": "start_recueil", "schema:startDate": ""}
            },
            "hdhrdf:CollectionStatus": "",
            "hdhrdf:CompletenessRate": "",
            "dct:temporal#end_recueil": {
                "dct:PeriodOfTime": {"rdf:ID": "end_recueil", "schema:endDate": ""}
            },
            "hdhrdf:Historical": "",
            "dcat:modified": "",
            "dct:accrualPeriodicity": "",
            "hdhrdf:MethodOfCollection": "",
            "hdhrdf:MethodOfApproval": "",
            "hdhrdf:CollectionProcess": "",
            "hdhrdf:CollectionProtocol": "",
            "hdhrdf:QualityProcedure": "",
            "hdhrdf:CollectionTools": "",
            "hdhrdf:ParticipantsFollow-up": "",
            "hdhrdf:Follow-upModalities": "",
            "hdhrdf:Follow-upDetails": "",
            "hdhrdf:Paring": "",
            "hdhrdf:ParingType": "",
            "hdhrdf:ParingRate": "",
            "hdhrdf:AccessConditions": "",
            "hdhrdf:ExternalAccessConditions": "",
            "hdhrdf:Sample": "",
            "hdhrdf:Variables": "",
            "hdhrdf:OperatingPrograms": "",
            "hdhrdf:ValuationDocument": "",
            "hdhrdf:Expertise": "",
            "hdhrdf:UsedInProjects": "",
            "dcat:dataset": {
                "dcat:Dataset#": {
                    "rdf:ID": "",
                    "dct:title": "",
                    "dct:description": "",
                    "dct:temporal#mise_en_prod": {
                        "dct:PeriodOfTime": {
                            "rdf:ID": "mise_en_prod",
                            "schema:startDate": "",
                        }
                    },
                    "dct:temporal#arret": {
                        "dct:PeriodOfTime": {"rdf:ID": "arret", "schema:endDate": ""}
                    },
                    "hdhrdf:MissingDates": "",
                    "hdhrdf:graph#Evolution_table_par_annees": {
                        "hdhrdf:Graph": {
                            "rdf:ID": "Evolution_table_par_annees",
                            "xds:anyURI": "",
                            "hdhrdf:Mean": "",
                            "hdhrdf:Dispersion": "",
                        }
                    },
                    "hdhrdf:graph#Evolution_variable_par_annees": {
                        "hdhrdf:Graph": {
                            "rdf:ID": "Evolution_variable_par_annees",
                            "xds:anyURI": "",
                            "hdhrdf:Mean": "",
                            "hdhrdf:Dispersion": "",
                        }
                    },
                    "hdhrdf:PrimaryKey": "",
                    "hdhrdf:ForeignKeys": "",
                    "hdhrdf:variable#": {
                        "hdhrdf:Variable": {
                            "rdf:ID": "",
                            "dct:title": "",
                            "dct:identifier": "",
                            "dct:description": "",
                            "hdhrdf:Type": "",
                            "hdhrdf:Format": "",
                            "hdhrdf:Unit": "",
                            "hdhrdf:ObjectType": "",
                            "hdhrdf:Terminology": "",
                            "hdhrdf:graph#Evolution_terminologie_par_annees": {
                                "hdhrdf:Graph": {
                                    "rdf:ID": "Evolution_terminologie_par_annees",
                                    "xds:anyURI": "",
                                    "hdhrdf:Mean": "",
                                    "hdhrdf:Dispersion": "",
                                }
                            },
                            "hdhrdf:Classification": "",
                            "dct:issued": "",
                            "dcat:modified": "",
                            "hdhrdf:CompletenessDate": "",
                            "hdhrdf:MissingDates": "",
                            "hdhrdf:PresenceInOtherDataset": "",
                            "hdhrdf:NumberOfUniqueValues": "",
                            "hdhrdf:MaxValue": "",
                            "hdhrdf:MinValue": "",
                            "hdhrdf:Mean": "",
                            "hdhrdf:Median": "",
                            "hdhrdf:Variance": "",
                            "hdhrdf:StandardDeviation": "",
                            "hdhrdf:PourcentageMissingValues": "",
                            "hdhrdf:MissingValuesCode": "",
                            "hdhrdf:PourcentageNotAvailableValues": "",
                            "hdhrdf:NotAvailableValuesCode": "",
                            "hdhrdf:graph#Evolution_completude_par_annees": {
                                "hdhrdf:Graph": {
                                    "rdf:ID": "Evolution_completude_par_annees",
                                    "xds:anyURI": "",
                                    "hdhrdf:Mean": "",
                                    "hdhrdf:Dispersion": "",
                                }
                            },
                            "hdhrdf:ComplianceWithTerminologie": "",
                            "hdhrdf:ComplianceWithObjectType": "",
                            "hdhrdf:ComplianceWithUnit": "",
                            "hdhrdf:ComplianceWithFormat": "",
                            "hdhrdf:MeasurementMethodology": "",
                            "hdhrdf:Plausibility": "",
                            "hdhrdf:Terminals": "",
                            "hdhrdf:MediumResolution": "",
                            "hdhrdf:PointsDensity": "",
                            "dcat:byteSize": "",
                            "hdhrdf:Sample": "",
                            "dcat:mediaType": "",
                        }
                    },
                }
            },
        },
    }

    response = verificationKeys(data, structure)

    if response == "":
        client = meilisearch.Client(
            "http://workmetadata.francecentral.cloudapp.azure.com:5002"
        )
        index = client.index("Metadata")

        search = index.search(data["id"])
        for h in search["hits"]:
            if h["id"] == data["id"]:
                error = "Il existe déjà une base avec cet identifiant."
                return error

        resp = index.add_documents(data)

        if index.get_task(resp["uid"])["status"] == "failed":
            return False
        else:
            catalogue = ""
            publisher = ""
            creator = ""
            Contributors = ""
            contactPoint = ""
            Droits = ""
            Editor = ""
            Distribution_population = ""
            start_recueil = ""
            end_recueil = ""

            for key, value in data.items():
                if "dcat:Catalog#" in key:
                    catalogue_name_jena = key.replace("dcat:Catalog#", "")
                    catalogue += (
                        "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                        + catalogue_name_jena
                        + "> a dcat:Catalog; "
                    )
                    datasets = ""

                    for key_cat, val_cat in value.items():
                        if "dct:publisher" in key_cat:
                            for k, v in val_cat["foaf:Organization"].items():
                                publisher += str(k) + ' "' + str(v) + '"; '
                        elif "dct:creator" in key_cat:
                            for k, v in val_cat["foaf:Organization"].items():
                                creator += str(k) + ' "' + str(v) + '"; '

                        elif "hdhrdf:Contributors" in key_cat:
                            for k, v in val_cat["foaf:Organization"].items():
                                Contributors += str(k) + ' "' + str(v) + '"; '

                        elif "dcat:contactPoint" in key_cat:
                            for k, v in val_cat["foaf:Person"].items():
                                contactPoint += str(k) + ' "' + str(v) + '"; '

                        elif "hdhrdf:Droits" in key_cat:
                            for k, v in val_cat["foaf:Organization"].items():
                                Droits += str(k) + ' "' + str(v) + '"; '

                        elif "hdhrdf:Editor" in key_cat:
                            for k, v in val_cat["foaf:Person"].items():
                                Editor += str(k) + ' "' + str(v) + '"; '

                        elif "hdhrdf:graph" in key_cat:
                            for k, v in val_cat["hdhrdf:Graph"].items():
                                Distribution_population += (
                                    str(k) + ' "' + str(v) + '"; '
                                )

                        elif "dct:temporal#start_recueil" in key_cat:
                            for k, v in val_cat["dct:PeriodOfTime"].items():
                                start_recueil += str(k) + ' "' + str(v) + '"; '

                        elif "dct:temporal#end_recueil" in key_cat:
                            for k, v in val_cat["dct:PeriodOfTime"].items():
                                end_recueil += str(k) + ' "' + str(v) + '"; '

                        elif "dcat:dataset" in key_cat:
                            variables = ""
                            mise_en_prod = ""
                            arret = ""
                            Evolution_table = ""
                            Evolution_variable = ""

                            for key_dat, value_dat in val_cat.items():
                                dataset_name = key_dat.replace("dcat:Dataset#", "")
                                dataset = (
                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + "> a dcat:Dataset; "
                                )
                                catalogue = (
                                    catalogue[:-2]
                                    + "; "
                                    + str(key_dat).split("#")[0]
                                    + " "
                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                    + catalogue_name_jena
                                    + "/"
                                    + dataset_name
                                    + '>". '
                                )

                                for k_dat, v_dat in value_dat.items():
                                    if "dct:temporal#mise_en_prod" in k_dat:
                                        for k, v in v_dat["dct:PeriodOfTime"].items():
                                            mise_en_prod += (
                                                str(k) + ' "' + str(v) + '"; '
                                            )
                                        datasets += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#mise_en_prod> a dct:PeriodOfTime; "
                                            + mise_en_prod
                                            + ".\n"
                                        )
                                        dataset += (
                                            str(k_dat).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#mise_en_prod"
                                            + '>"; '
                                        )

                                    elif "dct:temporal#arret" in k_dat:
                                        for k, v in v_dat["dct:PeriodOfTime"].items():
                                            arret += str(k) + ' "' + str(v) + '"; '
                                        datasets += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#arret> a dct:PeriodOfTime; "
                                            + arret
                                            + ".\n"
                                        )
                                        dataset += (
                                            str(k_dat).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#arret"
                                            + '>"; '
                                        )

                                    elif "hdhrdf:graph#Evolution_table" in k_dat:
                                        for k, v in v_dat["hdhrdf:Graph"].items():
                                            Evolution_table += (
                                                str(k) + ' "' + str(v) + '"; '
                                            )
                                        datasets += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#Evolution_table_par_années> a hdhrdf:Graph; "
                                            + Evolution_table
                                            + ".\n"
                                        )
                                        dataset += (
                                            str(k_dat).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#Evolution_table_par_années"
                                            + '>"; '
                                        )

                                    elif "hdhrdf:graph#Evolution_variable" in k_dat:
                                        for k, v in v_dat["hdhrdf:Graph"].items():
                                            Evolution_variable += (
                                                str(k) + ' "' + str(v) + '"; '
                                            )
                                        datasets += (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#Evolution_variable_par_années> a hdhrdf:Graph; "
                                            + Evolution_variable
                                            + ".\n"
                                        )
                                        dataset += (
                                            str(k_dat).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "#Evolution_variable_par_années"
                                            + '>"; '
                                        )

                                    elif "hdhrdf:variable#" in k_dat:
                                        variable_name = k_dat.replace(
                                            "hdhrdf:variable#", ""
                                        )
                                        variable = (
                                            "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + "> a hdhrdf:Variable; "
                                        )
                                        dataset = (
                                            dataset[:-2]
                                            + "; "
                                            + str(k_dat).split("#")[0]
                                            + " "
                                            + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                            + catalogue_name_jena
                                            + "/"
                                            + dataset_name
                                            + "/"
                                            + variable_name
                                            + '>". '
                                        )

                                        Evolution_terminologie = ""
                                        Evolution_completude = ""

                                        for key_var, val_var in v_dat[
                                            "hdhrdf:Variable"
                                        ].items():
                                            if (
                                                "hdhrdf:graph#Evolution_terminologie"
                                                in key_var
                                            ):
                                                for k, v in val_var[
                                                    "hdhrdf:Graph"
                                                ].items():
                                                    Evolution_terminologie += (
                                                        str(k) + ' "' + str(v) + '"; '
                                                    )
                                                variables += (
                                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                                    + catalogue_name_jena
                                                    + "/"
                                                    + dataset_name
                                                    + "/"
                                                    + variable_name
                                                    + "#Evolution_terminologie_par_années> a hdhrdf:Graph; "
                                                    + Evolution_terminologie
                                                    + ".\n"
                                                )
                                                variable += (
                                                    str(key_var).split("#")[0]
                                                    + " "
                                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                                    + catalogue_name_jena
                                                    + "/"
                                                    + dataset_name
                                                    + "/"
                                                    + variable_name
                                                    + "#Evolution_terminologie_par_années"
                                                    + '>"; '
                                                )

                                            elif (
                                                "hdhrdf:graph#Evolution_compl"
                                                in key_var
                                            ):
                                                for k, v in val_var[
                                                    "hdhrdf:Graph"
                                                ].items():
                                                    Evolution_completude += (
                                                        str(k) + ' "' + str(v) + '"; '
                                                    )
                                                variables += (
                                                    "<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                                                    + catalogue_name_jena
                                                    + "/"
                                                    + dataset_name
                                                    + "/"
                                                    + variable_name
                                                    + "#Evolution_complétude_par_années> a hdhrdf:Graph; "
                                                    + Evolution_completude
                                                    + ".\n"
                                                )
                                                variable += (
                                                    str(key_var).split("#")[0]
                                                    + " "
                                                    + '"<http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/'
                                                    + catalogue_name_jena
                                                    + "/"
                                                    + dataset_name
                                                    + "/"
                                                    + variable_name
                                                    + "#Evolution_complétude_par_années"
                                                    + '>"; '
                                                )

                                            else:
                                                variable += (
                                                    str(key_var)
                                                    + ' "'
                                                    + str(val_var)
                                                    + '"; '
                                                )

                                        variables += variable[:-2] + ". \n"

                                    else:
                                        dataset += (
                                            str(k_dat) + ' "' + str(v_dat) + '"; '
                                        )

                                datasets += dataset + "\n"

                            datasets += variables

                        else:
                            catalogue += str(key_cat) + ' "' + str(val_cat) + '"; '

            prefixes = "prefix dct: <http://purl.org/dc/terms/>\n \
                        prefix dcat: <http://www.w3.org/ns/dcat#>\n \
                        prefix foaf: <http://xmlns.com/foaf/0.1/>\n \
                        prefix skos: <http://www.w3.org/2004/02/skos/core#>\n \
                        prefix adms: <http://www.w3.org/ns/adms#>\n \
                        prefix xds: <http://www.w3.org/2001/XMLSchema#>\n \
                        prefix locn: <https://www.w3.org/ns/locn#>\n \
                        prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n \
                        prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n \
                        prefix schema: <http://schema.org/>\n \
                        prefix hdhrdf: <http://workmetadata.francecentral.cloudapp.azure.com:5005/hdh_rdf#>\n"

            query = (
                prefixes
                + "\n \
                    INSERT DATA { \n \
                        GRAPH <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "/>{\n \
                            "
                + catalogue[:-2]
                + ". \n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#publisher> a foaf:Organization; "
                + publisher[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#creator> a foaf:Organization; "
                + creator[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#Contributors> a foaf:Organization; "
                + Contributors[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#contactPoint> a foaf:Person; "
                + contactPoint[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#Droits> a foaf:Organization; "
                + Droits[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#Editor> a foaf:Person; "
                + Editor[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#Distribution_population> a hdhrdf:Graph; "
                + Distribution_population[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#start_recueil> a dct:PeriodOfTime; "
                + start_recueil[:-2]
                + ".\n \
                            <http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
                + catalogue_name_jena
                + "#end_recueil> a dct:PeriodOfTime; "
                + end_recueil[:-2]
                + ".\n \
                            "
                + datasets
                + "\
                        }\n \
                    }"
            )

            sparql = SPARQLWrapper(
                "http://workmetadata.francecentral.cloudapp.azure.com:5001/metadata/"
            )
            sparql.setQuery(query)
            sparql.setMethod("POST")
            sparql.setReturnFormat(JSON)
            sparql.query()

            return True

    else:
        return error


# Historique insertion d'une nouvelle base
@app.post("/insertion")
def insertion(modification: dict):

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    date = str(modification["date"])[:-5].replace("T", " ")

    insertion = (
        "INSERT INTO insertion "
        "VALUES ('"
        + modification["username"]
        + "', '"
        + modification["base"]
        + "', '"
        + date
        + "')"
    )

    access = (
        "INSERT INTO bases "
        "VALUES ('" + modification["base"] + "', '" + modification["username"] + "')"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(insertion)
        cursor.execute(access)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Historique insertion d'une nouvelle base sauvegardée
@app.post("/insertion_in_progress")
def insertion(modification: dict):
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    access = (
        "INSERT INTO bases_in_progress "
        "VALUES ('" + modification["base"] + "', '" + modification["username"] + "')"
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(access)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Supprimer de l'historique des bases sauvegardées
@app.post("/supp_in_progress")
def suppression(modification: dict):
    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata_in_progress")

    supp = index.delete_document(
        modification["id_document"] + "_" + modification["token"]
    )

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    supp = (
        'DELETE FROM bases_in_progress WHERE id_base="'
        + modification["id_document"]
        + '" AND username="'
        + modification["token"]
        + '"'
    )

    try:
        print("Insert {}: ".format("insertion"), end="")
        cursor.execute(supp)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Historique modification d'une base
@app.post("/modification")
def modification(modification: dict):

    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    date = str(modification["date"])[:-5].replace("T", " ")

    listModification = modification["modification"]

    for m in listModification:
        inputModification = m
        valueModification = listModification[m]

        add_modification = (
            "INSERT INTO modification "
            "VALUES ('"
            + modification["username"]
            + "', '"
            + modification["base"]
            + "', '"
            + date
            + "', '"
            + inputModification
            + "', '"
            + valueModification
            + "')"
        )

        try:
            print("Insert {}: ".format("modification"), end="")
            cursor.execute(add_modification)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")

    cnx.commit()

    cursor.close()
    cnx.close()


# Récupération des informations d'une base pour la modifier
@app.post("/edit_base/{base_id}")
def edit_base(base_id: str):
    url = (
        "http://workmetadata.francecentral.cloudapp.azure.com:5002/indexes/Metadata/documents/"
        + base_id
    )

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"

    resp = requests.get(url)
    resp = resp.json()

    state = {}

    # Récupération des clés / valeurs des données reçues
    keys = resp.keys()
    values = resp.values()

    # Parcours des clés
    for k in keys:
        if k != "id":
            keys_cat = resp[k].keys()

            # Parcours des clés catalogue
            for k_cat in keys_cat:
                if k_cat == "rdf:ID":
                    state["base_ID"] = resp[k][k_cat]

                if k_cat == "dct:title":
                    state["base_titre"] = resp[k][k_cat]

                if k_cat == "skos:altLabel":
                    state["base_acronyme"] = resp[k][k_cat]

                if k_cat == "adms:identifier":
                    state["base_identifiant_DOI"] = resp[k][k_cat]

                if k_cat == "dct:identifier":
                    state["base_reference_HDH"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MedicalDomain":
                    state["base_domaine_medical"] = resp[k][k_cat]

                if k_cat == "dcat:keyword":
                    state["base_keyword"] = resp[k][k_cat]

                if k_cat == "dct:description":
                    state["base_description"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Financing":
                    state["base_financement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ScientificValorization":
                    state["base_valorisation_scientifique"] = resp[k][k_cat]

                if k_cat == "dct:issued":
                    state["base_annee_publication"] = resp[k][k_cat]

                if k_cat == "dct:language":
                    state["base_langue"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ResourceType":
                    state["base_type_ressource"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataCategory":
                    state["base_categorie_donnees"] = resp[k][k_cat]

                if k_cat == "hdhrdf:BaseType":
                    state["base_type_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataType":
                    state["base_type_donnees"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Objectives":
                    state["base_objectifs"] = resp[k][k_cat]

                if k_cat == "dct:source":
                    state["base_source"] = resp[k][k_cat]

                if k_cat == "dct:spatial":
                    state["base_perimetre_geographique"] = resp[k][k_cat]

                if k_cat == "hdhrdf:GeographicalPrecision":
                    state["base_precision_geographique"] = resp[k][k_cat]

                if k_cat == "hdhrdf:PopulationOfInterest":
                    state["base_population_interet"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfParticipants":
                    state["base_nombre_participants"] = resp[k][k_cat]

                if k_cat == "dct:format":
                    state["base_format_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataStandard":
                    state["base_standard_donnees"] = resp[k][k_cat]

                if k_cat == "dcat:byteSize":
                    state["base_taille_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfTables":
                    state["base_nombre_tables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfVariables":
                    state["base_nombre_variables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionStatus":
                    state["base_status_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CompletenessRate":
                    state["base_taux_exhaustivite"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Historical":
                    state["base_profondeur_historique"] = resp[k][k_cat]

                if k_cat == "dcat:modified":
                    state["base_derniere_modification"] = resp[k][k_cat]

                if k_cat == "dct:accrualPeriodicity":
                    state["base_frequence_modification"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MethodOfCollection":
                    state["base_mode_recueil"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MethodOfApproval":
                    state["base_modalite_approbation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionProcess":
                    state["base_processus_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionProtocol":
                    state["base_protocole_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:QualityProcedure":
                    state["base_procedure_qualite"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionTools":
                    state["base_outils_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParticipantsFollow-up":
                    state["base_suivi_participants"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Follow-upModalities":
                    state["base_modalites_suivi"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Follow-upDetails":
                    state["base_details_suivi"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Paring":
                    state["base_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParingType":
                    state["base_type_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParingRate":
                    state["base_taux_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:AccessConditions":
                    state["base_conditions_access_HDH"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ExternalAccessConditions":
                    state["base_conditions_access_externe"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Sample":
                    state["base_echantillon"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Variables":
                    state["base_variables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:OperatingPrograms":
                    state["base_programmes_exploitation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ValuationDocument":
                    state["base_document_valorisation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Expertise":
                    state["base_expertise"] = resp[k][k_cat]

                if k_cat == "hdhrdf:UsedInProjects":
                    state["base_projets"] = resp[k][k_cat]

                if k_cat == "dct:publisher":
                    keys_publisher = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_publisher in keys_publisher:
                        if k_publisher == "foaf:name":
                            state["base_publisher_organisation"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "locn:Adress":
                            state["base_publisher_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "hdhrdf:OrganizationStatus":
                            state["base_publisher_status"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "dct:identifier":
                            state["base_publisher_ROR"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "hdhrdf:ScientificCommittee":
                            state["base_publisher_comite"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "foaf:homepage":
                            state["base_publisher_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]

                if k_cat == "dct:creator":
                    keys_creator = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_creator in keys_creator:
                        if k_creator == "foaf:name":
                            state["base_creator_institut"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "xds:anyURI":
                            state["base_creator_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "locn:Adress":
                            state["base_creator_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "hdhrdf:Scientist":
                            state["base_creator_responsable"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "dct:identifier":
                            state["base_creator_identifiant"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]

                if k_cat == "hdhrdf:Contributors":
                    keys_contributors = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_contributors in keys_contributors:
                        if k_contributors == "foaf:name":
                            state["base_contributors_institut"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "xds:anyURI":
                            state["base_contributors_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "locn:Adress":
                            state["base_contributors_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "hdhrdf:Scientist":
                            state["base_contributors_responsable"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "dct:identifier":
                            state["base_contributors_identifiant"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]

                if k_cat == "dcat:contactPoint":
                    keys_contact = resp[k][k_cat]["foaf:Person"].keys()
                    for k_contact in keys_contact:
                        if k_contact == "foaf:name":
                            state["base_contact_nom"] = resp[k][k_cat]["foaf:Person"][
                                k_contact
                            ]
                        if k_contact == "locn:Adress":
                            state["base_contact_adresse"] = resp[k][k_cat][
                                "foaf:Person"
                            ][k_contact]
                        if k_contact == "foaf:mbox":
                            state["base_contact_mail"] = resp[k][k_cat]["foaf:Person"][
                                k_contact
                            ]

                if k_cat == "hdhrdf:Droits":
                    keys_droits = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_droits in keys_droits:
                        if k_droits == "xds:anyURI":
                            state["base_droits_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]
                        if k_droits == "locn:Adress":
                            state["base_droits_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]
                        if k_droits == "foaf:mbox":
                            state["base_droits_mail"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]

                if k_cat == "hdhrdf:Editor":
                    keys_editor = resp[k][k_cat]["foaf:Person"].keys()
                    for k_editor in keys_editor:
                        if k_editor == "foaf:name":
                            state["base_editor_nom"] = resp[k][k_cat]["foaf:Person"][
                                k_editor
                            ]

                if k_cat == "hdhrdf:graph":
                    keys_graph = resp[k][k_cat]["hdhrdf:Graph"].keys()
                    for k_graph in keys_graph:
                        if k_graph == "xds:anyURI":
                            state["base_distribution_population_lien"] = resp[k][k_cat][
                                "hdhrdf:Graph"
                            ][k_graph]
                        if k_graph == "hdhrdf:Mean":
                            state["base_distribution_population_moyenne"] = resp[k][
                                k_cat
                            ]["hdhrdf:Graph"][k_graph]
                        if k_graph == "hdhrdf:Dispersion":
                            state["base_distribution_population_dispersion"] = resp[k][
                                k_cat
                            ]["hdhrdf:Graph"][k_graph]

                if k_cat == "dct:temporal#start_recueil":
                    keys_start = resp[k][k_cat]["dct:PeriodOfTime"].keys()
                    for k_start in keys_start:
                        if k_start == "schema:startDate":
                            state["base_start_recueil"] = resp[k][k_cat][
                                "dct:PeriodOfTime"
                            ][k_start]

                if k_cat == "dct:temporal#end_recueil":
                    keys_end = resp[k][k_cat]["dct:PeriodOfTime"].keys()
                    for k_end in keys_end:
                        if k_end == "schema:endDate":
                            state["base_end_recueil"] = resp[k][k_cat][
                                "dct:PeriodOfTime"
                            ][k_end]

                if k_cat == "dcat:dataset":
                    keys_dat = resp[k][k_cat].keys()
                    nb_table = -1
                    # Parcours des datasets
                    for table in keys_dat:
                        keys_table = resp[k][k_cat][table].keys()
                        nb_table += 1
                        nb_var = -1
                        # Parcours des clés d'une table
                        for k_table in keys_table:
                            if k_table == "rdf:ID":
                                state["table_ID#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dct:title":
                                state["table_titre#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dct:description":
                                state["table_description#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "dct:identifier":
                                state["table_reference#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "dct:format":
                                state["table_format#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dcat:byteSize":
                                state["table_taille#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "hdhrdf:Variables":
                                state["table_variables#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "hdhrdf:MissingDates":
                                state["table_dates_manquantes#" + str(nb_table)] = resp[
                                    k
                                ][k_cat][table][k_table]

                            if k_table == "hdhrdf:PrimaryKey":
                                state["table_cle_primaire#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "hdhrdf:PrimaryKey":
                                state["table_cles_secondaires#" + str(nb_table)] = resp[
                                    k
                                ][k_cat][table][k_table]

                            if k_table == "dct:temporal#mise_en_prod":
                                keys_start = resp[k][k_cat][table][k_table][
                                    "dct:PeriodOfTime"
                                ].keys()
                                for k_start in keys_start:
                                    if k_start == "schema:startDate":
                                        state[
                                            "table_mise_en_production#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "dct:PeriodOfTime"
                                        ][
                                            k_start
                                        ]

                            if k_table == "dct:temporal#arret":
                                keys_end = resp[k][k_cat][table][k_table][
                                    "dct:PeriodOfTime"
                                ].keys()
                                for k_end in keys_end:
                                    if k_end == "schema:endDate":
                                        state["table_arret#" + str(nb_table)] = resp[k][
                                            k_cat
                                        ][table][k_table]["dct:PeriodOfTime"][k_end]

                            if k_table == "hdhrdf:graph#Evolution_table_par_années":
                                keys_evolution_table = resp[k][k_cat][table][k_table][
                                    "hdhrdf:Graph"
                                ].keys()
                                for k_evolution_table in keys_evolution_table:
                                    if k_evolution_table == "xds:anyURI":
                                        state[
                                            "table_graph_table_lien#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]
                                    if k_evolution_table == "hdhrdf:Mean":
                                        state[
                                            "table_graph_table_moyenne#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]
                                    if k_evolution_table == "hdhrdf:Dispersion":
                                        state[
                                            "table_graph_table_dispersion#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]

                            if k_table == "hdhrdf:graph#Evolution_variable_par_années":
                                keys_evolution_variable = resp[k][k_cat][table][
                                    k_table
                                ]["hdhrdf:Graph"].keys()
                                for k_evolution_variable in keys_evolution_variable:
                                    if k_evolution_variable == "xds:anyURI":
                                        state[
                                            "table_graph_variables_lien#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]
                                    if k_evolution_variable == "hdhrdf:Mean":
                                        state[
                                            "table_graph_variables_moyenne#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]
                                    if k_evolution_variable == "hdhrdf:Dispersion":
                                        state[
                                            "table_graph_variables_dispersion#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]

                            if k_table.split("#")[0] == "hdhrdf:variable":

                                keys_var = resp[k][k_cat][table][k_table][
                                    "hdhrdf:Variable"
                                ].keys()
                                nb_var += 1

                                # Parcours des clés d'une table
                                for k_var in keys_var:

                                    if k_var == "rdf:ID":
                                        state[
                                            "variable_ID#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:identifier":
                                        state[
                                            "variable_identifiant#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:title":
                                        state[
                                            "variable_titre#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:description":
                                        state[
                                            "variable_description#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Type":
                                        state[
                                            "variable_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Format":
                                        state[
                                            "variable_format#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Unit":
                                        state[
                                            "variable_unite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ObjectType":
                                        state[
                                            "variable_type_objet#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Terminology":
                                        state[
                                            "variable_terminologie#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Classification":
                                        state[
                                            "variable_classification#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:CompletenessDate":
                                        state[
                                            "variable_exhaustivite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MissingDates":
                                        state[
                                            "variable_dates_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PresenceInOtherDataset":
                                        state[
                                            "variable_tables_presence#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:NumberOfUniqueValues":
                                        state[
                                            "variable_nombre_valeurs_uniques#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MaxValue":
                                        state[
                                            "variable_valeur_max#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MinValue":
                                        state[
                                            "variable_valeur_min#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_moyenne#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_mediane#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_variance#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:StandardDeviation":
                                        state[
                                            "variable_ecart_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PourcentageMissingValues":
                                        state[
                                            "variable_pourcentage_valeurs_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MissingValuesCode":
                                        state[
                                            "variable_code_valeurs_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PourcentageNotAvailableValues":
                                        state[
                                            "variable_pourcentage_valeurs_non_dispo#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:NotAvailableValuesCode":
                                        state[
                                            "variable_code_valeurs_non_dispo#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithTerminologie":
                                        state[
                                            "variable_conformite_terminologies#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithObjectType":
                                        state[
                                            "variable_conformite_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithUnit":
                                        state[
                                            "variable_conformite_unite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithFormat":
                                        state[
                                            "variable_conformite_format#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MeasurementMethodology":
                                        state[
                                            "variable_methodologie_mesure#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Plausibility":
                                        state[
                                            "variable_pausibilite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Terminals":
                                        state[
                                            "variable_bornes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MediumResolution":
                                        state[
                                            "variable_resolution_moyenne#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PointsDensity":
                                        state[
                                            "variable_densite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dcat:byteSize":
                                        state[
                                            "variable_taille#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Sample":
                                        state[
                                            "variable_echantillon#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dcat:mediaType":
                                        state[
                                            "variable_format_images#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if (
                                        k_var
                                        == "hdhrdf:graph#Evolution_teminologie_par_années_V_actes"
                                    ):
                                        keys_evolution_terminologie = resp[k][k_cat][
                                            table
                                        ][k_table]["hdhrdf:Variable"][k_var][
                                            "hdhrdf:Graph"
                                        ].keys()
                                        for (
                                            k_evolution_terminologie
                                        ) in keys_evolution_terminologie:
                                            if k_evolution_terminologie == "xds:anyURI":
                                                state[
                                                    "variable_graph_terminologie_lien#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]
                                            if (
                                                k_evolution_terminologie
                                                == "hdhrdf:Mean"
                                            ):
                                                state[
                                                    "variable_graph_terminologie_moyenne#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]
                                            if (
                                                k_evolution_terminologie
                                                == "hdhrdf:Dispersion"
                                            ):
                                                state[
                                                    "variable_graph_terminologie_dispersion#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]

                                    if k_var == "dct:temporal#mise_en_prod":
                                        keys_start = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][k_var]["dct:PeriodOfTime"].keys()
                                        for k_start in keys_start:
                                            if k_start == "schema:startDate":
                                                state[
                                                    "variable_mise_en_production#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "dct:PeriodOfTime"
                                                ][
                                                    k_start
                                                ]

                                    if k_var == "dct:temporal#arret":
                                        keys_end = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][k_var]["dct:PeriodOfTime"].keys()
                                        for k_end in keys_end:
                                            if k_end == "schema:endDate":
                                                state[
                                                    "variable_fin#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "dct:PeriodOfTime"
                                                ][
                                                    k_end
                                                ]

                                    if (
                                        k_var
                                        == "hdhrdf:graph#Evolution_complétude_par_années_V_actes"
                                    ):
                                        keys_evolution_completude = resp[k][k_cat][
                                            table
                                        ][k_table]["hdhrdf:Variable"][k_var][
                                            "hdhrdf:Graph"
                                        ].keys()
                                        for (
                                            k_evolution_completude
                                        ) in keys_evolution_completude:
                                            if k_evolution_completude == "xds:anyURI":
                                                state[
                                                    "variable_graph_completude_lien#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]
                                            if k_evolution_completude == "hdhrdf:Mean":
                                                state[
                                                    "variable_graph_completude_moyenne#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]
                                            if (
                                                k_evolution_completude
                                                == "hdhrdf:Dispersion"
                                            ):
                                                state[
                                                    "variable_graph_completude_dispersion#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]

    return state


# Récupération des informations d'une base pour la modifier
@app.post("/edit_base_in_progress/{base_id}")
def edit_base(base_id: str):
    url = (
        "http://workmetadata.francecentral.cloudapp.azure.com:5002/indexes/Metadata_in_progress/documents/"
        + base_id
    )

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/json"

    resp = requests.get(url)
    resp = resp.json()

    state = {}

    # Récupération des clés / valeurs des données reçues
    keys = resp.keys()
    values = resp.values()

    # Parcours des clés
    for k in keys:
        if k != "id":
            keys_cat = resp[k].keys()

            # Parcours des clés catalogue
            for k_cat in keys_cat:
                if k_cat == "rdf:ID":
                    state["base_ID"] = resp[k][k_cat]

                if k_cat == "dct:title":
                    state["base_titre"] = resp[k][k_cat]

                if k_cat == "skos:altLabel":
                    state["base_acronyme"] = resp[k][k_cat]

                if k_cat == "adms:identifier":
                    state["base_identifiant_DOI"] = resp[k][k_cat]

                if k_cat == "dct:identifier":
                    state["base_reference_HDH"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MedicalDomain":
                    state["base_domaine_medical"] = resp[k][k_cat]

                if k_cat == "dcat:keyword":
                    state["base_keyword"] = resp[k][k_cat]

                if k_cat == "dct:description":
                    state["base_description"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Financing":
                    state["base_financement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ScientificValorization":
                    state["base_valorisation_scientifique"] = resp[k][k_cat]

                if k_cat == "dct:issued":
                    state["base_annee_publication"] = resp[k][k_cat]

                if k_cat == "dct:language":
                    state["base_langue"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ResourceType":
                    state["base_type_ressource"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataCategory":
                    state["base_categorie_donnees"] = resp[k][k_cat]

                if k_cat == "hdhrdf:BaseType":
                    state["base_type_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataType":
                    state["base_type_donnees"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Objectives":
                    state["base_objectifs"] = resp[k][k_cat]

                if k_cat == "dct:source":
                    state["base_source"] = resp[k][k_cat]

                if k_cat == "dct:spatial":
                    state["base_perimetre_geographique"] = resp[k][k_cat]

                if k_cat == "hdhrdf:GeographicalPrecision":
                    state["base_precision_geographique"] = resp[k][k_cat]

                if k_cat == "hdhrdf:PopulationOfInterest":
                    state["base_population_interet"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfParticipants":
                    state["base_nombre_participants"] = resp[k][k_cat]

                if k_cat == "dct:format":
                    state["base_format_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:DataStandard":
                    state["base_standard_donnees"] = resp[k][k_cat]

                if k_cat == "dcat:byteSize":
                    state["base_taille_base"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfTables":
                    state["base_nombre_tables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:NumberOfVariables":
                    state["base_nombre_variables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionStatus":
                    state["base_status_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CompletenessRate":
                    state["base_taux_exhaustivite"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Historical":
                    state["base_profondeur_historique"] = resp[k][k_cat]

                if k_cat == "dcat:modified":
                    state["base_derniere_modification"] = resp[k][k_cat]

                if k_cat == "dct:accrualPeriodicity":
                    state["base_frequence_modification"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MethodOfCollection":
                    state["base_mode_recueil"] = resp[k][k_cat]

                if k_cat == "hdhrdf:MethodOfApproval":
                    state["base_modalite_approbation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionProcess":
                    state["base_processus_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionProtocol":
                    state["base_protocole_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:QualityProcedure":
                    state["base_procedure_qualite"] = resp[k][k_cat]

                if k_cat == "hdhrdf:CollectionTools":
                    state["base_outils_collecte"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParticipantsFollow-up":
                    state["base_suivi_participants"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Follow-upModalities":
                    state["base_modalites_suivi"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Follow-upDetails":
                    state["base_details_suivi"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Paring":
                    state["base_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParingType":
                    state["base_type_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ParingRate":
                    state["base_taux_appariement"] = resp[k][k_cat]

                if k_cat == "hdhrdf:AccessConditions":
                    state["base_conditions_access_HDH"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ExternalAccessConditions":
                    state["base_conditions_access_externe"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Sample":
                    state["base_echantillon"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Variables":
                    state["base_variables"] = resp[k][k_cat]

                if k_cat == "hdhrdf:OperatingPrograms":
                    state["base_programmes_exploitation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:ValuationDocument":
                    state["base_document_valorisation"] = resp[k][k_cat]

                if k_cat == "hdhrdf:Expertise":
                    state["base_expertise"] = resp[k][k_cat]

                if k_cat == "hdhrdf:UsedInProjects":
                    state["base_projets"] = resp[k][k_cat]

                if k_cat == "dct:publisher":
                    keys_publisher = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_publisher in keys_publisher:
                        if k_publisher == "foaf:name":
                            state["base_publisher_organisation"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "locn:Adress":
                            state["base_publisher_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "hdhrdf:OrganizationStatus":
                            state["base_publisher_status"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "dct:identifier":
                            state["base_publisher_ROR"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "hdhrdf:ScientificCommittee":
                            state["base_publisher_comite"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]
                        if k_publisher == "foaf:homepage":
                            state["base_publisher_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_publisher]

                if k_cat == "dct:creator":
                    keys_creator = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_creator in keys_creator:
                        if k_creator == "foaf:name":
                            state["base_creator_institut"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "xds:anyURI":
                            state["base_creator_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "locn:Adress":
                            state["base_creator_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "hdhrdf:Scientist":
                            state["base_creator_responsable"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]
                        if k_creator == "dct:identifier":
                            state["base_creator_identifiant"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_creator]

                if k_cat == "hdhrdf:Contributors":
                    keys_contributors = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_contributors in keys_contributors:
                        if k_contributors == "foaf:name":
                            state["base_contributors_institut"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "xds:anyURI":
                            state["base_contributors_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "locn:Adress":
                            state["base_contributors_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "hdhrdf:Scientist":
                            state["base_contributors_responsable"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]
                        if k_contributors == "dct:identifier":
                            state["base_contributors_identifiant"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_contributors]

                if k_cat == "dcat:contactPoint":
                    keys_contact = resp[k][k_cat]["foaf:Person"].keys()
                    for k_contact in keys_contact:
                        if k_contact == "foaf:name":
                            state["base_contact_nom"] = resp[k][k_cat]["foaf:Person"][
                                k_contact
                            ]
                        if k_contact == "locn:Adress":
                            state["base_contact_adresse"] = resp[k][k_cat][
                                "foaf:Person"
                            ][k_contact]
                        if k_contact == "foaf:mbox":
                            state["base_contact_mail"] = resp[k][k_cat]["foaf:Person"][
                                k_contact
                            ]

                if k_cat == "hdhrdf:Droits":
                    keys_droits = resp[k][k_cat]["foaf:Organization"].keys()
                    for k_droits in keys_droits:
                        if k_droits == "xds:anyURI":
                            state["base_droits_site"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]
                        if k_droits == "locn:Adress":
                            state["base_droits_adresse"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]
                        if k_droits == "foaf:mbox":
                            state["base_droits_mail"] = resp[k][k_cat][
                                "foaf:Organization"
                            ][k_droits]

                if k_cat == "hdhrdf:Editor":
                    keys_editor = resp[k][k_cat]["foaf:Person"].keys()
                    for k_editor in keys_editor:
                        if k_editor == "foaf:name":
                            state["base_editor_nom"] = resp[k][k_cat]["foaf:Person"][
                                k_editor
                            ]

                if k_cat == "hdhrdf:graph":
                    keys_graph = resp[k][k_cat]["hdhrdf:Graph"].keys()
                    for k_graph in keys_graph:
                        if k_graph == "xds:anyURI":
                            state["base_distribution_population_lien"] = resp[k][k_cat][
                                "hdhrdf:Graph"
                            ][k_graph]
                        if k_graph == "hdhrdf:Mean":
                            state["base_distribution_population_moyenne"] = resp[k][
                                k_cat
                            ]["hdhrdf:Graph"][k_graph]
                        if k_graph == "hdhrdf:Dispersion":
                            state["base_distribution_population_dispersion"] = resp[k][
                                k_cat
                            ]["hdhrdf:Graph"][k_graph]

                if k_cat == "dct:temporal#start_recueil":
                    keys_start = resp[k][k_cat]["dct:PeriodOfTime"].keys()
                    for k_start in keys_start:
                        if k_start == "schema:startDate":
                            state["base_start_recueil"] = resp[k][k_cat][
                                "dct:PeriodOfTime"
                            ][k_start]

                if k_cat == "dct:temporal#end_recueil":
                    keys_end = resp[k][k_cat]["dct:PeriodOfTime"].keys()
                    for k_end in keys_end:
                        if k_end == "schema:endDate":
                            state["base_end_recueil"] = resp[k][k_cat][
                                "dct:PeriodOfTime"
                            ][k_end]

                if k_cat == "dcat:dataset":
                    keys_dat = resp[k][k_cat].keys()
                    nb_table = -1
                    # Parcours des datasets
                    for table in keys_dat:
                        keys_table = resp[k][k_cat][table].keys()
                        nb_table += 1
                        nb_var = -1
                        # Parcours des clés d'une table
                        for k_table in keys_table:
                            if k_table == "rdf:ID":
                                state["table_ID#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dct:title":
                                state["table_titre#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dct:description":
                                state["table_description#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "dct:identifier":
                                state["table_reference#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "dct:format":
                                state["table_format#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "dcat:byteSize":
                                state["table_taille#" + str(nb_table)] = resp[k][k_cat][
                                    table
                                ][k_table]

                            if k_table == "hdhrdf:Variables":
                                state["table_variables#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "hdhrdf:MissingDates":
                                state["table_dates_manquantes#" + str(nb_table)] = resp[
                                    k
                                ][k_cat][table][k_table]

                            if k_table == "hdhrdf:PrimaryKey":
                                state["table_cle_primaire#" + str(nb_table)] = resp[k][
                                    k_cat
                                ][table][k_table]

                            if k_table == "hdhrdf:PrimaryKey":
                                state["table_cles_secondaires#" + str(nb_table)] = resp[
                                    k
                                ][k_cat][table][k_table]

                            if k_table == "dct:temporal#mise_en_prod":
                                keys_start = resp[k][k_cat][table][k_table][
                                    "dct:PeriodOfTime"
                                ].keys()
                                for k_start in keys_start:
                                    if k_start == "schema:startDate":
                                        state[
                                            "table_mise_en_production#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "dct:PeriodOfTime"
                                        ][
                                            k_start
                                        ]

                            if k_table == "dct:temporal#arret":
                                keys_end = resp[k][k_cat][table][k_table][
                                    "dct:PeriodOfTime"
                                ].keys()
                                for k_end in keys_end:
                                    if k_end == "schema:endDate":
                                        state["table_arret#" + str(nb_table)] = resp[k][
                                            k_cat
                                        ][table][k_table]["dct:PeriodOfTime"][k_end]

                            if k_table == "hdhrdf:graph#Evolution_table_par_années":
                                keys_evolution_table = resp[k][k_cat][table][k_table][
                                    "hdhrdf:Graph"
                                ].keys()
                                for k_evolution_table in keys_evolution_table:
                                    if k_evolution_table == "xds:anyURI":
                                        state[
                                            "table_graph_table_lien#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]
                                    if k_evolution_table == "hdhrdf:Mean":
                                        state[
                                            "table_graph_table_moyenne#" + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]
                                    if k_evolution_table == "hdhrdf:Dispersion":
                                        state[
                                            "table_graph_table_dispersion#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_table
                                        ]

                            if k_table == "hdhrdf:graph#Evolution_variable_par_années":
                                keys_evolution_variable = resp[k][k_cat][table][
                                    k_table
                                ]["hdhrdf:Graph"].keys()
                                for k_evolution_variable in keys_evolution_variable:
                                    if k_evolution_variable == "xds:anyURI":
                                        state[
                                            "table_graph_variables_lien#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]
                                    if k_evolution_variable == "hdhrdf:Mean":
                                        state[
                                            "table_graph_variables_moyenne#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]
                                    if k_evolution_variable == "hdhrdf:Dispersion":
                                        state[
                                            "table_graph_variables_dispersion#"
                                            + str(nb_table)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Graph"
                                        ][
                                            k_evolution_variable
                                        ]

                            if k_table.split("#")[0] == "hdhrdf:variable":

                                keys_var = resp[k][k_cat][table][k_table][
                                    "hdhrdf:Variable"
                                ].keys()
                                nb_var += 1

                                # Parcours des clés d'une table
                                for k_var in keys_var:

                                    if k_var == "rdf:ID":
                                        state[
                                            "variable_ID#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:identifier":
                                        state[
                                            "variable_identifiant#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:title":
                                        state[
                                            "variable_titre#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dct:description":
                                        state[
                                            "variable_description#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Type":
                                        state[
                                            "variable_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Format":
                                        state[
                                            "variable_format#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Unit":
                                        state[
                                            "variable_unite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ObjectType":
                                        state[
                                            "variable_type_objet#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Terminology":
                                        state[
                                            "variable_terminologie#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Classification":
                                        state[
                                            "variable_classification#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:CompletenessDate":
                                        state[
                                            "variable_exhaustivite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MissingDates":
                                        state[
                                            "variable_dates_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PresenceInOtherDataset":
                                        state[
                                            "variable_tables_presence#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:NumberOfUniqueValues":
                                        state[
                                            "variable_nombre_valeurs_uniques#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MaxValue":
                                        state[
                                            "variable_valeur_max#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MinValue":
                                        state[
                                            "variable_valeur_min#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_moyenne#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_mediane#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Mean":
                                        state[
                                            "variable_variance#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:StandardDeviation":
                                        state[
                                            "variable_ecart_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PourcentageMissingValues":
                                        state[
                                            "variable_pourcentage_valeurs_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MissingValuesCode":
                                        state[
                                            "variable_code_valeurs_manquantes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PourcentageNotAvailableValues":
                                        state[
                                            "variable_pourcentage_valeurs_non_dispo#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:NotAvailableValuesCode":
                                        state[
                                            "variable_code_valeurs_non_dispo#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithTerminologie":
                                        state[
                                            "variable_conformite_terminologies#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithObjectType":
                                        state[
                                            "variable_conformite_type#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithUnit":
                                        state[
                                            "variable_conformite_unite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:ComplianceWithFormat":
                                        state[
                                            "variable_conformite_format#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MeasurementMethodology":
                                        state[
                                            "variable_methodologie_mesure#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Plausibility":
                                        state[
                                            "variable_pausibilite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Terminals":
                                        state[
                                            "variable_bornes#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:MediumResolution":
                                        state[
                                            "variable_resolution_moyenne#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:PointsDensity":
                                        state[
                                            "variable_densite#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dcat:byteSize":
                                        state[
                                            "variable_taille#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "hdhrdf:Sample":
                                        state[
                                            "variable_echantillon#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if k_var == "dcat:mediaType":
                                        state[
                                            "variable_format_images#"
                                            + str(nb_table)
                                            + "#"
                                            + str(nb_var)
                                        ] = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][
                                            k_var
                                        ]

                                    if (
                                        k_var
                                        == "hdhrdf:graph#Evolution_teminologie_par_années_V_actes"
                                    ):
                                        keys_evolution_terminologie = resp[k][k_cat][
                                            table
                                        ][k_table]["hdhrdf:Variable"][k_var][
                                            "hdhrdf:Graph"
                                        ].keys()
                                        for (
                                            k_evolution_terminologie
                                        ) in keys_evolution_terminologie:
                                            if k_evolution_terminologie == "xds:anyURI":
                                                state[
                                                    "variable_graph_terminologie_lien#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]
                                            if (
                                                k_evolution_terminologie
                                                == "hdhrdf:Mean"
                                            ):
                                                state[
                                                    "variable_graph_terminologie_moyenne#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]
                                            if (
                                                k_evolution_terminologie
                                                == "hdhrdf:Dispersion"
                                            ):
                                                state[
                                                    "variable_graph_terminologie_dispersion#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_terminologie
                                                ]

                                    if k_var == "dct:temporal#mise_en_prod":
                                        keys_start = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][k_var]["dct:PeriodOfTime"].keys()
                                        for k_start in keys_start:
                                            if k_start == "schema:startDate":
                                                state[
                                                    "variable_mise_en_production#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "dct:PeriodOfTime"
                                                ][
                                                    k_start
                                                ]

                                    if k_var == "dct:temporal#arret":
                                        keys_end = resp[k][k_cat][table][k_table][
                                            "hdhrdf:Variable"
                                        ][k_var]["dct:PeriodOfTime"].keys()
                                        for k_end in keys_end:
                                            if k_end == "schema:endDate":
                                                state[
                                                    "variable_fin#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "dct:PeriodOfTime"
                                                ][
                                                    k_end
                                                ]

                                    if (
                                        k_var
                                        == "hdhrdf:graph#Evolution_complétude_par_années_V_actes"
                                    ):
                                        keys_evolution_completude = resp[k][k_cat][
                                            table
                                        ][k_table]["hdhrdf:Variable"][k_var][
                                            "hdhrdf:Graph"
                                        ].keys()
                                        for (
                                            k_evolution_completude
                                        ) in keys_evolution_completude:
                                            if k_evolution_completude == "xds:anyURI":
                                                state[
                                                    "variable_graph_completude_lien#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]
                                            if k_evolution_completude == "hdhrdf:Mean":
                                                state[
                                                    "variable_graph_completude_moyenne#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]
                                            if (
                                                k_evolution_completude
                                                == "hdhrdf:Dispersion"
                                            ):
                                                state[
                                                    "variable_graph_completude_dispersion#"
                                                    + str(nb_table)
                                                    + "#"
                                                    + str(nb_var)
                                                ] = resp[k][k_cat][table][k_table][
                                                    "hdhrdf:Variable"
                                                ][
                                                    k_var
                                                ][
                                                    "hdhrdf:Graph"
                                                ][
                                                    k_evolution_completude
                                                ]

    return state


# Recherche sur MeiliSearch
@app.post("/search/{i}")
async def search(i: str):

    client = meilisearch.Client(
        "http://workmetadata.francecentral.cloudapp.azure.com:5002"
    )
    index = client.index("Metadata")

    search = index.search(i)

    return search


# Structure des inputs
@app.get("/new")
def new_step():

    structure_base_required = {
        "base_ID": {"placeholder": "rdf:ID", "label": "ID", "type": "string"}
    }

    structure_base = {
        "base_titre": {"placeholder": "dct:title", "label": "Titre", "type": "string"},
        "base_acronyme": {
            "placeholder": "skos:altLabel",
            "label": "Acronyme",
            "type": "string",
        },
        "base_identifiant_DOI": {
            "placeholder": "adms:identifier",
            "label": "Identifiant DOI",
            "type": "string",
        },
        "base_reference_HDH": {
            "placeholder": "dct:identifier",
            "label": "Référence catalogue HDH",
            "type": "string",
        },
        "base_domaine_medical": {
            "placeholder": "hdhrdf:MedicalDomain",
            "label": "Domaine médical",
            "type": "string",
        },
        "base_keyword": {
            "placeholder": "dcat:keyword",
            "label": "Mots-clés",
            "type": "string",
        },
        "base_description": {
            "placeholder": "dct:description",
            "label": "Description",
            "type": "string",
        },
        "base_financement": {
            "placeholder": "hdhrdf:Financing",
            "label": "Financement",
            "type": "string",
        },
        "base_publisher_organisation": {
            "placeholder": "foaf:name",
            "label": "Organisation responsable",
            "type": "string",
        },
        "base_publisher_adresse": {
            "placeholder": "locn:Adress",
            "label": "Adresse",
            "type": "string",
        },
        "base_publisher_status": {
            "placeholder": "hdhrdf:OrganizationStatus",
            "label": "Status de l'organisation",
            "type": "string",
        },
        "base_publisher_ROR": {
            "placeholder": "dct:identifier",
            "label": "Identifiant numérique ROR",
            "type": "string",
        },
        "base_publisher_comite": {
            "placeholder": "hdhrdf:ScientificCommittee",
            "label": "Existence d'un comité scientifique",
            "type": "string",
        },
        "base_publisher_site": {
            "placeholder": "foaf:homepage",
            "label": "Site internet",
            "type": "url",
        },
        "base_creator_institut": {
            "placeholder": "foaf:name",
            "label": "Institut",
            "type": "string",
        },
        "base_creator_site": {
            "placeholder": "xds:anyURI",
            "label": "Site internet",
            "type": "url",
        },
        "base_creator_adresse": {
            "placeholder": "locn:Adress",
            "label": "Adresse",
            "type": "string",
        },
        "base_creator_responsable": {
            "placeholder": "hdhrdf:Scientist",
            "label": "Responsable scientifique",
            "type": "string",
        },
        "base_creator_identifiant": {
            "placeholder": "dct:identifier",
            "label": "Identifiant numérique",
            "type": "string",
        },
        "base_contributors_institut": {
            "placeholder": "foaf:name",
            "label": "Institut",
            "type": "string",
        },
        "base_contributors_site": {
            "placeholder": "xds:anyURI",
            "label": "Site internet",
            "type": "url",
        },
        "base_contributors_adresse": {
            "placeholder": "locn:Adress",
            "label": "Adresse",
            "type": "string",
        },
        "base_contributors_responsable": {
            "placeholder": "hdhrdf:Scientist",
            "label": "Responsable scientifique",
            "type": "string",
        },
        "base_contributors_identifiant": {
            "placeholder": "dct:identifier",
            "label": "Identifiant numérique",
            "type": "string",
        },
        "base_contact_nom": {
            "placeholder": "foaf:name",
            "label": "Nom",
            "type": "string",
        },
        "base_contact_adresse": {
            "placeholder": "locn:Adress",
            "label": "Adresse",
            "type": "string",
        },
        "base_contact_mail": {
            "placeholder": "foaf:mbox",
            "label": "Adresse mail",
            "type": "email",
        },
        "base_droits_site": {
            "placeholder": "xds:anyURI",
            "label": "Site internet",
            "type": "url",
        },
        "base_droits_adresse": {
            "placeholder": "locn:Adress",
            "label": "Adresse",
            "type": "string",
        },
        "base_droits_mail": {
            "placeholder": "foaf:mbox",
            "label": "Adresse mail",
            "type": "email",
        },
        "base_valorisation_scientifique": {
            "placeholder": "hdhrdf:ScientificValorization",
            "label": "Valorisation scientifique",
            "type": "string",
        },
        "base_annee_publication": {
            "placeholder": "dct:issued",
            "label": "Année de publication",
            "type": "date",
        },
        "base_langue": {
            "placeholder": "dct:language",
            "label": "Langue",
            "type": "langage",
        },
        "base_editor_nom": {
            "placeholder": "foaf:name",
            "label": "Nom",
            "type": "string",
        },
        "base_type_ressource": {
            "placeholder": "hdhrdf:ResourceType",
            "label": "Type de ressource",
            "type": "string",
        },
        "base_categorie_donnees": {
            "placeholder": "hdhrdf:DataCategory",
            "label": "Catégorie de données",
            "type": "string",
        },
        "base_type_base": {
            "placeholder": "hdhrdf:BaseType",
            "label": "Type de base",
            "type": "string",
        },
        "base_type_donnees": {
            "placeholder": "hdhrdf:DataType",
            "label": "Type de données",
            "type": "string",
        },
        "base_objectifs": {
            "placeholder": "hdhrdf:Objectives",
            "label": "Objectifs de la base de données",
            "type": "string",
        },
        "base_source": {
            "placeholder": "dct:source",
            "label": "Source de données",
            "type": "string",
        },
        "base_perimetre_geographique": {
            "placeholder": "dct:spatial",
            "label": "Périmètre géographique",
            "type": "string",
        },
        "base_precision_geographique": {
            "placeholder": "hdhrdf:GeographicalPrecision",
            "label": "Niveau de précision géographique",
            "type": "string",
        },
        "base_population_interet": {
            "placeholder": "hdhrdf:PopulationOfInterest",
            "label": "Population d'intérêt",
            "type": "string",
        },
        "base_nombre_participants": {
            "placeholder": "hdhrdf:NumberOfParticipants",
            "label": "Nombre de participants",
            "type": "string",
        },
        "base_distribution_population_lien": {
            "placeholder": "xds:anyURI",
            "label": "Lien",
            "type": "url",
        },
        "base_distribution_population_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "base_distribution_population_dispersion": {
            "placeholder": "hdhrdf:Dispersion",
            "label": "Dispersion",
            "type": "integer",
        },
        "base_format_base": {
            "placeholder": "dct:format",
            "label": "Format de la base",
            "type": "string",
        },
        "base_standard_donnees": {
            "placeholder": "hdhrdf:DataStandard",
            "label": "Standard des données",
            "type": "string",
        },
        "base_taille_base": {
            "placeholder": "dcat:byteSize",
            "label": "Taille de la base",
            "type": "string",
        },
        "base_nombre_tables": {
            "placeholder": "hdhrdf:NumberOfTables",
            "label": "Nombre de tables",
            "type": "integer",
        },
        "base_nombre_variables": {
            "placeholder": "hdhrdf:NumberOfVariables",
            "label": "Nombre de variables",
            "type": "integer",
        },
        "base_start_recueil": {
            "placeholder": "schema:startDate",
            "label": "Date du premier recueil",
            "type": "date",
        },
        "base_status_collecte": {
            "placeholder": "hdhrdf:CollectionStatus",
            "label": "Status de la collecte des données",
            "type": "string",
        },
        "base_taux_exhaustivite": {
            "placeholder": "hdhrdf:CompletenessRate",
            "label": "Taux d'exhaustivité",
            "type": "string",
        },
        "base_end_recueil": {
            "placeholder": "schema:endDate",
            "label": "Date du dernier recueil",
            "type": "date",
        },
        "base_profondeur_historique": {
            "placeholder": "hdhrdf:Historical",
            "label": "Profondeur historique",
            "type": "string",
        },
        "base_derniere_modification": {
            "placeholder": "dcat:modified",
            "label": "Date de dernière modification",
            "type": "date",
        },
        "base_frequence_modification": {
            "placeholder": "dct:accrualPeriodicity",
            "label": "Fréquence de mise à jour",
            "type": "string",
        },
        "base_mode_recueil": {
            "placeholder": "hdhrdf:MethodOfCollection",
            "label": "Mode de recueil des données",
            "type": "string",
        },
        "base_modalite_approbation": {
            "placeholder": "hdhrdf:MethodOfApproval",
            "label": "Modalité d'approbation du patient pour le receuil de ses données",
            "type": "string",
        },
        "base_processus_collecte": {
            "placeholder": "hdhrdf:CollectionProcess",
            "label": "Processus de collecte",
            "type": "string",
        },
        "base_protocole_collecte": {
            "placeholder": "hdhrdf:CollectionProtocol",
            "label": "Protocole de collecte",
            "type": "string",
        },
        "base_procedure_qualite": {
            "placeholder": "hdhrdf:QualityProcedure",
            "label": "Procédure qualité utilisée",
            "type": "string",
        },
        "base_outils_collecte": {
            "placeholder": "hdhrdf:CollectionTools",
            "label": "Outils utilisés pour la collecte",
            "type": "string",
        },
        "base_suivi_participants": {
            "placeholder": "hdhrdf:ParticipantsFollow-up",
            "label": "Suivi des participants",
            "type": "string",
        },
        "base_modalites_suivi": {
            "placeholder": "hdhrdf:Follow-upModalities",
            "label": "Modalités de suivi des participants",
            "type": "string",
        },
        "base_details_suivi": {
            "placeholder": "hdhrdf:Follow-upDetails",
            "label": "Détails du suivi",
            "type": "string",
        },
        "base_appariement": {
            "placeholder": "hdhrdf:Paring",
            "label": "Appariement au SNDS",
            "type": "string",
        },
        "base_type_appariement": {
            "placeholder": "hdhrdf:ParingType",
            "label": "Type d'appariement",
            "type": "string",
        },
        "base_taux_appariement": {
            "placeholder": "hdhrdf:ParingRate",
            "label": "Taux d'appariement",
            "type": "string",
        },
        "base_conditions_access_HDH": {
            "placeholder": "hdhrdf:AccessConditions",
            "label": "Conditions d'accès au HDH",
            "type": "string",
        },
        "base_conditions_access_externe": {
            "placeholder": "hdhrdf:ExternalAccessConditions",
            "label": "Conditions d'accès externe au HDH",
            "type": "string",
        },
        "base_echantillon": {
            "placeholder": "hdhrdf:Sample",
            "label": "Echantillon synthétique",
            "type": "string",
        },
        "base_variables": {
            "placeholder": "hdhrdf:Variables",
            "label": "Dictionnaire de variables",
            "type": "string",
        },
        "base_programmes_exploitation": {
            "placeholder": "hdhrdf:OperatingPrograms",
            "label": "Programmes facilitant l'exploitation des données",
            "type": "string",
        },
        "base_document_valorisation": {
            "placeholder": "hdhrdf:ValuationDocument",
            "label": "Document de valorisation",
            "type": "string",
        },
        "base_expertise": {
            "placeholder": "hdhrdf:Expertise",
            "label": "Formations et expertises",
            "type": "string",
        },
        "base_projets": {
            "placeholder": "hdhrdf:UsedInProjectss",
            "label": "Liste des projets utilisant la base de donnée",
            "type": "string",
        },
    }

    categories_base = {
        "Général": [
            "base_ID",
            "base_titre",
            "base_acronyme",
            "base_identifiant_DOI",
            "base_reference_HDH",
            "base_domaine_medical",
            "base_keyword",
            "base_description",
            "base_financement",
            "base_valorisation_scientifique",
            "base_annee_publication",
            "base_langue",
        ],
        "Gouvernance de la base de données": [
            "base_publisher_organisation",
            "base_publisher_adresse",
            "base_publisher_status",
            "base_publisher_ROR",
            "base_publisher_comite",
            "base_publisher_site",
        ],
        "Créateurs": [
            "base_creator_institut",
            "base_creator_site",
            "base_creator_adresse",
            "base_creator_responsable",
            "base_creator_identifiant",
        ],
        "Contributeurs": [
            "base_contributors_institut",
            "base_contributors_site",
            "base_contributors_adresse",
            "base_contributors_responsable",
            "base_contributors_identifiant",
        ],
        "Contact supplémentaire": [
            "base_contact_nom",
            "base_contact_adresse",
            "base_contact_mail",
        ],
        "Exercer ses droits": [
            "base_droits_site",
            "base_droits_adresse",
            "base_droits_mail",
        ],
        "Editeur": ["base_editor_nom"],
        "Catactéristiques": [
            "base_type_ressource",
            "base_categorie_donnees",
            "base_type_base",
            "base_type_donnees",
            "base_objectifs",
            "base_source",
            "base_perimetre_geographique",
            "base_precision_geographique",
            "base_population_interet",
            "base_nombre_participants",
            "base_distribution_population_lien",
            "base_distribution_population_moyenne",
            "base_distribution_population_dispersion",
            "base_format_base",
            "base_standard_donnees",
            "base_taille_base",
            "base_nombre_tables",
            "base_nombre_variables",
        ],
        "Collecte": [
            "base_start_recueil",
            "base_status_collecte",
            "base_taux_exhaustivite",
            "base_end_recueil",
            "base_profondeur_historique",
            "base_derniere_modification",
            "base_frequence_modification",
            "base_mode_recueil",
            "base_modalite_approbation",
            "base_processus_collecte",
            "base_protocole_collecte",
            "base_procedure_qualite",
            "base_outils_collecte",
            "base_suivi_participants",
            "base_modalites_suivi",
            "base_details_suivi",
            "base_appariement",
            "base_type_appariement",
            "base_taux_appariement",
        ],
        "Accès": ["base_conditions_access_HDH", "base_conditions_access_externe"],
        "Services supplémentaires": [
            "base_echantillon",
            "base_variables",
            "base_programmes_exploitation",
            "base_document_valorisation",
            "base_expertise",
        ],
        "Projets liés": ["base_projets"],
    }

    structure_table_required = {"table_ID": {"placeholder": "rdf:ID", "label": "ID"}}

    structure_table = {
        "table_titre": {"placeholder": "dct:title", "label": "Titre", "type": "string"},
        "table_description": {
            "placeholder": "dct:description",
            "label": "Description",
            "type": "string",
        },
        "table_reference": {
            "placeholder": "dct:identifier",
            "label": "Référence",
            "type": "string",
        },
        "table_format": {
            "placeholder": "dct:format",
            "label": "Format",
            "type": "string",
        },
        "table_taille": {
            "placeholder": "dcat:byteSize",
            "label": "Taille",
            "type": "string",
        },
        "table_variables": {
            "placeholder": "hdhrdf:Variables",
            "label": "Liste des variables",
            "type": "string",
        },
        "table_mise_en_production": {
            "placeholder": "schema:startDate",
            "label": "Date de mise en production",
            "type": "date",
        },
        "table_arret": {
            "placeholder": "schema:endDate",
            "label": "Date d'arrêt",
            "type": "date",
        },
        "table_dates_manquantes": {
            "placeholder": "hdhrdf:MissingDates",
            "label": "Dates manquantes",
            "type": "string",
        },
        "table_graph_table_lien": {
            "placeholder": "xds:anyURI",
            "label": "Lien",
            "type": "url",
        },
        "table_graph_table_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "table_graph_table_dispersion": {
            "placeholder": "hdhrdf:Dispersion",
            "label": "Dispersion",
            "type": "integer",
        },
        "table_graph_variables_lien": {
            "placeholder": "xds:anyURI",
            "label": "Lien",
            "type": "url",
        },
        "table_graph_variables_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "table_graph_variables_dispersion": {
            "placeholder": "hdhrdf:Dispersion",
            "label": "Dispersion",
            "type": "integer",
        },
        "table_cle_primaire": {
            "placeholder": "hdhrdf:PrimaryKey",
            "label": "Clé primaire",
            "type": "string",
        },
        "table_cles_secondaires": {
            "placeholder": "hdhrdf:ForeignKeys",
            "label": "Clés secondaires",
            "type": "string",
        },
    }

    categories_table = {
        "Général": [
            "table_ID",
            "table_titre",
            "table_description",
            "table_reference",
            "table_format",
            "table_taille",
            "table_variables",
            "table_mise_en_production",
            "table_arret",
            "table_dates_manquantes",
            "table_graph_table_lien",
            "table_graph_table_moyenne",
            "table_graph_table_dispersion",
            "table_graph_variables_lien",
            "table_graph_variables_moyenne",
            "table_graph_variables_dispersion",
        ],
        "Relations entre tables": ["table_cle_primaire", "table_cles_secondaires"],
    }

    structure_variable_required = {
        "variable_ID": {"placeholder": "rdf:ID", "label": "ID"}
    }

    structure_variable = {
        "variable_identifiant": {
            "placeholder": "dct:identifier",
            "label": "Indentifiant",
            "type": "string",
        },
        "variable_titre": {
            "placeholder": "dct:title",
            "label": "Titre",
            "type": "string",
        },
        "variable_description": {
            "placeholder": "dct:description",
            "label": "Description",
            "type": "string",
        },
        "variable_type": {
            "placeholder": "hdhrdf:Type",
            "label": "Type",
            "type": "string",
        },
        "variable_format": {
            "placeholder": "hdhrdf:Format",
            "label": "Format",
            "type": "string",
        },
        "variable_unite": {
            "placeholder": "hdhrdf:Unit",
            "label": "Unité",
            "type": "string",
        },
        "variable_type_objet": {
            "placeholder": "hdhrdf:ObjectType",
            "label": "Type d'objet",
            "type": "string",
        },
        "variable_terminologie": {
            "placeholder": "hdhrdf:Terminology",
            "label": "Terminologie de références",
            "type": "string",
        },
        "variable_graph_terminologie_lien": {
            "placeholder": "xds:anyURI",
            "label": "Lien",
            "type": "url",
        },
        "variable_graph_terminologie_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "variable_graph_terminologie_dispersion": {
            "placeholder": "hdhrdf:Dispersion",
            "label": "Dispersion",
            "type": "integer",
        },
        "variable_classification": {
            "placeholder": "hdhrdf:Classification",
            "label": "Classification de la variable",
            "type": "string",
        },
        "variable_mise_en_production": {
            "placeholder": "dct:issued",
            "label": "Date de mise en production",
            "type": "date",
        },
        "variable_fin": {
            "placeholder": "dcat:modified",
            "label": "Date de fin",
            "type": "date",
        },
        "variable_exhaustivite": {
            "placeholder": "hdhrdf:CompletenessDate",
            "label": "Date d'exhaustivité",
            "type": "date",
        },
        "variable_dates_manquantes": {
            "placeholder": "hdhrdf:MissingDates",
            "label": "Dates manquantes",
            "type": "string",
        },
        "variable_tables_presence": {
            "placeholder": "hdhrdf:PresenceInOtherDataset",
            "label": "Liste des tables où la variable est présente",
            "type": "string",
        },
        "variable_nombre_valeurs_uniques": {
            "placeholder": "hdhrdf:NumberOfUniqueValues",
            "label": "Nombre de valeurs uniques",
            "type": "integer",
        },
        "variable_valeur_max": {
            "placeholder": "hdhrdf:MaxValue",
            "label": "Valeur maximale",
            "type": "integer",
        },
        "variable_valeur_min": {
            "placeholder": "hdhrdf:MinValue",
            "label": "Valeur minimale",
            "type": "integer",
        },
        "variable_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "variable_mediane": {
            "placeholder": "hdhrdf:Median",
            "label": "Médiane",
            "type": "integer",
        },
        "variable_variance": {
            "placeholder": "hdhrdf:Variance",
            "label": "Variance",
            "type": "integer",
        },
        "variable_ecart_type": {
            "placeholder": "hdhrdf:StandardDeviation",
            "label": "Ecart-type",
            "type": "integer",
        },
        "variable_pourcentage_valeurs_manquantes": {
            "placeholder": "hdhrdf:PourcentageMissingValues",
            "label": "Pourcentage de valeurs manquantes",
            "type": "string",
        },
        "variable_code_valeurs_manquantes": {
            "placeholder": "hdhrdf:MissingValuesCode",
            "label": "Code pour les valeurs manquantes",
            "type": "string",
        },
        "variable_pourcentage_valeurs_non_dispo": {
            "placeholder": "hdhrdf:PourcentageNotAvailableValues",
            "label": "Pourcentage de valeurs non disponibles",
            "type": "string",
        },
        "variable_code_valeurs_non_dispo": {
            "placeholder": "hdhrdf:NotAvailableValuesCode",
            "label": "Code pour les valeurs non disponibles",
            "type": "string",
        },
        "variable_graph_completude_lien": {
            "placeholder": "xds:anyURI",
            "label": "Lien",
            "type": "url",
        },
        "variable_graph_completude_moyenne": {
            "placeholder": "hdhrdf:Mean",
            "label": "Moyenne",
            "type": "integer",
        },
        "variable_graph_completude_dispersion": {
            "placeholder": "hdhrdf:Dispersion",
            "label": "Dispersion",
            "type": "integer",
        },
        "variable_conformite_terminologies": {
            "placeholder": "hdhrdf:ComplianceWithTerminologie",
            "label": "Conformité avec les terminologies de références",
            "type": "string",
        },
        "variable_conformite_type": {
            "placeholder": "hdhrdf:ComplianceWithObjectType",
            "label": "Conformité avec le type d'objet",
            "type": "string",
        },
        "variable_conformite_unite": {
            "placeholder": "hdhrdf:ComplianceWithUnit",
            "label": "Conformité avec l'unité",
            "type": "string",
        },
        "variable_conformite_format": {
            "placeholder": "hdhrdf:ComplianceWithFormat",
            "label": "Conformité avec le format",
            "type": "string",
        },
        "variable_methodologie_mesure": {
            "placeholder": "hdhrdf:MeasurementMethodology",
            "label": "Méthodologie de mesure et description",
            "type": "string",
        },
        "variable_pausibilite": {
            "placeholder": "hdhrdf:Plausibility",
            "label": "Pausibilité",
            "type": "string",
        },
        "variable_bornes": {
            "placeholder": "hdhrdf:Terminals",
            "label": "Bornes",
            "type": "string",
        },
        "variable_resolution_moyenne": {
            "placeholder": "hdhrdf:MediumResolution",
            "label": "Résolution moyenne",
            "type": "string",
        },
        "variable_densite": {
            "placeholder": "hdhrdf:PointsDensity",
            "label": "Densité des points",
            "type": "string",
        },
        "variable_taille": {
            "placeholder": "dcat:byteSize",
            "label": "Taille de fichier",
            "type": "string",
        },
        "variable_echantillon": {
            "placeholder": "hdhrdf:Sample",
            "label": "Echantillon synthétique",
            "type": "string",
        },
        "variable_format_images": {
            "placeholder": "dcat:mediaType",
            "label": "Format des images",
            "type": "string",
        },
    }

    categories_variable = {
        "Général": [
            "variable_ID",
            "variable_identifiant",
            "variable_titre",
            "variable_description",
            "variable_type",
            "variable_format",
            "variable_unite",
            "variable_type_objet",
            "variable_terminologie",
            "variable_graph_terminologie_lien",
            "variable_graph_terminologie_moyenne",
            "variable_graph_terminologie_dispersion",
            "variable_classification",
            "variable_mise_en_production",
            "variable_fin",
            "variable_exhaustivite",
            "variable_dates_manquantes",
            "variable_tables_presence",
        ],
        "Statistiques": [
            "variable_nombre_valeurs_uniques",
            "variable_valeur_max",
            "variable_valeur_min",
            "variable_moyenne",
            "variable_mediane",
            "variable_variance",
            "variable_ecart_type",
        ],
        "Indicateurs qualité": [
            "variable_pourcentage_valeurs_manquantes",
            "variable_code_valeurs_manquantes",
            "variable_pourcentage_valeurs_non_dispo",
            "variable_code_valeurs_non_dispo",
            "variable_graph_completude_lien",
            "variable_graph_completude_moyenne",
            "variable_graph_completude_dispersion",
            "variable_conformite_terminologies",
            "variable_conformite_type",
            "variable_conformite_unite",
            "variable_conformite_format",
            "variable_methodologie_mesure",
            "variable_pausibilite",
            "variable_bornes",
        ],
        "Images": [
            "variable_resolution_moyenne",
            "variable_densite",
            "variable_taille",
            "variable_echantillon",
            "variable_format_images",
        ],
    }

    return (
        structure_base_required,
        structure_base,
        categories_base,
        structure_table_required,
        structure_table,
        categories_table,
        structure_variable_required,
        structure_variable,
        categories_variable,
    )


# Récupératon de l'historique des insertions
@app.get("/insertion")
def insertion():
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `insertion`"
    cursor.execute(query)

    insertions = []

    for (username, base, date) in cursor:
        insertions.append({"username": username, "base": base, "date": date})

    cursor.close()
    cnx.close()

    return insertions


# Récupératon de l'historique des modifications
@app.get("/modification")
def modification():
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `modification`"
    cursor.execute(query)

    modifications = []

    for (username, base, date, input, modification) in cursor:
        modifications.append(
            {
                "username": username,
                "base": base,
                "date": date,
                "input": input,
                "modification": modification,
            }
        )

    cursor.close()
    cnx.close()

    return modifications


# Récupératon de l'historique des suppressions
@app.get("/suppression")
def suppression():
    # Connexion à MySQL
    cnx = mysql.connector.connect(
        host="0.0.0.0", port="5005", password="hdh", database="metadata"
    )
    cursor = cnx.cursor()

    query = "SELECT * FROM `suppression`"
    cursor.execute(query)

    modifications = []

    for (username, base, date) in cursor:
        modifications.append({"username": username, "base": base, "date": date})

    cursor.close()
    cnx.close()

    return modifications


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5004)
