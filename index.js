import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import App from "./App";
import Document from "./components/Pages/Document";
import Form from "./components/Pages/Form";
import Supp from "./components/Pages/Supp";
import Login from "./components/Pages/Login";
import Logout from "./components/Pages/Logout";
import User from "./components/Pages/User";
import ModifPerso from "./components/Pages/ModifPerso";
import ModifPassword from "./components/Pages/ModifPassword";
import Documents from "./components/Pages/ListDocumentsUser";
import SuccessPage from "./components/Success/Success";
import Historique from "./components/Pages/Historique";
import NewUser from "./components/Pages/NewUser";
import ModifUser from "./components/Pages/ModifUser";
import SuppUser from "./components/Pages/SuppUser";
import Access from "./components/Pages/Access";
import Import from "./components/Pages/Import";
import Export from "./components/Pages/Export";
import FirstConnexion from "./components/Pages/FirstConnexion";
import LostPassword from "./components/Pages/LostPassword";

const rootElement = document.getElementById("root");

render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="login" element={<Login />} />
      <Route path="logout" element={<Logout />} />
      <Route path="base" element={<Document />} />
      <Route path="table" element={<Document />} />
      <Route path="variable" element={<Document />} />
      <Route path="new" element={<Form />} />
      <Route path="supp" element={<Supp />} />
      <Route path="success" element={<SuccessPage />} />
      <Route path="success_modif" element={<SuccessPage />} />
      <Route path="success_save" element={<SuccessPage />} />
      <Route path="success_user" element={<SuccessPage />} />
      <Route path="success_user_modif" element={<SuccessPage />} />
      <Route path="success_access" element={<SuccessPage />} />
      <Route path="success_modif_perso" element={<SuccessPage />} />
      <Route path="success_modif_password" element={<SuccessPage />} />
      <Route path="success_first" element={<SuccessPage />} />
      <Route path="success_supp" element={<SuccessPage />} />
      <Route path="user" element={<User />} />
      <Route path="modif_perso" element={<ModifPerso />} />
      <Route path="modif_password" element={<ModifPassword />} />
      <Route path="bases" element={<Documents />} />
      <Route path="export_base" element={<Export />} />
      <Route path="edit_base" element={<Form />} />
      <Route path="edit_base_in_progress" element={<Form />} />
      <Route path="historique" element={<Historique />} />
      <Route path="new_user" element={<NewUser />} />
      <Route path="modif_user" element={<ModifUser />} />
      <Route path="supp_user" element={<SuppUser />} />
      <Route path="access" element={<Access />} />
      <Route path="import" element={<Import />} />
      <Route path="export_base" element={<Export />} />
      <Route path="first_connexion" element={<FirstConnexion />} />
      <Route path="lost_password" element={<LostPassword />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);
