import { Link } from "react-router-dom";
import { Box, Stack, Container } from "@mui/material";
import HomeIcon from '@mui/icons-material/Home';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {Page, Navigation, Titre_page, Titre_section, Texte, Input_search, Button_search, Tableau, Tableau_head, Tableau_cell_head, Tableau_body, Tableau_row, Tableau_cell} from "./components/Styles/Styles.js"
import useToken from './components/Token/useToken';
import SearchPage from "./components/Pages/Search";

export default function App() {

  const { token, setToken } = useToken();

  if(!token) {
    return (
      <Page>
        <Box>
        <Titre_page variant="h3">
          Catalogue de métadonnées
        </Titre_page>
        </Box>
        <Navigation>
          <Link to="/login">Login</Link>
        </Navigation>
        <SearchPage></SearchPage>
    </Page>
    );
  }

  return (
    <Page>
        <Box>
        <Titre_page variant="h3">
          Catalogue de métadonnées
        </Titre_page>
        </Box>
        <Navigation>
          <Link to="/"><HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />Home</Link> |{" "}
          <Link to={"/user"}>Profil</Link> |{" "}
          <Link to="/new">New</Link> |{" "}
          <Link to="/bases">Bases</Link> |{" "}
        </Navigation>
        <SearchPage></SearchPage>
    </Page>
  );

}